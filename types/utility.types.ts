export type UnaryFunc = (arg1?: unknown) => unknown;

export type BinaryFunc = (arg1: unknown, arg2: unknown) => unknown;

export type EmptyObject = Record<string, never>;

export type UnknownObject = Record<string, any>;

export type ObjectEntries<T = any> = {
  [K in keyof T]: [K, T[K]];
}[keyof T][];

export type CallbackFunction = (key: string, delta: any) => void;
