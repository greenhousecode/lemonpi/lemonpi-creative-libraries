declare namespace Omid {
  type AnyCallback = (...args: any[]) => any;

  type CreativeType =
    | 'definedByJavaScript'
    | 'htmlDisplay'
    | 'nativeDisplay'
    | 'video'
    | 'audio';
  type AccessMode = 'full' | 'limited';
  type Environment = 'web' | 'app';
  type AdSessionType = 'native' | 'html' | 'javascript';
  type OmidNativeInfo = {
    partnerName: string;
    partnerVersion: string;
  };
  type OmidJsInfo = {
    omidImplementer: string;
    serviceVersion: string;
    sessionClientVersion: string;
    partnerName: string;
    partnerVersion: string;
  };
  type App = {
    libraryVersion: string;
    appId: string;
  };
  type ErrorType = 'video' | 'generic';
  type ImpressionType =
    | 'definedByJavaScript'
    | 'unspecified'
    | 'loaded'
    | 'beginToRender'
    | 'onePixel'
    | 'viewable'
    | 'audible'
    | 'other';

  type AdEventType =
    | 'start'
    | ' firstQuartile'
    | ' midpoint'
    | ' thirdQuartile'
    | ' complete'
    | ' pause'
    | ' resume'
    | ' bufferStart'
    | ' bufferFinish'
    | ' skipped'
    | ' volumeChange'
    | ' playerStateChange'
    | ' adUserInteraction';

  type VastProperties = {
    isSkippable: boolean;
    skipOffset: number;
    isAutoPlay: boolean;
    position: string;
  };
  type VideoPlayerState =
    | 'MINIMIZED'
    | 'COLLAPSED'
    | 'NORMAL'
    | 'EXPANDED'
    | 'FULLSCREEN';

  type InteractionType = 'CLICK' | 'INVITATION_ACCEPTED';

  // types  internal use only
  type OmidJsSessionInterface = any;
  type Communication = any;

  class Rectangle {
    x: number;
    y: number;
    width: number;
    height: number;
    /**
     * @param {number} x The x coordinate of the upper left corner.
     * @param {number} y The y coordinate of the upper left corner.
     * @param {number} width The width of the rectangle.
     * @param {number} height The height of the rectangle.
     */
    constructor(x: number, y: number, width: number, height: number);
  }
  /**
   * The JS ad session API enabling the integration partner to contribute to an
   * existing native ad session. This is also responsible for communicating to
   * the OM SDK JS service and will also handle scenarios with limited access
   * to the OM SDK JS service - i.e. cross-domain iFrames.
   * This API is commonly used in the following scenarios;
   *  - video ad session relying on the HTML5 video player for injecting
   *    verification script resources and/or publishing OMID video events.
   *  - display ad session relying on a separate JS component to handle the
   *    impression event.
   * @public
   * https://docs.iabtechlab.com/omsdk-1.4/js/session-client_ad-session.js.html
   */
  class AdSession {
    constructor(
      context: Context,
      communication?: Communication,
      sessionInterface?: OmidJsSessionInterface
    );
    /**
     * Specifies the type of creative to be rendered in this session.
     * Requires that the native layer set the creative type to
     * DEFINED_BY_JAVASCRIPT.
     * @param {!CreativeType} creativeType The type of creative.
     * @throws error if arg type is DEFINED_BY_JAVASCRIPT.
     * @throws error if impression has already occured.
     * @throws error if creative has already loaded.
     * @throws error if creativeType was already defined to something
     * other than DEFINED_BY_JAVASCRIPT.
     * @throws error if native integration has started and
     * is using OMID 1.2 or earlier.
     * @public
     */
    setCreativeType(creativeType: CreativeType): void;

    /**
     * Specifies the type of impression to be triggered in this session.
     * Requires that the native layer set the impression type to
     * DEFINED_BY_JAVASCRIPT.
     * @param {!ImpressionType} impressionType The type of impression.
     * @throws error if arg type is DEFINED_BY_JAVASCRIPT
     * @throws error if impression has already occurred
     * @throws error if impressionType was already defined to something
     * other than DEFINED_BY_JAVASCRIPT.
     * @throws error if native integration has started and is
     * using OMID 1.2 or earlier.
     * @public
     */
    setImpressionType(impressionType: ImpressionType): void;
    /**
     * Returns true if OMID is available, false otherwise.
     * @return {boolean}
     * @public
     */
    isSupported(): boolean;
    // JSDoc, and remove the event handler field descriptions below.
    /**
     * Subscribes to all session events ('sessionStart', 'sessionError', and
     * 'sessionFinish').
     * The event handler will be called with a single argument that has the
     * following fields:
     *   'adSessionId': string,
     *   'timestamp': number,
     *   'type': string,
     *   'data': object
     * @param {function(!Event)} functionToExecute An event handler which will be
     *     invoked on session events.
     * @public
     */
    registerSessionObserver(functionToExecute: (event: Event) => void): void;
    /**
     * If there is no currently active ad session, this notifies all session
     * observers that an ad session has started with a SESSION_START event.
     * This starts ad view tracking and makes video and ad events available to
     * send to verification scripts injected for this ad session. This method
     * has no effect if called after the ad session has already started or in a
     * mobile app environment.
     */
    start(): void;
    /**
     * If there is a currently active ad session, this notifies all session
     * observers that the ad session has finished with a SESSION_FINISH event.
     * This ceases ad view tracking and message sending to verification scripts
     * injected for the ad session. This method has no effect if called if there
     * is no active ad session or in a mobile app environment.
     */
    finish(): void;
    /**
     * Notifies that an error has occurred on the ad session.
     * All verification clients will be notified via the 'sessionError' session
     * observer event.
     * @param {!ErrorType} errorType High level error type.
     * @param {string} message Description of the session error.
     * @public
     */
    error(errorType: ErrorType, message: string): void;
    /**
     * Registers the existence of an AdEvent instance.
     */
    registerAdEvents(): void;
    /**
     * Registers the existence of an MediaEvents instance.
     */
    registerMediaEvents(): void;
    /**
     * Sends a message to the OMID VerificationService and ignores responses.
     * NOTE: This method is friend scoped. Therefore it should not be exported
     * beyond obfuscation.
     * @param {string} method Name of the remote method to invoke.
     * @param {...?} args Arguments to use when invoking the remote
     *     function.
     */
    sendOneWayMessage(method: string, ...args: any): void;
    /**
     * Sends a message to the OMID SessionService.
     * NOTE: This method is friend scoped. Therefore it should not be exported
     * beyond obfuscation.
     * @param {string} method Name of the remote method to invoke.
     * @param {?function(...?)} responseCallback Callback to be called when a
     *     response is received.
     * @param {...?} args Arguments to use when invoking the remote function.
     */
    sendMessage(
      method: string,
      responseCallback: AnyCallback,
      ...args: any
    ): void;
    /**
     * Throws an error if the session is not running.
     * NOTE: This method is friend scoped. Therefore it should not be exported
     * beyond obfuscation.
     */
    assertSessionRunning(): void;
    /**
     * Handles when an impression has occurred.
     * Sets a flag of this class so that it can remember that an impression has
     * occured.
     * NOTE: This method is friend scoped. Therefore it should not be exported
     * beyond obfuscation.
     * @throws error if creativeType or impressionType has not be redefined
     * from the JS layer. Both the creative and impression types must be redefined
     * by the JS layer before the impression event can be sent from the JS layer.
     */
    impressionOccurred(): void;
    /**
     * Handles when a load event has occurred.
     * Sets a flag of this class so that it can remember that a loaded event
     * has occured.
     * NOTE: This method is friend scoped. Therefore it should not be exported
     * beyond obfuscation.
     * @throws error if creativeType or impressionType has not be redefined
     * from the JS layer. Both the creative and impression types must be redefined
     * by the JS layer before the loaded event can be sent from the JS layer.
     */
    creativeLoaded(): void;
    /**
     * Set the DOM element's geometry relative to the geometry of either the
     * slotElement or the cross domain iframe the creative's DOM element is in.
     * @param {?Rectangle} elementBounds
     * @throws Error if the elementBounds parameter is null or undefined.
     * @public
     */
    setElementBounds(elementBounds: Rectangle): void;
  }
  /**
   * Ad event API enabling the JS component to signal to all verification
   * providers when key events have occurred. The OM SDK JS service will allow
   * only one ad events instance to be associated with the ad session and any
   * attempt to create multiple instances will result in an error.
   * @public
   */
  class AdEvents {
    /**
     * @param {!AdSession} adSession The ad session instance for sending events.
     * @throws error if the supplied ad session is null.
     * @throws error if an ad events instance has already been registered with
     *   the ad session.
     */
    constructor(adSession: AdSession);

    /**
     * Notifies all verification providers that an impression event should be
     * recorded.
     * @throws error if the native ad session has not been started.
     * @public
     */
    impressionOccurred(): void;
    /**
     * Notifies all verification providers that a loaded event should be
     * recorded. Video/audio creatives should supply non-null vastProperties.
     * Display creatives should supply a null argument.
     *
     * @param {?VastProperties=} vastProperties containing static information
     * about the video placement. This is non-null for video/audio creatives and null
     * for display creatives.
     * @public
     */
    loaded(vastProperties: VastProperties | null): void;
  }

  /**
   * Holds information provided into the ad session context by the JavaScript
   * layer.
   * @public
   */
  class Context {
    /**
     * Create a new ad session context providing reference to partner and a list
     * of script resources which should be managed by OM SDK service.
     * @param {!Partner} partner The integration's partner ID and version.
     * @param {?Array<!VerificationScriptResource>} verificationScriptResources
     *   The verification resources to load.
     * @param {?string=} contentUrl On web, the URL of top-level web page.
     *     In apps, an optional content URL of the screen within the app that is
     *     showing the ad, such as an Android deep link or iOS universal link.
     *     Defaults to null.
     * @param {?string=} customReferenceData Arbitrary reference data the
     * integrator can share with verification scripts. Has no effect in
     * mobile app environment. Defaults to null.
     * @throws error if the supplied partner is undefined or null.
     */

    public contentUrl: string | null;
    public customReferenceData: string | null;
    public serviceWindow: Window | null;
    public slotElement: HTMLElement | null;
    public underEvaluation: boolean;
    public videoElement: HTMLVideoElement | null;

    constructor(
      partner: Partner,
      verificationScriptResources: Array<VerificationScriptResource>,
      contentUrl: string | null,
      customReferenceData: string | null
    );

    /**
     * Specifies the video element within the WebView.
     * @param {?HTMLVideoElement} videoElement The video element.
     * @public
     */
    setVideoElement(videoElement: HTMLVideoElement): void;
    /**
     * Specifies the ad creative HTML element within the WebView.
     * @param {?HTMLElement} slotElement The ad creative DOM element.
     * @public
     */
    setSlotElement(slotElement: HTMLElement): void;
    /**
     * By default, the OM SDK Session Client Library will assume the Service
     * Script is present in the same frame the library is loaded in, or top. Call
     * this method to override this default and point the library to the give
     * window instead.
     * @param {!Window} serviceWindow The window containing the OMID Service
     *     Script.
     */
    setServiceWindow(serviceWindow: Window): void;
  }
  /**
   * Provides a complete list of supported JS media events. Using this event API
   * assumes the media player is fully responsible for communicating all media
   * events at the appropriate times. Only one media events implementation can be
   * associated with the ad session and any attempt to create multiple instances
   * will result in an error. The same rules apply to both multiple JS media
   * events and any attempt to register a JS media events instance when a native
   * instance has already been registered via the native bridge.
   * @public
   */
  class MediaEvents {
    /**
     * @param {!AdSession} adSession The ad session instance for sending events.
     * @throws error if the supplied ad session is undefined or null.
     */
    constructor(adSession: AdSession);
    /**
     * Notifies all media listeners that media content has started playing.
     * @param {number} duration Duration of the selected media media (in seconds).
     * @param {number} mediaPlayerVolume Audio volume of the media player with a
     *   range between 0 and 1.
     * @throws error if an invalid duration or mediaPlayerVolume has been
     *   supplied.
     * @public
     */
    start(duration: number, mediaPlayerVolume: number): void;
    /**
     * Notifies all media listeners that media playback has reached the first
     * quartile.
     * @public
     */
    firstQuartile(): void;
    /**
     * Notifies all media listeners that media playback has reached the midpoint.
     * @public
     */
    midpoint(): void;
    /**
     * Notifies all media listeners that media playback has reached the third
     * quartile.
     * @public
     */
    thirdQuartile(): void;
    /**
     * Notifies all media listeners that media playback is complete.
     * @public
     */
    complete(): void;
    /**
     * Notifies all media listeners that media playback has paused after a user
     * interaction.
     * @public
     */
    pause(): void;
    /**
     * Notifies all media listeners that media playback has resumed (after being
     * paused) after a user interaction.
     * @public
     */
    resume(): void;
    /**
     * Notifies all media listeners that media playback has stopped and started
     * buffering.
     * @public
     */
    bufferStart(): void;
    /**
     * Notifies all media listeners that buffering has finished and media playback
     * has resumed.
     * @public
     */
    bufferFinish(): void;
    /**
     * Notifies all media listeners that media playback has stopped as a user skip
     * interaction. Once skipped media it should not be possible for the media to
     * resume playing content.
     * @public
     */
    skipped(): void;
    /**
     * Notifies all media listeners that the media player has changed the volume.
     * @param {number} mediaPlayerVolume Audio volume of the media player with a
     *   range between 0 and 1.
     * @throws error if an invalid mediaPlayerVolume has been supplied.
     * @public
     */
    volumeChange(mediaPlayerVolume: number): void;
    /**
     * Notifies all media listeners that media player state has changed.
     * @param {!VideoPlayerState} playerState The latest media player state.
     * @throws error if the supplied player state is undefined or null.
     * @see VideoPlayerState
     * @public
     */
    playerStateChange(playerState: VideoPlayerState): void;
    /**
     * Notifies all media listeners that the user has performed an ad interaction.
     * @param {!InteractionType} interactionType The latest user interaction.
     * @throws error if the supplied interaction type is undefined or null.
     * @public
     */
    adUserInteraction(interactionType: InteractionType): void;
  }
  /**
   * Represents the Version of OMID Session Client.
   */
  class OmidVersion {
    /**
     * @param {string} semanticVersion
     * @param {string} apiLevel
     * @throws error if any of the arguments is undefined, null or blank.
     */
    constructor(semanticVersion: string, apiLevel: string);
  }
  /**
   * Holds information about the integration partner that is using the session
   * client.
   * @public
   */
  class Partner {
    /**
     * Creates a new partner instance given a name and a version.
     * @param {string} name The partner ID of the integration.
     * @param {string} version The version of the integration's session script.
     * @throws error if any of the parameters are undefined, null or blank.
     */
    constructor(name: string, version: string);
  }
  /**
   * Allows verification scripts to interact with the OM SDK Service.
   * @public
   */
  class VerificationClient {
    public communication: Communication;
    /**
     * @param {?Communication<?>=} communication This parameter is for OM SDK
     *    internal use only and should be omitted.
     */
    constructor(communication: Communication | undefined);
    /**
     * Checks if OMID is available.
     * @return {boolean}
     * @public
     */
    isSupported(): boolean;
    /**
     * Gets the environment type of the OM Service that injected the verification
     * resource.
     * @return {Environment|undefined} the injecting service's environment type or
     * undefined if the verification resource was side-loaded or the service is a
     * 3rd party custom service.
     * @public
     */
    injectionSource(): Environment | undefined;
    // JSDoc, and remove the event handler field descriptions below.
    /**
     * Subscribes to all session events ('sessionStart', 'sessionError', and
     * 'sessionFinish'). This method also signals that the verification script has
     * loaded and is ready to receive events, so it should be called upon
     * initialization.
     * The event handler will be called with a single argument that has the
     * following fields:
     *   'adSessionId': string,
     *   'timestamp': number,
     *   'type': string,
     *   'data': object
     * @param {SessionObserverCallback} functionToExecute An event handler which
     *     will be invoked on session events.
     * @param {string=} vendorKey
     * @throws error if the function to execute is undefined or null.
     * @throws error if the vendor key is undefined, null or blank.
     * @public
     */
    registerSessionObserver(
      functionToExecute: AnyCallback,
      vendorKey: string | undefined
    ): void;
    /**
     * Subscribes to ad lifecycle and metric events.
     * The event handler will be called with a single argument that has the
     * following fields:
     *   'adSessionId': string,
     *   'timestamp': number,
     *   'type': string,
     *   'data': object
     * @param {!AdEventType} eventType The event type to subscribe this listener
     *     to.
     * @param {!EventCallback} functionToExecute An event handler to be invoked
     *     when the given event type is triggered.
     * @throws error if the event type is undefined, null or blank.
     * @throws error if the function to execute is undefined or null.
     * @public
     */
    addEventListener(
      eventType: AdEventType,
      functionToExecute: AnyCallback
    ): void;
    /**
     * Requests the target URL.
     *
     * This can be used to transmit data to a remote server by requesting a URL
     * with the payload embeded into the URL as query arg(s).
     * @param {string} url The URL to be requested.
     * @param {function()=} successCallback Optional callback to be executed if
     *     the request was successfully received (2xx response code).
     * @param {function()=} failureCallback Optional callback to be executed if
     *     the request was not successfully received (non-success response code or
     *     other error).
     * @throws error if the url is undefined, null or blank.
     * @public
     */
    sendUrl(
      url: string,
      successCallback: (() => void) | undefined,
      failureCallback: (() => void) | undefined
    ): void;
    /**
     * Injects the supplied JavaScript resource into the same execution
     * environment as the verification provider.
     *
     * For all DOM based environments (incl. Android native ad sessions) this will
     * append `script` elements to the DOM.
     * For native ad sessions this will delegate responsibility to the OM SDK
     * library which will be responsible for downloading and injecting the
     * JavaScript content into the execution environment.
     * @param {string} url The URL of the JavaScript resource to load into the
     *     environment.
     * @param {function()=} successCallback Optional callback to be executed if
     *     the HTTP request was successful. Does not indicate whether the script
     *     evaluation was successful.
     * @param {function()=} failureCallback Optional callback to be executed if
     *     the script failed to load.
     * @throws error if the supplied URL is undefined, null or blank.
     * @public
     */
    injectJavaScriptResource(
      url: string,
      successCallback: (() => void) | undefined,
      failureCallback: (() => void) | undefined
    ): void;
    /**
     * Schedules a function to be called a function after the specified delay.
     * Provides behavior equivalent to the window.setTimeout web API method.
     * @param {function()} functionToExecute The callback to execute after the
     *     delay.
     * @param {number} timeInMillis The number of milliseconds to wait before
     *     invoking the callback.
     * @return {number} A unique ID which can be used with clearTimeout to cancel
     *     the function execution.
     * @throws error if the function to execute is undefined or null.
     * @throws error if the time in millis is undefined, null or a non-positive
     *     number.
     * @public
     */
    setTimeout(functionToExecute: AnyCallback, timeInMillis: number): number;
    /**
     * Cancels a timeout before its callback has been executed.
     * Provides behavior equivalent to the window.clearTimeout web API method.
     * @param {number} timeoutId The ID returned from setTimeout of the callback
     *     to cancel.
     * @throws error if the timeout ID is undefined, null or a non-positive
     *     number.
     * @public
     */
    clearTimeout(timeoutId: number): void;
    /**
     * Schedules a function to be called repeatedly at a specified interval.
     * Provides behavior equivalent to the window.setInterval web API method.
     * @param {function()} functionToExecute The callback to execute repeatedly.
     * @param {number} timeInMillis The number of milliseconds to wait between
     *     callback invocations.
     * @return {number} A unique ID which can be used with clearInterval to cancel
     *     the function execution.
     * @throws error if the function to execute is undefined or null.
     * @throws error if the time in millis is undefined, null or a non-positive
     *     number.
     * @public
     */
    setInterval(functionToExecute: AnyCallback, timeInMillis: number): number;
    /**
     * Cancels further execution of a repeated callback.
     * @param {number} intervalId The ID returned from setInterval of the callback
     *     to cancel.
     * @throws error if the interval ID is undefined, null or a non-positive
     *     number.
     * @public
     */
    clearInterval(intervalId: number): void;
  }
  /**
   * Represents a verification script resource that comes in a VAST extension for
   * VAST versions <= 3 or a verification node for VAST versions >= 4
   * @public
   */
  class VerificationScriptResource {
    public accessMode: AccessMode;
    public resourceUrl: string;
    public vendorKey: string | undefined;
    public verificationParameters: string | undefined;
    /**
     * Creates new verification script resource instance which requires vendor
     * specific verification parameters.
     * @param {string} resourceUrl
     * @param {string=} vendorKey
     * @param {string=} verificationParameters
     * @param {AccessMode=} accessMode The level of access this verification
     *     script will have when executed.
     * @throws error if either the vendorKey or resourceUrl is undefined, null or
     *   blank.
     */
    constructor(
      resourceUrl: string,
      vendorKey: string | undefined,
      verificationParameters: string | undefined,
      accessMode: AccessMode
    );
    /**
     * @override
     * @return {!Object}
     */
    toJSON(): Record<string, any>;
  }
}

type OmidSessionClient = typeof Omid;

interface Window {
  OmidSessionClient?: {
    default: OmidSessionClient;
  };
}
