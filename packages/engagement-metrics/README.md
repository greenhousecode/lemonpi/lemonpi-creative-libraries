# Lemonpi engagement handler 

This package contains the Lemonpi engagement handler. It is responsible for sending engagement events to Lemonpi.

Lemonpi engagment handler registers the following events:
 1. Intersection Observer
    a. Intersection observer is used to track the minimum visibility of elements on the page - 50% during from the point of impression.
      On the unloadevent the following data should be send in url encoded format:
        ```json
            {
            "type": "engagement",
            "name": "intersection",
            "data: {
                totalImpressionTime,
                totalIntersectionTime
                } 
            }
            ```
    b. Intersection observer interval based tracking is used to track the minimum visibility of elements on the page - 50% during and send the visibility data for the duration of the event after 1 second then each 3 seconds.
          ```json
         {
            "type": "engagement",
            "name": "interaction-time",
            "data: {
                totalIntersectionTime
            } 
         }
        ```
 2. Interaction tracker 
    a. Hover tracker
          ```json
         {
            "type": "engagement",
            "name": "interaction",
            "data: {
                interactionPath,
                interactionDuration
            } 
         }
        ```
    b. Touch tracker
         ```json
         {
            "type": "engagement",
            "name": "interaction",
            "data: {
                interactionPath,
                interactionDuration
            } 
         }
        ```
 3. Click element tracker 
    Click tracker
          ```json
         {
            "type": "engagement",
            "name": "interaction",
            "data: {
                clickElement,
            } 
         }
        ```