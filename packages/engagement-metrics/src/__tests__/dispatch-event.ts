import * as dispatch from '../dispatch-event';

jest.mock('../dispatch-event', () => ({
  ...jest.requireActual('../dispatch-event'),
  postEvent: jest.fn()
}));
const interactionHandlerUrl = 'https://track.lemonpi.io/events';
const interactionHandlerUrlWithEventPlaceholder =
  interactionHandlerUrl + '?e=LEMONPI%25EVENTDATA';

function configureCreativeLibraries(config: any) {
  window.dispatchEvent(
    new CustomEvent('lemonpi.config/ready', {
      detail: {
        interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
        adsetId: 1,
        creativeId: 2,
        impressionId: '00000000-0000-0000-0000-000000000000',
        version: 2,
        adserver: { code: 'lemonpi' },
        ...config
      }
    })
  );
}

describe('sendEngagementData', () => {
  beforeAll(() => {
    configureCreativeLibraries({});
  });

  test('should call postEvent', () => {
    const spyOnPostEvent = jest.spyOn(dispatch, 'sendEngagementData');
    dispatch.sendEngagementData('viewability', {});
    expect(spyOnPostEvent).toHaveReturnedWith(
      dispatch.postEvent({
        type: 'engagement',
        name: 'viewability',
        data: {}
      })
    );
  });
  test('should return null', () => {
    const spyOnPostEvent = jest.spyOn(dispatch, 'sendEngagementData');

    dispatch.sendEngagementData('wrongEvent' as any, {});

    expect(spyOnPostEvent).toHaveReturnedWith(null);
    spyOnPostEvent.mockRestore();
  });
});
