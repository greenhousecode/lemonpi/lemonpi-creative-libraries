import { registerClickTracking } from '../click-element-tracking';

describe('click element tracking', () => {
  const iframe = document.createElement('iframe');

  beforeEach(() => {
    Object.defineProperty(window, 'frameElement', {
      value: iframe,
      writable: true
    });
  });
  test('should track click element', () => {
    const addEventListenerSpy = jest.spyOn(iframe, 'addEventListener');
    registerClickTracking();

    expect(addEventListenerSpy).toHaveBeenCalledWith(
      'click',
      expect.any(Function)
    );

    const elementWithinIframe = document.createElement('div');
    document.body.appendChild(elementWithinIframe);

    let hasEventFired = false;

    elementWithinIframe.addEventListener('click', () => {
      hasEventFired = true;
    });
    const clickEvent = new Event('click', {
      bubbles: true,
      cancelable: true
    });

    elementWithinIframe.dispatchEvent(clickEvent);

    expect(hasEventFired).toBe(true);
  });
});
