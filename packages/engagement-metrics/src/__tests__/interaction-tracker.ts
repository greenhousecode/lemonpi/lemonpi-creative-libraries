import { throttle } from '../interaction-tracker';

describe('throttle', () => {
  it('should throttle the callback function', () => {
    jest.useFakeTimers();

    const callback = jest.fn();
    const throttledCallback = throttle(callback, 1000);

    throttledCallback();
    jest.advanceTimersByTime(500);
    throttledCallback();

    expect(callback).toHaveBeenCalledTimes(1); // The callback should have been called only once at this point

    jest.advanceTimersByTime(500);
    throttledCallback();
    expect(callback).toHaveBeenCalledTimes(2); // The callback should have been called again after the full delay

    jest.useRealTimers();
  });
});
