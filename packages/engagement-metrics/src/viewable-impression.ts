import { tryCatchWrapper } from '../../_utils/src/tryCatchWrapper';
import { postEvent } from './dispatch-event';
import { Engagement, EngagementEvent } from './engament-metrics.types';

// 50% pixels in view
function handleViewableImpression(viewabilityData: Record<string, number>) {
  postEvent({
    type: Engagement.ENGAGEMENT,
    name: EngagementEvent.VIEWABLE_IMPRESSION,
    data: viewabilityData
  });
}

const wrappedHandleViewableImpression = tryCatchWrapper(
  handleViewableImpression
);

window.addEventListener(
  'lemonpi.interaction/viewable-impression',
  (event: CustomEvent) => {
    wrappedHandleViewableImpression(event.detail);
  }
);

// 1 pixel in view
function handleMinimalViewableImpression(
  viewabilityData: Record<string, number>
) {
  postEvent({
    type: Engagement.ENGAGEMENT,
    name: EngagementEvent.MINIMAL_VIEWABLE_IMPRESSION,
    data: viewabilityData
  });
}
const wrappedHandleMinimalViewableImpression = tryCatchWrapper(
  handleMinimalViewableImpression
);

window.addEventListener(
  'lemonpi.interaction/minimal-viewable-impression',
  (event: CustomEvent) => {
    wrappedHandleMinimalViewableImpression(event.detail);
  }
);
