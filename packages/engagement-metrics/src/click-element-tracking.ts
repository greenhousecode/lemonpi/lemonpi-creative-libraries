import { beaconBlobData } from './dispatch-event';
import { Engagement, EngagementEvent } from './engament-metrics.types';

export function trackClickElement(e: MouseEvent) {
  let clickedElement = null;

  if (e.target instanceof HTMLElement) {
    clickedElement = {
      tagName: e.target.tagName.trim(),
      innerText: e.target.textContent.trim()
    };
  }

  const encodedData = beaconBlobData({
    type: Engagement.ENGAGEMENT,
    name: EngagementEvent.INTERACTION_CLICK,
    data: {
      clickedElement,
      clickCordinates: {
        x: e.clientX,
        y: e.clientY
      }
    }
  });
  const { url, blobData } = encodedData;
  if (navigator.sendBeacon && url && blobData) {
    navigator.sendBeacon(url, blobData);
  }
}

export function registerClickTracking() {
  const targetElement =
    window.frameElement || document.getElementsByTagName('body')[0];

  if (targetElement) {
    targetElement.addEventListener('click', trackClickElement);
  }
}
