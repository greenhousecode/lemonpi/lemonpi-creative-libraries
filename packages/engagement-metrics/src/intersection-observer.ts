import { calculateRoundedTimeDiff } from '../../_utils/src/calculateRoundedTimeDiff';
import { sendEngagementData } from './dispatch-event';
import { EngagementEvent } from './engament-metrics.types';
import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';

let isTrackEngagementMetrics = false;
let impressionTimestamp: number | null = null;
let totalImpressionTime = 0;
let timestamp: number | null = null;
let totalIntersectionTime = 0;
let fullIntersectionTime = 0;
let intersectionTimeoutRef: NodeJS.Timer | null = null;
let updateIntervalRef: NodeJS.Timer | null = null;
const INTERSECTION_OBSERVER_TRESHOLD = 0.5; // decimal (0.5 = 50%)
const INTERSECTION_OBSERVER_UPDATE_INTERVAL = 500; // ms

let minimalIntersectionTime = 0;
let minimalUpdateIntervalRef: NodeJS.Timer | null = null;

let hasViewableImpressionEventFired = false;
function handleViewableImpressionEvent(data: Record<string, number>) {
  if (hasViewableImpressionEventFired) return;

  dispatchCustomEvent('lemonpi.interaction/viewable-impression', data);
  hasViewableImpressionEventFired = true;
}

function incrementIntersectionAndTimestamp(intersectionRatio: number) {
  if (intersectionRatio === 1) {
    fullIntersectionTime += 0.5;
  }

  totalIntersectionTime += 0.5;
  timestamp += 500;

  if (totalIntersectionTime >= 1) {
    handleViewableImpressionEvent({
      totalImpressionTime,
      fullIntersectionTime
    });
  }
}

let hasMinimalViewableImpressionEventFired = false;
function handleMinimalViewableImpressionEvent(data: Record<string, number>) {
  if (hasMinimalViewableImpressionEventFired) return;

  dispatchCustomEvent('lemonpi.interaction/minimal-viewable-impression', data);
  hasMinimalViewableImpressionEventFired = true;
}
function incrementMinimalViewableImpression() {
  minimalIntersectionTime += 0.5;

  if (minimalIntersectionTime >= 1) {
    handleMinimalViewableImpressionEvent({
      minimalIntersectionTime,
      totalImpressionTime
    });
  }
}

const observer = new IntersectionObserver(
  (entries) => {
    for (const entry of entries) {
      // 1pixel of the element is visible
      if (!hasMinimalViewableImpressionEventFired) {
        if (entry.isIntersecting) {
          minimalUpdateIntervalRef = setInterval(
            () => incrementMinimalViewableImpression(),
            INTERSECTION_OBSERVER_UPDATE_INTERVAL
          );
        } else {
          clearInterval(minimalUpdateIntervalRef);
        }
      }
      // 50% of the element is visible
      if (entry.intersectionRatio >= INTERSECTION_OBSERVER_TRESHOLD) {
        if (timestamp === null) {
          timestamp = entry.time;
          updateIntervalRef = setInterval(
            () => incrementIntersectionAndTimestamp(entry.intersectionRatio),
            INTERSECTION_OBSERVER_UPDATE_INTERVAL
          );
        }
      } else {
        clearInterval(updateIntervalRef);
        if (timestamp !== null) {
          totalIntersectionTime += (entry.time - timestamp) / 1000;
        }
        timestamp = null;
      }
    }
  },
  {
    threshold: [0, INTERSECTION_OBSERVER_TRESHOLD]
  }
);

const targetElement =
  window.frameElement || document.getElementsByTagName('body')[0];

export function unregisterIntersectionObserver() {
  totalImpressionTime = calculateRoundedTimeDiff(
    Date.now(),
    impressionTimestamp
  );
  sendEngagementData(EngagementEvent.VIEWABILITY, {
    totalImpressionTime,
    totalIntersectionTime,
    fullIntersectionTime
  });
  if (intersectionTimeoutRef) {
    clearTimeout(intersectionTimeoutRef);
    intersectionTimeoutRef = null;
  }
  totalImpressionTime = 0;
  impressionTimestamp = null;
  observer.unobserve(targetElement);
}

// handle visibility change
export function endInteractionMeasurement() {
  clearTimeout(intersectionTimeoutRef);
  clearInterval(updateIntervalRef);
  timestamp = null;
  intersectionTimeoutRef = null;
}

export const handleVisibilityChange = () => {
  if (document.visibilityState === 'hidden') {
    endInteractionMeasurement();
  } else {
    registerIntersectionObserver(isTrackEngagementMetrics);
  }
};

export function registerEventListeners() {
  if (registerEventListeners.registered) {
    return;
  }
  window.addEventListener('visibilitychange', handleVisibilityChange);
  window.addEventListener('beforeunload', unregisterIntersectionObserver);
  registerEventListeners.registered = true;
}

registerEventListeners.registered = false;

const REPORT_INTERVALS = [0.5, 1, 1.5, 3, 6, 9, 12, 15, 20, 25, 30, 40, 50, 60]; // sec
let intervalCounter = 0;

function exponentialGrowingInterval() {
  totalImpressionTime = calculateRoundedTimeDiff(
    Date.now(),
    impressionTimestamp
  );

  sendEngagementData(EngagementEvent.INTERACTION_TIME, {
    totalImpressionTime,
    totalIntersectionTime,
    fullIntersectionTime
  });
  intervalCounter += 1;

  if (REPORT_INTERVALS.length !== intervalCounter) {
    intersectionTimeoutRef = setTimeout(
      exponentialGrowingInterval,
      (REPORT_INTERVALS[intervalCounter] -
        REPORT_INTERVALS[intervalCounter - 1]) *
        1000
    );
  } else {
    unregisterIntersectionObserver();
    window.removeEventListener('visibilitychange', handleVisibilityChange);
    window.removeEventListener('beforeunload', unregisterIntersectionObserver);
  }
}
export function registerIntersectionObserver(trackEngagementMetrics: boolean) {
  impressionTimestamp = Date.now();
  isTrackEngagementMetrics = trackEngagementMetrics;
  observer.observe(targetElement);

  if (intersectionTimeoutRef == null && trackEngagementMetrics) {
    intersectionTimeoutRef = setTimeout(
      exponentialGrowingInterval,
      REPORT_INTERVALS[0] * 1000
    );
  }

  registerEventListeners();
}
