export const Engagement = {
  IMPRESSION: 'impression',
  CLICK: 'click',
  ENGAGEMENT: 'engagement'
} as const;

export type EngagementType = typeof Engagement[keyof typeof Engagement];

export const EngagementEvent = {
  VIEWABILITY: 'viewability',
  VIEWABLE_IMPRESSION: 'viewable-impression',
  MINIMAL_VIEWABLE_IMPRESSION: 'minimal-viewable-impression',
  INTERACTION_TIME: 'interaction-time',
  INTERACTION: 'interaction',
  INTERACTION_CLICK: 'interaction-click'
} as const;

export type EngagementEventName =
  typeof EngagementEvent[keyof typeof EngagementEvent];

export type NormalizedEvent = {
  type: EngagementType;
  name: EngagementEventName;
  schema: 'adset-creative';
  data: any;
  'adset-id': string;
  'creative-id': string;
  version?: number;
  'impression-id'?: string;
  'advertiser-id'?: string;
};
