import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import { registerInteractionTracker } from './interaction-tracker';
import { registerIntersectionObserver } from './intersection-observer';
import './viewable-impression';

let context: any;
let config: LemonpiEnrichedConfig;

function handleEngagementEvents() {
  // Template with engagement metrics enabled should contain
  // data-engagement-metrics="true" attribute
  const templateBody = document.getElementsByTagName('body')[0];
  const isEngagementMetricsEnabled =
    templateBody.hasAttribute('data-engagement-metrics') &&
    templateBody.getAttribute('data-engagement-metrics') === 'true';

  // Details of GDPR consent are not available yet
  const isGdprConsentGiven = () => {
    if (context['gdpr'] == 1 && context['gdpr-consent']) return true;
    else if (context['gdpr'] == 0 || context['gdpr'] == null) return true;
    else return false;
  };

  // Check if engagement metrics is in live env
  const isLiveTemplate = () => {
    if (
      config &&
      (config.creativeEnv === 'live' || config.creativeEnv === 'displayTag')
    )
      return true;
    else return false;
  };

  const trackEngagementMetrics =
    isEngagementMetricsEnabled && isGdprConsentGiven() && isLiveTemplate();

  registerIntersectionObserver(trackEngagementMetrics);

  if (trackEngagementMetrics) {
    registerInteractionTracker();
  }
}

window.addEventListener(
  'lemonpi.interaction/impression',
  handleEngagementEvents
);

window.addEventListener('lemonpi.context/ready', (event: CustomEvent) => {
  context = event.detail;
});

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
});
