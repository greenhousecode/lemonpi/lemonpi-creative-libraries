import { calculateRoundedTimeDiff } from '../../_utils/src/calculateRoundedTimeDiff';
import { sendEngagementData } from './dispatch-event';
import { EngagementEvent } from './engament-metrics.types';

const targetElement =
  window.frameElement || document.getElementsByTagName('body')[0];

const interactionPath: Record<string, number> = {};
let interactionTimestamp: number | null = null;

function isTouchDevice() {
  return 'ontouchstart' in window || navigator.maxTouchPoints;
}

// Throttle function to limit the number of events fired
export function throttle(cb: (...args: any[]) => void, delay: number) {
  let wait = false;
  return (...args: any[]) => {
    if (wait) {
      return;
    }
    cb(...args);
    wait = true;
    setTimeout(() => {
      wait = false;
    }, delay);
  };
}

function handleMouseHover(e: MouseEvent) {
  const key = `${e.offsetX},${e.offsetY}`;
  if (interactionPath[key]) {
    interactionPath[key] += 1;
  } else {
    interactionPath[key] = 1;
  }
}

const throttledHandleMouseHover = throttle(handleMouseHover, 100);

function handleMouseEnter() {
  interactionTimestamp = Date.now();
  document.addEventListener('mousemove', throttledHandleMouseHover);
}

function handleMouseLeave() {
  const interactionDuration = calculateRoundedTimeDiff(
    Date.now(),
    interactionTimestamp
  );
  document.removeEventListener('mousemove', throttledHandleMouseHover);
  sendEngagementData(EngagementEvent.INTERACTION, {
    interactionPath,
    interactionDuration,
    type: 'mouse-event'
  });
  interactionTimestamp = null;
}

function handleTouchMove(e: TouchEvent) {
  const touch = e.touches[0] || e.changedTouches[0];
  const key = `${touch.pageX},${touch.pageY}`;
  if (interactionPath[key]) {
    interactionPath[key] += 1;
  } else {
    interactionPath[key] = 1;
  }
}

const throttledHandleTouchMove = throttle(handleTouchMove, 100);

function handleTouchStart() {
  interactionTimestamp = Date.now();
  document.addEventListener('touchmove', throttledHandleTouchMove);
}

function handleTouchEnd() {
  const interactionDuration = calculateRoundedTimeDiff(
    Date.now(),
    interactionTimestamp
  );
  document.removeEventListener('touchmove', throttledHandleTouchMove);
  sendEngagementData(EngagementEvent.INTERACTION, {
    interactionPath,
    interactionDuration,
    type: 'touch-event'
  });
  interactionTimestamp = null;
}

export function registerInteractionTracker() {
  if (isTouchDevice()) {
    targetElement.addEventListener('touchstart', handleTouchStart);
    targetElement.addEventListener('touchend', handleTouchEnd);
  } else {
    targetElement.addEventListener('mouseenter', handleMouseEnter);
    targetElement.addEventListener('mouseleave', handleMouseLeave);
  }
}
