import {
  MACRO_EVENT,
  replaceMacro
} from '../../interaction-handler/src/interaction-handler';
import {
  EngagementEvent,
  EngagementEventName,
  Engagement,
  NormalizedEvent
} from './engament-metrics.types';

let config: any;

const enrichEventData = (data: any) => {
  if (
    config &&
    config.version == 2 &&
    config.impressionId &&
    config.advertiserId &&
    config.creativeRevisionId &&
    config.adsetId
  ) {
    return {
      ...data,
      'impression-id': config.impressionId,
      'advertiser-id': config.advertiserId,
      'creative-revision-id': config.creativeRevisionId,
      'adset-id': config.adsetId,
      'creative-id': config.creativeId,
      version: config.version
    };
  } else if (
    config &&
    config.interactionHandlerUrl &&
    config.adsetId &&
    config.creativeId
  ) {
    return {
      ...data,
      'adset-id': config.adsetId,
      'creative-id': config.creativeId
    };
  }
};

type IntersectionEvent = Pick<NormalizedEvent, 'type' | 'name' | 'data'>;

export const encodeData = (data: IntersectionEvent) => {
  const enrichedData = enrichEventData(data);
  const url = replaceMacro(
    config.interactionHandlerUrl,
    MACRO_EVENT,
    JSON.stringify(enrichedData)
  );
  return url;
};

export const beaconBlobData = (data: IntersectionEvent) => {
  const enrichedData = enrichEventData(data);
  const url = config.interactionHandlerUrl;
  const blobData = new Blob([JSON.stringify(enrichedData)], {
    type: 'application/json'
  });
  return { url, blobData };
};

export const postEvent = (data: IntersectionEvent) => {
  const url = encodeData(data);
  const tag = document.createElement('img');
  tag.src = url;
};

export const sendEngagementData = (type: EngagementEventName, payload: any) => {
  switch (type) {
    case EngagementEvent.VIEWABILITY:
      return postEvent({
        type: Engagement.ENGAGEMENT,
        name: EngagementEvent.VIEWABILITY,
        data: {
          ...payload
        }
      });
    case EngagementEvent.INTERACTION_TIME: {
      return postEvent({
        type: Engagement.ENGAGEMENT,
        name: EngagementEvent.INTERACTION_TIME,
        data: {
          ...payload
        }
      });
    }
    case EngagementEvent.INTERACTION: {
      return postEvent({
        type: Engagement.ENGAGEMENT,
        name: EngagementEvent.INTERACTION,
        data: {
          ...payload
        }
      });
    }
    case EngagementEvent.INTERACTION_CLICK: {
      return postEvent({
        type: Engagement.ENGAGEMENT,
        name: EngagementEvent.INTERACTION_CLICK,
        data: {
          ...payload
        }
      });
    }
    default:
      return null;
  }
};

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
});
