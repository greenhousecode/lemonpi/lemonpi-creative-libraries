/**
 * Module for replacing content in the DOM with dynamic content from LemonPI.
 *
 * @module lemonpi-dom
 */

import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';

let previousStyleNode: HTMLStyleElement | null;

/**
 * Render placeholder of type `text`.
 */
function renderTextPlaceholder(key: any, placeholder: any, elements: any) {
  Array.prototype.forEach.call(elements, (element: any) => {
    element.textContent = placeholder.value;
  });
}

/**
 * Render placeholder of type `text`.
 */
function renderVideoPlaceholder(placeholder: any, elements: any) {
  Array.prototype.forEach.call(elements, (element: any) => {
    element.src = placeholder.value;
  });
}

/**
 * Render placeholder of type `image`.
 */
function renderImagePlaceholder(
  key: any,
  placeholder: any,
  elements: any,
  style: any
) {
  style.appendData(`
    .placeholder-${key} {
      background-image: url("${placeholder.value}");
    }
  `);

  Array.prototype.forEach.call(elements, (element: any) => {
    element.className = element.className
      .split(' ')
      .filter(
        (className: any) =>
          !!className &&
          className !== `placeholdertype-${placeholder.type}` &&
          className !== `placeholder-${key}`
      )
      .concat(['placeholdertype-' + placeholder.type, 'placeholder-' + key])
      .join(' ');
  });
}

/**
 * Render placeholder of type `click`.
 */
function renderClickPlaceholder(key: any, placeholder: any, elements: any) {
  Array.prototype.forEach.call(elements, (element: any) => {
    element.onclick = function () {
      dispatchCustomEvent('lemonpi.interaction/click', {
        placeholder: key,
        elementId: element.id,
        query: {}
      });
    };

    element.className = element.className
      .split(' ')
      .filter(
        (className: any) =>
          !!className &&
          className !== `placeholdertype-${placeholder.type}` &&
          className !== `placeholder-${key}`
      )
      .concat(['placeholdertype-' + placeholder.type, 'placeholder-' + key])
      .join(' ');
  });
}

/**
 * Create initial style text node.
 */
function initialStyle() {
  const style = document.createTextNode('\n');

  style.appendData(`
    .placeholdertype-click {
      cursor: pointer;
    }

    .placeholdertype-image {
      background-repeat: no-repeat;
      background-size: contain;
    }
  `);

  return style;
}

/**
 * Mapping from placeholder type to render function.
 */
const renderers = {
  text: renderTextPlaceholder,
  image: renderImagePlaceholder,
  click: renderClickPlaceholder,
  video: renderVideoPlaceholder
};

/**
 * Find DOM nodes with data-placeholder attributes and render their content
 * using the corresponding entries in the content object.
 *
 * @param  {module:lemonpi-content~content} content LemonPI content.
 * @return {undefined}
 */
function renderPlaceholders(content: any) {
  const style = initialStyle();

  Object.keys(content).forEach((key) => {
    const placeholder = content[key];
    const renderer = renderers[placeholder.type as keyof typeof renderers];

    if (renderer) {
      const domSelector = '[data-placeholder="' + key + '"]';
      const elements = document.querySelectorAll(domSelector);
      renderer(key, placeholder, elements, style);
    }
  });

  const styleNode = document.createElement('style');
  styleNode.type = 'text/css';
  styleNode.appendChild(style);
  document.head.insertBefore(styleNode, document.head.childNodes[0]);

  if (previousStyleNode) {
    previousStyleNode.parentNode.removeChild(previousStyleNode);
  }

  previousStyleNode = styleNode;
  window.requestAnimationFrame(() => {
    // Dispatch on the next animation frame to ensure the repaint caused by
    // replacing the placeholders has finished.
    dispatchCustomEvent('lemonpi.content/rendered', undefined);
  });
}

window.addEventListener('lemonpi.content/ready', (event: CustomEvent) => {
  if (!event.detail || !event.detail.content) {
    return;
  }

  window.requestAnimationFrame(() => {
    // Replace placeholders on the next animation frame to ensure the current
    // repaint has finished. This guarantees the next repaint contains the
    // changes caused by `renderPlaceholders`.
    renderPlaceholders(event.detail.content);
  });
});
