beforeEach(() => {
  jest.resetModules();
  document.head.innerHTML = '';
  document.body.innerHTML = '';
  window.requestAnimationFrame = handler => handler()

  require('../lemonpi-dom');
});

describe('DOM module', () => {
  test('renders nothing when content is undefined', () => {
    document.body.innerHTML = `
      <div data-placeholder="image1"></div>
      <div data-placeholder="text1"></div>
      <div data-placeholder="click1"></div>
    `;

    window.dispatchEvent(new CustomEvent('lemonpi.content/ready'));

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();
  });

  test('ignores unknown placeholder types', () => {
    document.body.innerHTML = `
      <div data-placeholder="text1"></div>
    `;

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', {
        detail: { content: { text1: { type: 'non-existing', value: 'abc' } } }
      })
    );

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();
  });

  test('renders text placeholders', () => {
    document.body.innerHTML = `
      <div data-placeholder="text1" class="existing">1</div>
      <div data-placeholder="text2">2</div>
      <div data-placeholder="text3">3</div>
    `;

    const content = {
      text1: { type: 'text', value: '1-replaced' },
      text2: { type: 'text', value: '2-replaced' }
    };

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', { detail: { content } })
    );

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();
  });

  test('renders video placeholders', () => {
    document.body.innerHTML = `
      <video data-placeholder="video1"></video>
      <video data-placeholder="video1" src="./sample/video.mp4"></video>
      <video data-placeholder="video2" src="./sample/video.mp4"></video>
    `;

    const content = {
      video1: { type: 'video', value: './some-video.mp4' }
    };

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', { detail: { content } })
    );

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();
  });

  test('renders image placeholders', () => {
    document.body.innerHTML = `
      <div data-placeholder="image1" class="existing"></div>
      <div data-placeholder="image2"></div>
      <div data-placeholder="image3"></div>
    `;

    const content = {
      image1: { type: 'image', value: 'url/i1' },
      image2: { type: 'image', value: 'url/i2' }
    };

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', { detail: { content } })
    );

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();
  });

  test('renders click placeholders that dispatch interaction/click', () => {
    const handler = jest.fn();
    window.addEventListener('lemonpi.interaction/click', handler);

    document.body.innerHTML = `
      <div data-placeholder="click1" id="existing" class="existing"></div>
      <div data-placeholder="click2"></div>
      <div data-placeholder="click3"></div>
    `;

    const content = {
      click1: { type: 'click', value: 'url/c1' },
      click2: { type: 'click', value: 'url/c2' }
    };

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', { detail: { content } })
    );

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();

    document.querySelector('[data-placeholder="click1"]').click();
    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { placeholder: 'click1', elementId: 'existing', query: {} }
      })
    );
  });

  test('removes the generated style block when rendering multiple times', () => {
    document.body.innerHTML = `
      <div id="image_1" data-placeholder="image1"></div>
    `;

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', {
        detail: { content: { image1: { type: 'image', value: 'url/i1' } } }
      })
    );
    expect(document.head.innerHTML).toMatchSnapshot();

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', { detail: { content: {} } })
    );
    expect(document.head.innerHTML).toMatchSnapshot();
  });

  test('emits content/rendered', () => {
    const handler = jest.fn();
    window.addEventListener('lemonpi.content/rendered', handler);

    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', {
        detail: { content: { image1: { type: 'image', value: 'url/i1' } } }
      })
    );
    expect(handler).toHaveBeenCalled();
  });
  test('supports numeric placeholder names', () => {
    document.body.innerHTML = `
      <div data-placeholder="200"></div>
    `;
    let content = { 200: { type: 'text', value: 'cta' } };
    window.dispatchEvent(
      new CustomEvent('lemonpi.content/ready', { detail: { content } })
    );

    expect(document.head.innerHTML).toMatchSnapshot();
    expect(document.body.innerHTML).toMatchSnapshot();
  });
});
