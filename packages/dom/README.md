# lemonpi-dom

This package is part of the lemonpi-creative-libraries. These libraries can be used in creative templates. This particular package's responsibility is to replace content for DOM-elements according to their placeholder value.
