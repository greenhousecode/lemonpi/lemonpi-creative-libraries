/**
 * Preload image
 *
 * @param {string} url
 * @return {Promise<void>}
 */
export default function preloadImage(url: string): Promise<any> {
  return new Promise((resolve) => {
    const img = new Image();
    img.onload = resolve;
    img.src = url;
  });
}
