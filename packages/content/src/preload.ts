import { ObjectEntries, UnknownObject } from '../../../types/utility.types';
import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import fetchMediaBlobObjectUrl from './fetch-media-blob-object-url';
import loadImage from './load-image';

/**
 * Preload fonts
 *
 * @return {Promise<boolean>}
 */
function preloadFonts() {
  return document.fonts.ready;
}

/**
 * Filter entries (content) by given predicate
 *
 * @param {Array<[string, any]>} entries
 * @param {(value: object) => boolean} pred
 * @return {Array<[string, any]>}
 */

function filterContent(
  entries: ObjectEntries,
  pred: (arg: UnknownObject) => boolean
) {
  return entries.filter(([, value]) => pred(value));
}

const mapContentObject = {
  /**
   * Append blob value to content object
   *
   * @param {Array<string, object>} param0
   * @return {Promise<[string, object]>}
   */
  appendBlobValue([id, contentObject]: [string, any]): Promise<
    [string, UnknownObject]
  > {
    return new Promise((resolve, reject) => {
      fetchMediaBlobObjectUrl(contentObject.value)
        .then((value) => resolve([id, { ...contentObject, value }]))
        .catch(reject);
    });
  },

  /**
   * Load image from content object
   *
   * @param {Array<string, object>} param0
   * @return {Promise<void>}
   */
  loadImage([, contentObject]: [string, any]): Promise<any> {
    return loadImage(contentObject.value);
  }
};

const ContentObjectPredicates = {
  isMedia: ({ type }: { type: string }) => ['audio', 'video'].includes(type),
  isImage: ({ type }: { type: string }) => type === 'image'
};

/**
 * Preload assets for DCP
 *
 * @param {object} content
 * @param {object} config
 * @return {Promise<void>}
 */

export default function preload(
  content: UnknownObject,
  config: LemonpiEnrichedConfig
): Promise<UnknownObject> {
  return new Promise((resolve, reject) => {
    // prevent non-dcp to preload assets
    if (!config.preloadAssetsInContent) {
      return resolve({ content, config });
    }

    const contentEntries = Object.entries(content.content);

    const { appendBlobValue, loadImage } = mapContentObject;
    const { isMedia, isImage } = ContentObjectPredicates;

    const mediaContent = filterContent(contentEntries, isMedia);
    const imageContent = filterContent(contentEntries, isImage);

    const mediaPromises = () => Promise.all(mediaContent.map(appendBlobValue));
    const imagePromises = () => Promise.all(imageContent.map(loadImage));

    Promise.all([mediaPromises(), imagePromises(), preloadFonts()])
      .then(([mediaEntries]) => {
        // sanitized content where media entries
        // have a blob object url as value
        const sanitizedContent = {
          ...content,
          content: { ...content.content, ...Object.fromEntries(mediaEntries) }
        };

        resolve({ content: sanitizedContent, config });
      })
      .catch(reject);
  });
}

let config: LemonpiEnrichedConfig | undefined;

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
});

window.addEventListener('lemonpi.content/fetched', (event: CustomEvent) => {
  if (config) {
    preload(event.detail, config)
      .then((data) =>
        dispatchCustomEvent('lemonpi.content/ready', data.content)
      )
      .catch((err) =>
        dispatchCustomEvent('lemonpi.content/preload-error', err)
      );
  }
});
