let content: any;

let handler: any;
let xhrInstances: any;
let XMLHttpRequestCopy: any;

jest.useFakeTimers();

class MockXMLRequest extends XMLHttpRequest {
  public open = jest.fn();
  public send = jest.fn();
  public readyState = 0;
  public status = 2;
  public responseText = '';
  public setRequestHeader = jest.fn();
}
const mockXMLHttp = () => {
  xhrInstances = [];
  XMLHttpRequestCopy = global.XMLHttpRequest;
  jest.spyOn(global, 'XMLHttpRequest').mockImplementation(() => {
    const mockXMLHttpRequest = new MockXMLRequest();
    xhrInstances.push(mockXMLHttpRequest);
    return mockXMLHttpRequest;
  });
};

beforeEach(() => {
  jest.resetModules();
  mockXMLHttp();

  handler = jest.fn();
  window.addEventListener('lemonpi.content/fetched', handler);
  content = require('../lemonpi-content');
});

afterEach(() => {
  global.XMLHttpRequest = XMLHttpRequestCopy;
  jest.clearAllTimers();
});

describe('Content module dispatches content/fetched event', () => {
  test('when no content can be loaded', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', { detail: {} })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', { detail: {} })
    );

    jest.advanceTimersByTime(0);
    expect(handler.mock.calls[0][0].detail).toEqual(
      expect.objectContaining({
        source: undefined,
        content: undefined
      })
    );
  });

  test('when only dynamic content is configured', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { dynamicContentURL: 'dynamic', creativeRevisionId: 42 }
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
    );

    expect(xhrInstances[0].send).toHaveBeenCalledWith(
      JSON.stringify({
        context: {
          k: 'v',
          $request: {}
        }
      })
    );

    xhrInstances[0].readyState = XMLHttpRequest.DONE;
    xhrInstances[0].status = 200;
    xhrInstances[0].responseText = JSON.stringify({ k: 'dynamic' });
    xhrInstances[0].onreadystatechange();

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: {
          source: content.sources.DYNAMIC,
          content: { k: 'dynamic' }
        }
      })
    );
  });

  test('when only default content is configured', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { defaultContentURL: 'default', creativeRevisionId: 42 }
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
    );

    xhrInstances[0].readyState = XMLHttpRequest.DONE;
    xhrInstances[0].status = 200;
    xhrInstances[0].responseText = JSON.stringify({ k: 'default' });
    xhrInstances[0].onreadystatechange();

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: {
          source: content.sources.DEFAULT,
          content: { k: 'default' }
        }
      })
    );
  });

  test('when only export content is configured', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { exportContent: { k: 'export' } }
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { source: content.sources.EXPORT, content: { k: 'export' } }
      })
    );
  });

  describe('with lemonpi content', () => {
    test('no xhr requests are performed and content is provided as it is', () => {
      const config: any = {
        staticContent: true,
        dynamicContentURL: 'dynamic',
        creativeRevisionId: 42,
        lemonpiContent: { k: 'export' }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
      );

      jest.advanceTimersByTime(0);

      expect(xhrInstances).toHaveLength(0);

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: { source: content.sources.EXPORT, content: { k: 'export' } }
        })
      );
    });
  });

  describe('with export content if alternatives fail', () => {
    test('dynamic/default URLs not specified', () => {
      const config: any = {
        staticContent: true,
        dynamicContentURL: null,
        defaultContentURL: null,
        exportContent: { k: 'export' }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
      );

      jest.advanceTimersByTime(0);

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: { source: content.sources.EXPORT, content: { k: 'export' } }
        })
      );
    });

    test('because of bad response', () => {
      const config = {
        dynamicContentURL: 'dynamic',
        defaultContentURL: 'default',
        exportContent: { k: 'export' },
        creativeRevisionId: 42
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
      );

      xhrInstances[0].readyState = XMLHttpRequest.DONE;
      xhrInstances[0].status = 404;
      xhrInstances[0].onreadystatechange();

      xhrInstances[1].readyState = XMLHttpRequest.DONE;
      xhrInstances[1].status = 404;
      xhrInstances[1].onreadystatechange();

      jest.advanceTimersByTime(0);

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: { source: content.sources.EXPORT, content: { k: 'export' } }
        })
      );
    });

    test('because of timeouts', () => {
      const config = {
        DYNAMIC_CONTENT_TIMEOUT_MS: 2000,
        DEFAULT_CONTENT_TIMEOUT_MS: 2000,
        dynamicContentURL: 'dynamic',
        defaultContentURL: 'default',
        exportContent: { k: 'export' },
        creativeRevisionId: 42
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
      );

      jest.advanceTimersByTime(4000);
      expect(handler).not.toHaveBeenCalled();

      jest.advanceTimersByTime(1);
      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: { source: content.sources.EXPORT, content: { k: 'export' } }
        })
      );
    });
  });

  describe('with default content if alternatives fail', () => {
    test('because of bad response', () => {
      const config = {
        dynamicContentURL: 'dynamic',
        defaultContentURL: 'default',
        exportContent: {},
        creativeRevisionId: 42
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
      );

      xhrInstances[0].readyState = XMLHttpRequest.DONE;
      xhrInstances[0].status = 404;
      xhrInstances[0].onreadystatechange();

      xhrInstances[1].readyState = XMLHttpRequest.DONE;
      xhrInstances[1].responseText = JSON.stringify({ k: 'default' });
      xhrInstances[1].status = 200;
      xhrInstances[1].onreadystatechange();

      jest.advanceTimersByTime(0);

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: { source: content.sources.DEFAULT, content: { k: 'default' } }
        })
      );
    });

    test('because of timeouts', () => {
      const config = {
        DYNAMIC_CONTENT_TIMEOUT_MS: 2000,
        DEFAULT_CONTENT_TIMEOUT_MS: 2000,
        dynamicContentURL: 'dynamic',
        defaultContentURL: 'default',
        exportContent: {},
        creativeRevisionId: 42
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
      );

      jest.advanceTimersByTime(2000);
      xhrInstances[0].readyState = XMLHttpRequest.LOADING;
      xhrInstances[0].onreadystatechange();
      expect(handler).not.toHaveBeenCalled();

      jest.advanceTimersByTime(1);
      xhrInstances[1].readyState = XMLHttpRequest.DONE;
      xhrInstances[1].responseText = JSON.stringify({ k: 'default' });
      xhrInstances[1].status = 200;
      xhrInstances[1].onreadystatechange();

      jest.advanceTimersByTime(0);

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: { source: content.sources.DEFAULT, content: { k: 'default' } }
        })
      );

      // Test whether the defaultContent doesn't get loaded if dynamic
      // eventually responds with an error.
      xhrInstances[0].readyState = XMLHttpRequest.DONE;
      xhrInstances[0].onreadystatechange();

      expect(xhrInstances.length).toBe(2);
    });
  });

  test('with dynamic content if it succeeds after starting the alternative', () => {
    const config = {
      DYNAMIC_CONTENT_TIMEOUT_MS: 2000,
      DEFAULT_CONTENT_TIMEOUT_MS: 2000,
      dynamicContentURL: 'dynamic',
      defaultContentURL: 'default',
      exportContent: {},
      creativeRevisionId: 42
    };

    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', { detail: config })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', { detail: { k: 'v' } })
    );

    xhrInstances[0].readyState = XMLHttpRequest.DONE;
    xhrInstances[0].responseText = JSON.stringify({ k: 'dynamic' });
    xhrInstances[0].status = 200;

    jest.advanceTimersByTime(2000);
    // Dynamic timed out, alternative started loading
    expect(xhrInstances[1]).not.toBe(undefined);

    // dynamic is finished
    xhrInstances[0].onreadystatechange();

    // alternative is also finished
    xhrInstances[1].readyState = XMLHttpRequest.DONE;
    xhrInstances[1].responseText = JSON.stringify({ k: 'default' });
    xhrInstances[1].status = 200;
    xhrInstances[1].onreadystatechange();

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { source: content.sources.DYNAMIC, content: { k: 'dynamic' } }
      })
    );

    // only dispatches 1 content event
    expect(handler.mock.calls.length).toEqual(1);
  });
});
