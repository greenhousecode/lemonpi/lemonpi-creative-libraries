// polyfill (current CI runs node 8.9.1)
if (!Object.fromEntries) {
  Object.fromEntries = (entries: any[]) => {
    return entries.reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {});
  };
}

const dispatchCustomEvent = jest.requireActual(
  '../../../bundle/src/dispatch-event'
).dispatchCustomEvent;

// mock for catching the `dispatchCustomEvent`
const dispatchCustomEventMock = jest.fn((...args) =>
  dispatchCustomEvent(...args)
);

// mock for catching the `loadImage`
const loadImageMock = jest.fn().mockReturnValue(Promise.resolve());

// mock for catching `fetchMediaBlobObjectUrl`
const fetchMediaBlobObjectUrlMock = jest
  .fn()
  .mockReturnValue(Promise.resolve('blob://some-blob-url'));

let preload: any;

beforeEach(() => {
  jest.resetModules();

  jest.doMock('../../../bundle/src/dispatch-event', () => ({
    dispatchCustomEvent: dispatchCustomEventMock
  }));

  jest.doMock('../load-image', () => ({
    __esModule: true,
    default: loadImageMock
  }));

  jest.doMock('../fetch-media-blob-object-url', () => ({
    __esModule: true,
    default: fetchMediaBlobObjectUrlMock
  }));

  preload = require('../preload');
  Object.defineProperty(document, 'fonts', {
    value: { ready: Promise.resolve() },
    writable: true
  });
});

describe('preload module dispatches content/ready', () => {
  test('when preloadAssetsInContent is falsy', () => {
    return new Promise<void>((done) => {
      const expected = {
        source: '',
        content: {
          img: { type: 'image', value: 'https://assets.lemonpi.io/a.png' }
        }
      };

      dispatchCustomEvent('lemonpi.config/ready', {});
      dispatchCustomEvent('lemonpi.content/fetched', expected);

      process.nextTick(() => {
        expect(dispatchCustomEventMock).toHaveBeenLastCalledWith(
          'lemonpi.content/ready',
          expected
        );
        done();
      });
    });
  });

  test('when there is nothing to preload', () => {
    return new Promise<void>((done) => {
      const expected = { content: {}, source: '' };
      const config = { preloadAssetsInContent: true };

      dispatchCustomEvent('lemonpi.config/ready', config);
      dispatchCustomEvent('lemonpi.content/fetched', expected);

      process.nextTick(() => {
        expect(dispatchCustomEventMock).toHaveBeenCalledWith(
          'lemonpi.content/ready',
          expected
        );
        done();
      });
    });
  });

  test('images are being preloaded when preloadAssetsInContent is true', async () => {
    const config = { preloadAssetsInContent: true };
    const content = {
      source: '',
      content: {
        img: { type: 'image', value: 'https://assets.lemonpi.io/a.png' }
      }
    };
    await preload.default(content, config);
    expect(loadImageMock).toHaveBeenCalledWith(
      'https://assets.lemonpi.io/a.png'
    );
  });

  test('media content contains blob values', async () => {
    const config = { preloadAssetsInContent: true };
    const content = {
      source: '',
      content: {
        video: { type: 'video', value: 'https://assets.lemonpi.io/a.mp4' }
      }
    };

    expect(await preload.default(content, config)).toHaveProperty(
      'content.content.video.value',
      'blob://some-blob-url'
    );
  });
});

describe('preload module dispatches preload-errror', () => {
  test('preload error when could not fetch blob data for media content', async () => {
    const expectedError = new Error('blob error');
    fetchMediaBlobObjectUrlMock.mockReturnValue(Promise.reject(expectedError));

    const config = { preloadAssetsInContent: true };
    const content = {
      source: '',
      content: {
        video: { type: 'video', value: 'https://assets.lemonpi.io/a.mp4' }
      }
    };

    await expect(preload.default(content, config)).rejects.toEqual(
      expectedError
    );
  });
});
