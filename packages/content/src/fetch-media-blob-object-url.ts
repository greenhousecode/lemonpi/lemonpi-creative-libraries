/**
 * Fetch media blob and convert it into an object url
 *
 * @param {string} url
 * @return {Promise<string>}
 */
export default function fetchMediaBlobObjectUrl(url: string): Promise<string> {
  return new Promise((resolve, reject) => {
    return fetch(url)
      .then((response: Response) => {
        if (!response.ok) throw new Error(`${response.status}`);
        return response.blob();
      })
      .then((blob: Blob) => resolve(URL.createObjectURL(blob)))
      .catch(reject);
  });
}
