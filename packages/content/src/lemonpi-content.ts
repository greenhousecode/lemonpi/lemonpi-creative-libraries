/**
 * Module for resolving dynamic content from the LemonPI platform. This module
 * utilizes a fallback mechanism for when the LemonPI platform is not responding
 * in a timely manner. It will attempt to get content from increasingly more
 * reliable yet less relevant sources.
 *
 * @module lemonpi-content
 */

import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import { tryCatchWrapper } from '../../_utils/src/tryCatchWrapper';
import { ContentSources } from './lemonpi-content.types';
import { convertGoogleCmToGeneric } from '../../_utils/src/macros';
import nativeDataStore from '../../native-data-store/src/native-data-store';
import {
  handleOmidImpressionEvent,
  handleOmidSessionStart,
  isOmidSupported
} from '../../omsdk/src/omsdk';

/**
 * Object containing content to be used in the creative. Every key corresponds
 * to a placeholder. The value is a description of the content for this
 * placeholder.
 *
 * @typedef {Object} module:lemonpi-content~content
 * @property {Text|Image|Click|ClickTracker|ImpressionTracker} [placeholder-name] placeholder content
 *
 * @example
 * {
 *   cta: { type: "text", value: "Act fast!" },
 *   product: { type: "image", value: "https://creative-content.lemonpi.io/assets/1234/abcd" },
 *   landing: { type: "click", value: "https://lemonpi.io" }
 * }
 */

let config: LemonpiEnrichedConfig;
let loading: boolean;

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
});

const wrappedGetDynamicContent = tryCatchWrapper(getDynamicContent);
window.addEventListener('lemonpi.context/ready', (event: CustomEvent) => {
  if (!config) {
    return;
  }

  loading = true;

  if (isOmidSupported()) {
    handleOmidSessionStart();
  }

  wrappedGetDynamicContent(config, event.detail);
});

function contentLoaded(source: any, content: any) {
  if (!loading) {
    return;
  }

  loading = false;

  if (isOmidSupported()) {
    return handleOmidImpressionEvent(source, content);
  }
  window.setTimeout(() => {
    dispatchCustomEvent('lemonpi.content/fetched', { source, content });
    dispatchCustomEvent('lemonpi.interaction/impression', undefined);
  }, 0);
}

function makeRequest(
  url: string,
  method: string,
  context: Record<string, any>,
  impressionId: string,
  queryParams: any,
  timeoutMs: number,
  error: () => void,
  success: (data: any) => void
) {
  let handledError = false;
  const timeout = setTimeout(() => {
    handledError = true;
    error();
  }, timeoutMs);
  const xhr = new XMLHttpRequest();

  let extendedUrl = url;

  let allQueryParams = queryParams || {};

  if (impressionId) {
    allQueryParams = Object.assign(allQueryParams, {
      'impression-id': impressionId
    });
  }

  if (Object.keys(allQueryParams).length > 0) {
    const queryStr = Object.entries(allQueryParams)
      .map(([k, v]) => k + '=' + v)
      .join('&');
    extendedUrl += '?' + queryStr;
  }

  xhr.open(method, extendedUrl, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      clearTimeout(timeout);

      if (xhr.status !== 200) {
        if (!handledError) {
          return error();
        }
        return;
      }

      success(JSON.parse(xhr.responseText));
    }
  };
  if (method === 'POST') {
    const contextRequestWithNativeData = {
      ...context.$request,
      ...nativeDataStore.getData()
    };
    context.$request = contextRequestWithNativeData;
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({ context }));
  } else {
    xhr.send();
  }
}

// works with nulls
function isEmpty(obj: any) {
  for (const prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      return false;
    }
  }

  return true;
}

function getExportContent(config: LemonpiEnrichedConfig) {
  if (config.lemonpiContent) {
    contentLoaded(ContentSources.EXPORT, config.lemonpiContent);
  } else if (config.exportContent) {
    contentLoaded(ContentSources.EXPORT, config.exportContent);
  } else {
    contentLoaded(undefined, undefined);
  }
}

function getDefaultContent(config: LemonpiEnrichedConfig, context: any) {
  if (!config.defaultContentURL || !isEmpty(config.lemonpiContent)) {
    return getExportContent(config);
  }

  makeRequest(
    config.defaultContentURL,
    'GET',
    context,
    config.impressionId,
    {},
    config.DEFAULT_CONTENT_TIMEOUT_MS,
    getExportContent.bind(null, config),
    contentLoaded.bind(null, ContentSources.DEFAULT)
  );
}

function extractGoogleCmAsGenericMacros(config: any, context: any) {
  if (config.adserver?.code === 'google-cm') {
    const googleCmMacros = context['google-cm'] || {};
    return convertGoogleCmToGeneric(googleCmMacros);
  }
  return {};
}

function getDynamicContent(config: LemonpiEnrichedConfig, context: any) {
  if (
    !config.dynamicContentURL ||
    !config.creativeRevisionId ||
    !isEmpty(config.lemonpiContent)
  ) {
    return getDefaultContent(config, context);
  }

  const macros = extractGoogleCmAsGenericMacros(config, context);

  makeRequest(
    config.dynamicContentURL,
    'POST',
    context,
    config.impressionId,
    macros,
    config.DYNAMIC_CONTENT_TIMEOUT_MS,
    getDefaultContent.bind(null, config),
    contentLoaded.bind(null, ContentSources.DYNAMIC)
  );
}

/**
 * Definition of the different sources from which dynamic content can be
 * retrieved. We will attempt to retrieve content in the following order
 *
 * <ul>
 *  <li>Dynamic - Retrieve content from LemonPI. Content is based on the user's context</li>
 *  <li>Default - Retrieve the latest configured context independent content from S3</li>
 *  <li>Export - Retrieve context independent content baked into the creative when exported to the adserver</li>
 * </ul>
 *
 * @access public
 * @type {Object}
 */
export const sources = {
  EXPORT: 'export',
  DEFAULT: 'default',
  DYNAMIC: 'dynamic'
};
