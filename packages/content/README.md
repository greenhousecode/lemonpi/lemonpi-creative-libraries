# lemonpi-content

This package is part of the lemonpi-creative-libraries. These libraries can be used in creative templates. This particular package's responsibility is to load content based on business rules:
* Try to get dynamic content
* Try to get latest default content
* Get content as configured upon export
