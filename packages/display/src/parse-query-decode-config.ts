/**
 * Module for parsing query and decoding config from query string.
 * {@link module:parse-query-decode-config~QueryConfig}
 * @module parse-query-decode-config
 */
/**
 * @typedef {Object} QueryConfig
 * @property {number} advertiserId
 * @property {number} version
 * @property {{code : string}} adserver
 * @property {number} creativeRevisionId
 * @property {number} campaignId
 * @property {number} creativeId
 * @property {number} adsetId
 * @property {string} environment
 */

/**
 * Parse configuration from script query.
 * @param {string} search Query string from the current script src
 * @return {{...params , config : QueryConfig | null}}
 *
 **/
export default function parseQueryAndDecodeConfig(search: string) {
  const parseToJson = () => {
    try {
      const prepareQuerySearch = search
        .substring(1)
        .split('&')
        .map((singleQuery) => {
          const [key, ...value] = singleQuery.split('=');
          return [key, value.join('')];
        });
      const parsedToJson = Object.fromEntries(prepareQuerySearch);
      return parsedToJson;
    } catch (e) {
      return null;
    }
  };
  const scriptConfigParam = parseToJson();
  if (scriptConfigParam && scriptConfigParam.config) {
    scriptConfigParam.config = JSON.parse(
      decodeURIComponent(escape(atob(scriptConfigParam.config)))
    );

    return scriptConfigParam;
  }
  throw Error('Not valid url configuration');
}
