import parseQueryAndDecodeConfig from '../parse-query-decode-config';

describe('Parse query and decode config', () => {
  test('Parses the valid query and decodes the config', () => {
    const validQuery = "?width=600&height=500&appnexus.AUCTION_ID=123&appnexus.LEMONPI=123&appnexus.ADV_ID=123&appnexus.SSP_DATA=123&appnexus.GEO_LAT=123&appnexus.GEO_LO=123&config=eyJhZHZlcnRpc2VySWQiOjEsInZlcnNpb24iOjEsImFkc2VydmVyIjp7ImNvZGUiOiJhcHBuZXh1cyJ9LCJjcmVhdGl2ZVJldmlzaW9uSWQiOjEsImNhbXBhaWduSWQiOjEsImNyZWF0aXZlSWQiOjEsImFkc2V0SWQiOjEsImVudmlyb25tZW50IjoicHJvZCJ9"  
    const parsed = {
      "appnexus.AUCTION_ID": '123',
      "appnexus.LEMONPI": '123',
      width: '600',
      height: '500',
      "appnexus.ADV_ID": '123',
      "appnexus.SSP_DATA": '123',
      "appnexus.GEO_LAT": '123',
      "appnexus.GEO_LO": '123',
            config: {
              advertiserId: 1,
              version: 1,
              adserver: { code: 'appnexus' },
              creativeRevisionId: 1,
              campaignId: 1,
              creativeId: 1,
              adsetId: 1,
              environment: 'prod'
            }     
          }
    expect(parseQueryAndDecodeConfig(validQuery)).toEqual(parsed);
  });
  
  test('Throw error trying to parse invalid query', () => {
    const invalidQuery = "?width=600&height=500&AUCTION_ID=123&LEMONPI=123&ADV_ID=123&SSP_DATA=123&GEO_LAT=123&GEO_LO=123"
    expect(() => parseQueryAndDecodeConfig(invalidQuery)).toThrow("Not valid url configuration");
  });

  test('Throw error trying to parse no query', () => {
    expect(() => parseQueryAndDecodeConfig("")).toThrow("Not valid url configuration");
  });
});
