/**
 * Google-CM context enricher.
 *
 * @module google-context
 */

import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import nativeDataStore from '../../native-data-store/src/native-data-store';

const CHECK_INTERVAL = 1;
const MACRO_START = 'LEMONPI%MACROS%START';
const MACRO_END = 'LEMONPI%MACROS%END';

/**
 * Recursively decode string until it cannot be decoded any further.
 *
 * @param {text} string String to be decoded
 */
function decodeString(text: string) {
  let previouslyDecoded = text;
  try {
    // eslint-disable-next-line no-constant-condition
    while (true) {
      const currentDecoded = decodeURIComponent(previouslyDecoded);
      if (currentDecoded === previouslyDecoded) {
        break;
      }

      previouslyDecoded = currentDecoded;
    }
  } catch (err) {
    //cannot decode anymore
  }

  return previouslyDecoded;
}

/**
 * Gets a context param from google geo string
 *
 * @param {contextString} google geo string
 * @param {name} name of the context param
 */
function contextParam(contextString: string, name: string) {
  const results = new RegExp('[&]' + name + '=([^&#]*)').exec(contextString);
  if (results == null) {
    return null;
  } else {
    return results[1];
  }
}

export default function enrich(
  config: LemonpiEnrichedConfig,
  cb: CallbackFunction,
  n = 0
): void {
  if (!window.clickTag2) {
    if (CHECK_INTERVAL * n >= config.CLIENT_CONTEXT_TIMEOUT_MS) {
      return cb('google-cm', {});
    }
    setTimeout(() => enrich(config, cb, n + 1), CHECK_INTERVAL);
    return;
  }

  const decoded = decodeString(window.clickTag2);
  const startIndex = decoded.indexOf(MACRO_START);
  const endIndex = decoded.indexOf(MACRO_END);

  if (startIndex === -1 || endIndex === -1) {
    return cb('google-cm', {});
  }

  const macroFragment = decoded.slice(
    startIndex + MACRO_START.length,
    endIndex
  );

  try {
    const context = JSON.parse(macroFragment);
    if (context.BUNDLE_ID) {
      nativeDataStore.dispatchPartialData({
        app_bundle_id: context.BUNDLE_ID
      });
    }
    const geoContext = context.geo
      ? {
          'geo-city': contextParam(context.geo, 'city'),
          'geo-state': contextParam(context.geo, 'st'),
          'geo-zip': contextParam(context.geo, 'zp')
        }
      : {};
    const contextWithGeo = {
      ...context,
      ...geoContext
    };
    cb('google-cm', contextWithGeo);
  } catch (err) {
    cb('google-cm', {});
  }
}
