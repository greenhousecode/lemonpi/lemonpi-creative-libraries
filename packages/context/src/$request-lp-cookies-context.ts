import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

let cookies = {};

window._lemonpiCookiesLoaded = function (result) {
  cookies = result;
};

function enrich(config: LemonpiEnrichedConfig, cb: CallbackFunction) {
  const tag = document.createElement('script');

  tag.onload = function () {
    cb('$request', cookies);
  };

  tag.src = config.lemonpiCookiesUrl + '?callback=_lemonpiCookiesLoaded';
  document.body.appendChild(tag);
}

export default enrich;
