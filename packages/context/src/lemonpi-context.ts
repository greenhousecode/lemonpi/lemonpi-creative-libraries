/**
 * Module for collecting client side context. This context is used to retrieve
 * more relevant content
 *
 * @module lemonpi-context
 */

import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';
import $requestTimestamp from './$request-timestamp-context';
import $requestLemonpiCookies from './$request-lp-cookies-context';
import $requestGdpr from './$request-gdpr-context';
import queryParameterContext from './query-parameter-context';
import adform from './adform-context';
import appnexus from './appnexus-context';
import google from './google-context';
import tradedesk from './trade-desk-context';
import displayLemonpi from './display-lemonpi-context';
import { UnknownObject } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import { tryCatchWrapper } from '../../_utils/src/tryCatchWrapper';

/**
 * Object containing client-side context such as adserver macros.
 *
 * @typedef {Object} module:lemonpi-context~context
 * @property {Object} creative context relating to creatives
 * @property {Object} appnexus context relating to appnexus adserver
 * @property {Object} $request context relating to the request
 * @example
 *  {
 *    creative: { width: 100, height: 600 },
 *    appnexus: { ADV_ID: "0" },
 *    $request: { timestamp: "2018-02-01T02:02:02-02:00" }
 *  }
 */

/**
 * Deep merge two objects.
 *
 * @access private
 * @param  {Object} objA Target object
 * @param  {Object} objB Source object
 * @return {Object} Target object with source object merged
 */

export function merge(objA: UnknownObject, objB: UnknownObject): UnknownObject {
  return Object.entries(objB).reduce((acc, [key, valueB]) => {
    const valueA = objA[key];

    if (!valueA) {
      return Object.assign(acc, { [key]: valueB });
    }

    if (typeof valueB === 'object') {
      return Object.assign(acc, { [key]: merge(valueA, valueB) });
    }

    return Object.assign(acc, {
      [key]: Object.assign(valueA, { [key]: valueB })
    });
  }, objA);
}

function getContextEnrichers(config: LemonpiEnrichedConfig) {
  const enrichers = [$requestTimestamp, $requestGdpr];

  if (config.lemonpiCookiesUrl) {
    enrichers.push($requestLemonpiCookies);
  }

  enrichers.push(queryParameterContext);
  if (!config.adserver) return enrichers;
  if (config.adserver.code === 'adform') {
    enrichers.push(adform);
  }

  if (config.adserver.code === 'appnexus') {
    enrichers.push(appnexus);
  }

  if (config.adserver.code === 'google-cm' || config.adserver.code === 'dcm') {
    enrichers.push(google);
  }

  if (config.adserver.code === 'trade-desk') {
    enrichers.push(tradedesk);
  }
  if (config.adserver.code === 'lemonpi') {
    enrichers.push(displayLemonpi);
  }

  return enrichers;
}

function getContext(config: LemonpiEnrichedConfig) {
  if (!config) {
    return contextReady({});
  }

  if (config.context) {
    // in case we initialized it in display-tag
    return contextReady(config.context);
  }

  const enrichers = getContextEnrichers(config);

  let context = {};
  let countdownLatch = enrichers.length;
  let resolved = false;

  const callback = (key: string, delta: any) => {
    contextPartial(key, delta);
    if (key === 'lemonpi') context = merge(context, delta);
    else context = merge(context, { [key]: delta });
    countdownLatch--;

    if (countdownLatch === 0 && !resolved) {
      clearTimeout(timeout);
      contextReady(context);
    }
  };

  const timeout = setTimeout(() => {
    resolved = true;
    contextReady(context);
  }, config.CLIENT_CONTEXT_TIMEOUT_MS);

  enrichers.forEach((enricher) => enricher(config, callback));
}

function contextPartial(key: string, context: any) {
  window.setTimeout(
    () => dispatchCustomEvent('lemonpi.context/partial', { key, context }),
    0
  );
}

function contextReady(context: any) {
  window.setTimeout(
    () => dispatchCustomEvent('lemonpi.context/ready', context),
    0
  );
}

const wrappedGetContext = tryCatchWrapper(getContext);
window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  wrappedGetContext(event.detail);
});
