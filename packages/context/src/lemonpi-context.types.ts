declare global {
  interface Window {
    _lemonpiCookiesLoaded: (results: any) => void;
    dhtml: any;
    APPNEXUS: any;
    clickTag1: string;
    clickTag2: string;
    googletag: {
      apiReady: boolean | undefined;
      pubadsReady: boolean | undefined;
    };
    __tcfapiLocator: any;
    __tcfapi: any;
  }
}

export {};
