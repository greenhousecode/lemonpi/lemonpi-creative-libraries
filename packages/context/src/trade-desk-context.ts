/**
 * The Trade Desk context enricher.
 *
 * @module trade-desk-context
 */

import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

const MACRO_START = 'LEMONPI%MACROS%START';
const MACRO_END = 'LEMONPI%MACROS%END';

export default function enrich(
  config: LemonpiEnrichedConfig,
  cb: CallbackFunction
) {
  let wasNotified = false;

  // Invoke cb on ad server context passed in the first message from the parent
  // containing the MACRO_START and MACRO_END delimiters.
  window.addEventListener('message', (event) => {
    if (event.source !== window.parent || wasNotified) {
      return;
    }
    const context = event.data;
    const startIndex = context.indexOf(MACRO_START);
    const endIndex = context.indexOf(MACRO_END);

    if (startIndex === -1 || endIndex === -1) {
      // Possibly an unrelated message from a different tracker.
      return;
    }

    wasNotified = true;
    const macroFragment = context.slice(
      startIndex + MACRO_START.length,
      endIndex
    );

    try {
      const parsed = JSON.parse(macroFragment);
      return cb('trade-desk', parsed);
    } catch (err) {
      return cb('trade-desk', {});
    }
  });

  // Notify the parent we can handle the message.
  window.parent.postMessage(null, '*');
}
