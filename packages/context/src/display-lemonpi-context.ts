/**
 * Display lemonpi context enricher.
 *
 * @module display-lemonpi-context
 */

import { CallbackFunction } from '../../../types/utility.types';
import { parseScriptDataAttributesFromScript } from '../../config-provider/src/lemonpi-config-provider';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

/**
 * Definition of protected data attributes
 *
 * @access private
 * @type {Array}
 */
const restrictedQueryKeys = [
  'width',
  'height',
  'config',
  'clickTag',
  'gdpr',
  'gdprConsent'
];

/**
 * Helper function to extract identifiers from key
 * @param {string} key
 * @returns identifier
 */
const replaceKeyValue = (key: string) =>
  key.replace('dspSignalKey', '').replace('dspSignalValue', '');

export function parseMacros(iframeId?: string): [string, any] {
  const scriptDataAttributes = parseScriptDataAttributesFromScript(iframeId);
  if (scriptDataAttributes) {
    const macros = Object.entries(scriptDataAttributes)
      //filter restricted values
      .filter(([key]) => !restrictedQueryKeys.includes(key))
      //sort by key identifier for simpler matching
      .sort(
        ([key1], [key2]) =>
          Number(replaceKeyValue(key1)) - Number(replaceKeyValue(key2))
      )
      .reduce<Record<string, any>>((acc, [key, value], index, arr) => {
        //match based on keys
        if (index < arr.length - 1) {
          const nextItem = index + 1;
          const [currKey, currId] = key.split(/(\d+)/);
          const [, nextId] = arr[nextItem][0].split(/(\d+)/);
          if (currId === nextId) {
            if (currKey === 'dspSignalKey') {
              const [head, ...tail] = value.split('.');
              acc[head] = { ...acc[head], [tail.join('.')]: arr[nextItem][1] };
            } else {
              const [head, ...tail] = arr[nextItem][1].split('.');
              acc[head] = { ...acc[head], [tail.join('.')]: value };
            }
          }
        }
        return acc;
      }, {});

    return ['lemonpi', macros];
  }
}

export default function enrich(
  config: LemonpiEnrichedConfig,
  cb: CallbackFunction
) {
  const macros = parseMacros(config.iframeId);
  if (macros) {
    const [k, v] = macros;
    return cb(k, v);
  }
  return cb('lemonpi', {});
}
