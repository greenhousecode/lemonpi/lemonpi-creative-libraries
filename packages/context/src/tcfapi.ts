function findTcfApiFrame() {
  //start here at our window
  let frame = window;

  // if we locate the CMP iframe we will reference it with this
  let cmpFrame;

  while (frame) {
    try {
      /**
       * throws a reference error if no frames exist
       */

      if (frame.frames['__tcfapiLocator']) {
        cmpFrame = frame;
        break;
      }
    } catch (ignore) {
      //ignore
    }

    if (frame === window.top) {
      break;
    }
    frame = frame.parent as Window & typeof globalThis;
  }

  return cmpFrame;
}

function tcfApiProxy(frame: Window) {
  let callId = 0;
  const callbacks: Record<string, any> = {};

  const nextCallId = () => {
    const result = 'lemonpi_' + callId;
    callId++;
    return result;
  };

  const onPostMessage = (event: MessageEvent) => {
    try {
      const data =
        typeof event.data === 'string' ? JSON.parse(event.data) : event.data;

      const payload = data.__tcfapiReturn;
      if (payload && callbacks[payload.callId]) {
        callbacks[payload.callId](payload.returnValue, payload.success);
        callbacks[payload.callId] = null;
      }
    } catch (e) {
      // event.data was not valid json. We're unable to map this message
      // to the callId that initiated the call. Do nothing
      return;
    }
  };

  window.addEventListener('message', onPostMessage, false);

  return (command: string, version: number, callback: any, arg: any) => {
    const id = nextCallId();
    const message = {
      __tcfapiCall: {
        command: command,
        parameter: arg,
        version: version,
        callId: id
      }
    };

    callbacks[id] = callback;
    frame.postMessage(message, '*');
  };
}

function initTcfApi() {
  if (window.__tcfapi) {
    return window.__tcfapi;
  }

  const tcfApiFrame = findTcfApiFrame();

  if (!tcfApiFrame) {
    return null;
  }

  return tcfApiProxy(tcfApiFrame);
}

export default initTcfApi;
