/**
 * Adform context enricher.
 *
 * @module adform-context
 */

import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import NativeDataStore from '../../native-data-store/src/native-data-store';

/**
 * Mapping of context keys to Adform variables.
 *
 * @access private
 * @type {Object}
 */
export const variableMapping = {
  TAG_ID: 'bn',
  CAMPAIGN_ID: 'ADFcmpgnID',
  LINE_ID: 'ADFbanPlacID',
  BANNER_ID: 'ADFbanID',
  ROTATION_NR: 'rotseqno'
};

const MOBILE_APP_BUNDLE_ID = 'ADFAPPID';

export default function enrich(
  config: LemonpiEnrichedConfig,
  cb: CallbackFunction
) {
  if (!window.dhtml) {
    return cb('adform', {});
  }

  const context: Record<string, any> = {};
  for (const prop in variableMapping) {
    context[prop] = window.dhtml.getVar(
      variableMapping[prop as keyof typeof variableMapping]
    );
  }

  const mobileAppBundleId = window.dhtml.getVar(MOBILE_APP_BUNDLE_ID);
  if (mobileAppBundleId) {
    NativeDataStore.dispatchPartialData({ app_bundle_id: mobileAppBundleId });
  }
  return cb('adform', context);
}
