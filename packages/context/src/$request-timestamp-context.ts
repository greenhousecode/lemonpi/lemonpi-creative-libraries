/**
 * $request.timestamp context enricher.
 *
 * @module $request-context
 */

import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

/**
 * Pad the start of a string with another string until the resulting string
 * reaches the desired length.
 *
 * @access private
 * @param  {string} str Source string
 * @param  {Number} length Desired string length
 * @param  {string} padStr String to use for padding
 * @return {string} Padded string
 */

function padStart(
  str: string | number,
  length: number,
  padStr: string
): string {
  str = typeof str === 'string' ? str : String(str);

  if (str.length >= length) {
    return str;
  }

  return padStart(padStr + str, length, padStr);
}

/**
 * Format a date instance to a ISO-8601 timezone designator string.
 * e.g `+01:00`, `-02:00`, `Z`
 *
 * @access private
 * @param  {Date} date Date instance
 * @return {string} ISO-8601 timezone designator
 */
function isoTimezone(date: Date) {
  const timezoneOffsetInMinutes = date.getTimezoneOffset();
  const offsetHours = Math.floor(Math.abs(timezoneOffsetInMinutes / 60));
  const offsetMinutes = Math.abs(timezoneOffsetInMinutes % 60);

  const offsetString =
    padStart(offsetHours, 2, '0') + ':' + padStart(offsetMinutes, 2, '0');

  if (timezoneOffsetInMinutes < 0) {
    return '+' + offsetString;
  }
  if (timezoneOffsetInMinutes > 0) {
    return '-' + offsetString;
  }

  return 'Z';
}

/**
 * Format a date instance to a ISO-8601 with UTC offset string.
 *
 * @access private
 * @param  {Date} date Date instance
 * @return {string} ISO-8601 formatted string
 */
export function isoDateTime(date: Date) {
  const timezone = isoTimezone(date);

  const year = date.getFullYear();
  const month = padStart(date.getMonth() + 1, 2, '0');
  const day = padStart(date.getDate(), 2, '0');

  const hours = padStart(date.getHours(), 2, '0');
  const minutes = padStart(date.getMinutes(), 2, '0');
  const seconds = padStart(date.getSeconds(), 2, '0');

  return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}${timezone}`;
}

export function getTimestamp(): [string, any] {
  const timestamp = isoDateTime(new Date());
  return ['$request', { timestamp }];
}

function enrich(config: LemonpiEnrichedConfig, cb: CallbackFunction) {
  const [k, v] = getTimestamp();
  cb(k, v);
}

export default enrich;
