import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import initTcfApi from './tcfapi';
import { parseScriptDataAttributesFromScript } from '../../config-provider/src/lemonpi-config-provider';

export function parseConsent(consent: string) {
  const reg = /^\${GDPR_CONSENT_[0-9]{1,3}}$/g;
  const m = consent && consent.match(reg);
  return (!m && consent) || null;
}

export function parseGdpr(gdpr: string): number | null {
  const v: number | false = ['0', '1'].includes(gdpr) ? parseInt(gdpr) : false;

  if (v !== false) {
    return v;
  } else {
    return null;
  }
}

export function parseLemonpiGdprAttributes(): [string, any] {
  const scriptDataAttributes = parseScriptDataAttributesFromScript();

  if (scriptDataAttributes) {
    const gdpr = parseGdpr(scriptDataAttributes['gdpr']);

    const gdprConsent = parseConsent(scriptDataAttributes['gdprConsent']);

    if (gdpr !== null && gdprConsent) {
      return [
        '$request',
        {
          'gdpr-consent': gdprConsent,
          gdpr: gdpr
        }
      ];
    }
  }
}

function enrich(config: LemonpiEnrichedConfig, cb: CallbackFunction) {
  const adserver = config.adserver && config.adserver.code;

  if (adserver === 'lemonpi') {
    const gdprAttrs = parseLemonpiGdprAttributes();
    if (gdprAttrs) {
      const [k, v] = gdprAttrs;
      cb(k, v);
      return;
    }
  }

  const api = initTcfApi();

  if (!api) {
    cb('$request', {});
    return;
  }

  api('getTCData', 2, (tcData: any, success: boolean) => {
    if (success) {
      cb('$request', {
        'gdpr-consent': tcData.tcString,
        gdpr: tcData.gdprApplies ? 1 : 0
      });
    } else {
      cb('$request', {});
    }
  });
}

export default enrich;
