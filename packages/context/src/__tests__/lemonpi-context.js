let $requestTimestamp;
let $requestLemonpiCookies;
let $requestGdpr;
let appnexus;
let google;
let merge;
let queryParameterContext;
let displayLemonpi;

jest.useFakeTimers();

beforeEach(() => {
  jest.resetModules();
  jest.mock('../$request-timestamp-context');
  jest.mock('../$request-lp-cookies-context');
  jest.mock('../$request-gdpr-context');
  jest.mock('../appnexus-context');
  jest.mock('../google-context');
  jest.mock('../query-parameter-context');
  jest.mock('../display-lemonpi-context');

  $requestTimestamp = require('../$request-timestamp-context').default;
  $requestLemonpiCookies = require('../$request-lp-cookies-context').default;
  $requestGdpr = require('../$request-gdpr-context').default;
  appnexus = require('../appnexus-context').default;
  google = require('../google-context').default;
  queryParameterContext = require('../query-parameter-context').default;
  displayLemonpi = require('../display-lemonpi-context').default;

  $requestTimestamp.mockImplementationOnce((config, cb) =>
    cb('$request', { k: 'v' })
  );

  $requestLemonpiCookies.mockImplementationOnce((config, cb) =>
    cb('$request', { k2: 'v2' })
  );

  $requestGdpr.mockImplementationOnce((config, cb) =>
    cb('$request', { k3: 'v3' })
  );

  appnexus.mockImplementationOnce((config, cb) => cb('appnexus', { k: 'v' }));
  google.mockImplementationOnce((config, cb) => cb('google-cm', { k: 'v' }));
  displayLemonpi.mockImplementationOnce((config, cb) =>
    cb('lemonpi', {
      appnexus: { k: 'v' },
      gcm: { k: 'v' }
    })
  );

  queryParameterContext.mockImplementationOnce((config, cb) =>
    cb('query-parameters', { qp_1: 'a', qp_2: 'b' })
  );

  const module = require('../lemonpi-context');
  merge = module.merge;
});

afterEach(() => {
  jest.clearAllTimers();
});

describe('merge', () => {
  test('shallow merges objects', () => {
    expect(merge({}, {})).toEqual({});
    expect(merge({ a: 1 }, { b: 2 })).toEqual({ a: 1, b: 2 });
  });

  test('deep merges objects', () => {
    expect(merge({ a: { aa: 1 } }, { a: { ab: 2 } })).toEqual({
      a: { aa: 1, ab: 2 }
    });
  });
});

describe('Context module dispatches context/partial event', () => {
  let handler;

  beforeEach(() => {
    handler = jest.fn();
    window.addEventListener('lemonpi.context/partial', handler);
  });

  test('$request', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', { detail: {} })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { key: '$request', context: { k: 'v', k3: 'v3' } }
      })
    );
  });

  test('appnexus', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { adserver: { code: 'appnexus' } }
      })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { key: 'appnexus', context: { k: 'v' } }
      })
    );
  });

  test('google', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { adserver: { code: 'google-cm' } }
      })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { key: 'google-cm', context: { k: 'v' } }
      })
    );
  });
  test('lemonpi', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { adserver: { code: 'lemonpi' } }
      })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: {
          key: 'lemonpi',
          context: {
            appnexus: { k: 'v' },
            gcm: { k: 'v' }
          }
        }
      })
    );
  });

  test('query-parameters', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: {}
      })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: { key: 'query-parameters', context: { qp_1: 'a', qp_2: 'b' } }
      })
    );
  });
});

describe('Context module dispatches context/ready event', () => {
  let handler;

  beforeEach(() => {
    handler = jest.fn();
    window.addEventListener('lemonpi.context/ready', handler);
  });

  test('when all enrichers are ready', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { adserver: { code: 'google-cm' }, lemonpiCookiesUrl: 'fake' }
      })
    );

    jest.advanceTimersByTime(0);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: {
          $request: { k: 'v', k2: 'v2', k3: 'v3' },
          'google-cm': { k: 'v' },
          'query-parameters': { qp_1: 'a', qp_2: 'b' }
        }
      })
    );
  });

  test('after timeout period', () => {
    $requestTimestamp.mockReset();
    $requestTimestamp.mockImplementationOnce(jest.fn());

    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: {
          adserver: { code: 'google-cm' },
          CLIENT_CONTEXT_TIMEOUT_MS: 1000
        }
      })
    );

    jest.advanceTimersByTime(1000);
    expect(handler).not.toHaveBeenCalled();
    jest.advanceTimersByTime(1);

    expect(handler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: {
          'google-cm': { k: 'v' },
          'query-parameters': { qp_1: 'a', qp_2: 'b' },
          $request: { k3: 'v3' }
        }
      })
    );
  });

  test('only once - timeout first', () => {
    $requestTimestamp.mockReset();
    $requestTimestamp.mockImplementationOnce((config, cb) =>
      setTimeout(() => cb('$request', {}), 1001)
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: {
          adserver: { code: 'google-cm' },
          CLIENT_CONTEXT_TIMEOUT_MS: 1000
        }
      })
    );

    jest.advanceTimersByTime(999);
    expect(handler).not.toHaveBeenCalled();
    jest.advanceTimersByTime(1);

    // timeout not occured
    expect(handler.mock.calls.length).toEqual(0);

    jest.advanceTimersByTime(1);

    // $request context resolved
    expect(handler.mock.calls.length).toEqual(1);
  });

  test('only once - resolve first', () => {
    $requestTimestamp.mockReset();
    $requestTimestamp.mockImplementationOnce((config, cb) =>
      cb('$request', {})
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: {
          adserver: { code: 'google-cm' },
          CLIENT_CONTEXT_TIMEOUT_MS: 1000
        }
      })
    );

    jest.advanceTimersByTime(0);

    // enrichers resolved
    expect(handler.mock.calls.length).toEqual(1);

    jest.advanceTimersByTime(1000);

    // timeout occured
    expect(handler.mock.calls.length).toEqual(1);
  });
});
