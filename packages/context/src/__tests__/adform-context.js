import enrich from '../adform-context';

beforeEach(() => {
  delete global.dhtml;
});

describe('Adform context enricher', () => {
  test('returns Adform macros', () => {
    global.dhtml = {
      // Return a wrapped variable name so we are sure the results are used.
      getVar: jest.fn((val) => `1${val}2`)
    };

    const cb = jest.fn();
    enrich({}, cb);

    expect(cb).toHaveBeenCalledWith(
      'adform',
      expect.objectContaining({
        TAG_ID: '1bn2',
        CAMPAIGN_ID: '1ADFcmpgnID2',
        LINE_ID: '1ADFbanPlacID2',
        BANNER_ID: '1ADFbanID2',
        ROTATION_NR: '1rotseqno2'
      })
    );
  });

  test('returns empty object when adform library is undefined', () => {
    const cb = jest.fn();
    enrich({}, cb);

    expect(cb).toHaveBeenCalledWith('adform', {});
  });
});
