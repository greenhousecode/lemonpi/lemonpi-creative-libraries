import enrich, { isoDateTime } from '../$request-timestamp-context';

describe('$request.timestamp context enricher', () => {
  test('returns timestamp', () => {
    const cb = jest.fn();
    enrich({}, cb);

    const context = cb.mock.calls[0];
    expect(context[0]).toEqual('$request');
    expect(Object.keys(context[1])).toEqual(['timestamp']);
  });

  test('formats timestamp in ISO-8601 format', () => {
    const tests = [
      [[2018, 1, 2, 3, 4, 5, 90], '2018-02-02T03:04:05-01:30'],
      [[2018, 1, 2, 3, 4, 5, -120], '2018-02-02T03:04:05+02:00'],
      [[2018, 1, 2, 3, 4, 5, 0], '2018-02-02T03:04:05Z']
    ];

    for (const [config, str] of tests) {
      const date = {
        getFullYear: () => config[0],
        getMonth: () => config[1],
        getDate: () => config[2],
        getHours: () => config[3],
        getMinutes: () => config[4],
        getSeconds: () => config[5],
        getTimezoneOffset: () => config[6]
      };

      expect(isoDateTime(date)).toEqual(str);
    }
  });
});
