import enrich from '../display-lemonpi-context';

beforeEach(() => {
  const script = document.createElement('script');
  Object.defineProperty(script, 'dataset', {
    value: {
      config:
        'eyJhZHZlcnRpc2VySWQiOjM4MCwiYWRzZXJ2ZXIiOnsiY29kZSI6ImxlbW9ucGkifSwiY3JlYXRpdmVSZXZpc2lvbklkIjozMjc0LCJjYW1wYWlnbklkIjo5ODIsImNyZWF0aXZlSWQiOjU1NzksImFkc2V0SWQiOjk4OTcsImVudmlyb25tZW50IjoidGVzdCIsICJ2ZXJzaW9uIjoyfQ==',
      width: 300,
      height: 600,
      dspSignalKey1: 'appnexus.LEMONPI',
      dspSignalKey2: 'appnexus.LAT',
      dspSignalValue1: '123',
      dspSignalValue3: '123-321-321',
      dspSignalKey3: 'appnexus.LONG',
      dspSignalValue4: 'test-123',
      dspSignalValue2: '123-123-123',
      dspSignalKey4: 'gcm.TEST',
      dspSignalKey5: 'gcm.LEMONPI',
      dspSignalValue5: '1',
      dspSignalKey6: 'ttd.LEMONPI',
      dspSignalKey7: 'ttd.GEO_LAT',
      dspSignalValue7: '3',
      dspSignalValue6: '2',
      clickTag: 'http://lemonpi.io'
    }
  });

  jest.spyOn(document, 'currentScript', 'get').mockReturnValue(script);
});

describe('Lemonpi context enricher', () => {
  test('returns macros split on the key', () => {
    const macros = {
      appnexus: { LEMONPI: '123', LAT: '123-123-123', LONG: '123-321-321' },
      gcm: { TEST: 'test-123', LEMONPI: '1' },
      ttd: { LEMONPI: '2', GEO_LAT: '3' }
    };

    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toHaveBeenCalledWith('lemonpi', expect.objectContaining(macros));
  });
});
