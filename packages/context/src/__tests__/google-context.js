import enrich from '../google-context';

jest.useFakeTimers();

beforeEach(() => {
  jest.clearAllTimers();
  window.clickTag2 = null;
});

describe('Google context enricher', () => {
  test('returns google-cm macros', () => {
    window.clickTag2 =
      'https://dcm.com?dc_redir=LEMONPI%25MACROS%25START%7B%22ADVERTISER_ID%22%3A%20%220%22%2C%20%22geo%22%3A%20%22ct%3DUS%26st%3DNY%26ac%3D212%26zp%3D10001%26bw%3D4%26dma%3D3%26city%3D13275%22%7DLEMONPI%25MACROS%25END';

    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toHaveBeenCalledWith('google-cm', {
      ADVERTISER_ID: '0',
      geo: 'ct=US&st=NY&ac=212&zp=10001&bw=4&dma=3&city=13275',
      'geo-city': '13275',
      'geo-state': 'NY',
      'geo-zip': '10001'
    });
  });

  test('returns', () => {
    window.clickTag2 =
      'https://dcm.com?dc_redir=LEMONPI%25MACROS%25START%7B%22ADVERTISER_ID%22%3A%20%220%22%7DLEMONPI%25MACROS%25END';

    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toBeCalledWith('google-cm', {
      ADVERTISER_ID: '0'
    });
  });

  test('returns empty object when clickTag2 is incomplete', () => {
    window.clickTag2 = 'https://dcm.com';
    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toBeCalledWith('google-cm', {});
  });

  test('returns empty object when clickTag2 is malformed', () => {
    window.clickTag2 =
      'https://dcm.com?dc_redir=LEMONPI%25MACROS%25START%22ADVERTISER_ID%22%3A%20%220%22%7DLEMONPI%25MACROS%25END';

    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toBeCalledWith('google-cm', {});
  });

  test('returns empty object when clickTag2 is unavailable after timeout', () => {
    const cb = jest.fn();
    enrich({ CLIENT_CONTEXT_TIMEOUT_MS: 2000 }, cb);

    jest.advanceTimersByTime(1999);
    expect(cb).not.toBeCalled();
    jest.advanceTimersByTime(1);
    expect(cb).toBeCalledWith('google-cm', {});
  });
});
