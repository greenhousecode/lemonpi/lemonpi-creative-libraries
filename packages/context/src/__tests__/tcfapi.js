import initTcfApi from '../tcfapi';

beforeEach(() => {
  delete window.frames;
  delete window.parent;
  delete window.top;
  delete window.__tcfapi;
  delete window.addEventListener;
});

describe('tcfapi', () => {
  test('returns api defined in current frame', () => {
    const api = jest.fn();
    window.__tcfapi = api;
    expect(initTcfApi()).toEqual(api);
  });

  test('returns null on no api and no viable ancestor', () => {
    const top = { frames: { aa: {}, bb: {} } };

    window.frames = {};
    window.top = top;
    window.parent = {
      top: top,
      parent: top,
      frames: {}
    };

    expect(initTcfApi()).toEqual(null);
  });

  test('returns proxy to ancestor frame', () => {
    const top = {
      frames: { __tcfapiLocator: {} }
    };

    window.addEventListener = jest.fn();

    window.frames = {};
    window.parent = {
      top: top,
      parent: top,
      frames: {}
    };

    expect(initTcfApi()).toBeTruthy();
  });

  test('proxy postmessages correct frame', () => {
    const postMessage = jest.fn();
    const top = {
      frames: { __tcfapiLocator: {} },
      postMessage
    };

    window.addEventListener = jest.fn();

    window.frames = {};
    window.parent = {
      top: top,
      parent: top,
      frames: {}
    };

    const api = initTcfApi();
    api('command', 'version', jest.fn(), null);
    expect(postMessage).toHaveBeenCalledWith(
      {
        __tcfapiCall: {
          callId: 'lemonpi_0',
          command: 'command',
          parameter: null,
          version: 'version'
        }
      },
      '*'
    );
  });

  test('proxy returns result', () => {
    const postMessage = jest.fn();
    let messageCallback;

    const addEventListener = jest.fn((m, f) => {
      messageCallback = f;
    });

    const top = {
      frames: { __tcfapiLocator: {} },
      postMessage
    };

    window.addEventListener = addEventListener;

    window.frames = {};
    window.parent = {
      top: top,
      parent: top,
      frames: {}
    };

    const api = initTcfApi();
    const callback = jest.fn();
    api('command', 'version', callback, null);

    messageCallback({
      data: JSON.stringify({
        __tcfapiReturn: { returnValue: 'a', success: true, callId: 'lemonpi_0' }
      })
    });

    expect(callback).toHaveBeenCalledWith('a', true);
  });
});
