import enrich from '../$request-lp-cookies-context';

describe('$request.cookies context enricher', () => {
  afterEach(() => {
    const scripts = document.querySelector('script');
    document.body.removeChild(scripts);
  });

  test('Places correct script tag', () => {
    enrich({ lemonpiCookiesUrl: 'fake' }, jest.fn());

    const tag = document.querySelector(
      'script[src="fake?callback=_lemonpiCookiesLoaded"]'
    );

    expect(tag).toBeDefined();
  });

  test('Returns cookies when script finishes loading', () => {
    const cb = jest.fn();
    enrich({ lemonpiCookiesUrl: 'fake' }, cb);

    const tag = document.querySelector(
      'script[src="fake?callback=_lemonpiCookiesLoaded"]'
    );

    expect(cb).not.toHaveBeenCalled();

    // Script is evaluated, after which the `onload` callback is triggered
    window._lemonpiCookiesLoaded({ a: 1 }); // Fake JSONP callback
    tag.onload(); // Fake onLoad callback

    expect(cb).toHaveBeenCalledWith('$request', { a: 1 });
  });
});
