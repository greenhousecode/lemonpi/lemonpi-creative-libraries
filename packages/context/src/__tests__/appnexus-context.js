import enrich from '../appnexus-context';

beforeEach(() => {
  delete global.APPNEXUS;
});

describe('Appnexus context enricher', () => {
  test('returns appnexus macros', () => {
    global.APPNEXUS = {
      ready: jest.fn((cb) => cb()),
      getMacroByName: jest.fn((val) => val)
    };

    const cb = jest.fn();
    enrich({}, cb);

    expect(cb).toHaveBeenCalledWith(
      'appnexus',
      expect.objectContaining({ ADV_ID: 'ADV_ID' })
    );
  });

  test('returns empty object when appnexus library is undefined', () => {
    const cb = jest.fn();
    enrich({}, cb);

    expect(cb).toHaveBeenCalledWith('appnexus', {});
  });
});
