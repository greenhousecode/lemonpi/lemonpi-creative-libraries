/* eslint-env jest */
import enrich from '../query-parameter-context';

describe('query-parameters enricher', () => {
  const { location } = window;
  let originalSearch;
  beforeAll(() => {
    delete window.location;
    window.location = { search: '' };
  });
  beforeEach(() => {
    originalSearch = window.location.search || '';
  });

  afterEach(() => {
    window.location.search = originalSearch;
  });

  afterAll(() => {
    window.location = location;
  });
  test('Returns all parameters correctly', () => {
    window.location.search = 'qp_1=a&qp_2=b&qp_3=c&qp_4=d';

    const cb = jest.fn();
    enrich({}, cb);

    expect(cb).toHaveBeenCalledWith('query-parameters', {
      qp_1: 'a',
      qp_2: 'b',
      qp_3: 'c',
      qp_4: 'd'
    });
  });

  test('Returns the parameters present in url', () => {
    window.location.search = 'qp_1=a&qp_3=c';

    const cb = jest.fn();
    enrich({}, cb);

    expect(cb).toHaveBeenCalledWith('query-parameters', {
      qp_1: 'a',
      qp_3: 'c'
    });
  });

  test("Doesn't return other parameters, like tagging", () => {
    window.location.search = 'utm_src=a_campaign&utm_medium=a_banner';

    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toHaveBeenCalledWith('query-parameters', {});
  });

  test('Window scope is configurable', () => {
    const cb = jest.fn();
    enrich(
      {
        window: {
          location: { search: '?qp_1=a&qp_2=b' }
        }
      },
      cb
    );

    const [context] = cb.mock.calls;
    expect(context[1]).toEqual({
      qp_1: 'a',
      qp_2: 'b'
    });
  });
});
