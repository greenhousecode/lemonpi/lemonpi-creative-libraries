import tradedesk from '../trade-desk-context';

beforeEach(() => {
  window.addEventListener = jest.fn();
  window.parent = { postMessage: jest.fn() };
});

describe('Trade Desk context enricher', () => {
  test('sends a message to the parent window', () => {
    tradedesk({}, jest.fn());
    expect(window.parent.postMessage).toHaveBeenCalledWith(null, '*');
  });

  test('does not invoke the callback if not notified', () => {
    const cb = jest.fn();
    tradedesk({}, cb);
    expect(cb).not.toHaveBeenCalled();
  });

  test('does not invoke the callback if notified with a message from a wrong source', () => {
    const cb = jest.fn();
    window.addEventListener = jest.fn((type, listener) => {
      expect(type).toBe('message');
      listener({ source: 'not the parent window' });
    });

    tradedesk({}, cb);

    expect(cb).not.toHaveBeenCalled();
  });

  ['', 'LEMONPI%MACROS%START', 'LEMONPI%MACROS%END'].forEach(data => {
    test('does not invoke the callback if notified with a message without macros', () => {
      const cb = jest.fn();
      window.addEventListener = jest.fn((type, listener) => {
        expect(type).toBe('message');
        listener({ source: window.parent, data });
      });

      tradedesk({}, cb);

      expect(cb).not.toHaveBeenCalled();
    });
  });

  test('invokes the callback if notified with a correct message', () => {
    const cb = jest.fn();
    window.addEventListener = jest.fn((type, listener) => {
      expect(type).toBe('message');
      listener({
        source: window.parent,
        data: 'LEMONPI%MACROS%START{"key1":"value","key2":3}LEMONPI%MACROS%END'
      });
    });

    tradedesk({}, cb);

    expect(cb).toHaveBeenCalledWith('trade-desk', { key1: 'value', key2: 3 });
  });

  test('invokes the callback if notified with an invalid message', () => {
    const cb = jest.fn();
    window.addEventListener = jest.fn((type, listener) => {
      expect(type).toBe('message');
      listener({
        source: window.parent,
        data: 'LEMONPI%MACROS%START{"key1":"value","key2":}LEMONPI%MACROS%END'
      });
    });

    tradedesk({}, cb);

    expect(cb).toHaveBeenCalledWith('trade-desk', {});
  });

  test('invokes the callback once if notified multiple times', () => {
    const cb = jest.fn();
    window.addEventListener = jest.fn((type, listener) => {
      expect(type).toBe('message');
      listener({ source: 'not the parent window' });
      listener({ source: window.parent, data: '' });
      listener({
        source: window.parent,
        data: 'LEMONPI%MACROS%START{"key1":"value","key2":3}LEMONPI%MACROS%END'
      });
      listener({
        source: window.parent,
        data: 'LEMONPI%MACROS%START{"key1":"value","key2":}LEMONPI%MACROS%END'
      });
    });

    tradedesk({}, cb);

    expect(cb).toHaveBeenCalledTimes(1);
    expect(cb).toHaveBeenCalledWith('trade-desk', { key1: 'value', key2: 3 });
  });
});
