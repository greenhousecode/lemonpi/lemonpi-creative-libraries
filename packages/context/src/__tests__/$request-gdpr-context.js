const {
  parseScriptDataAttributesFromScript
} = require('../../../config-provider/src/lemonpi-config-provider');

function pad(num, size) {
  num = num.toString();
  while (num.length < size) num = '0' + num;
  return num;
}

describe('parseGdpr', () => {
  const { parseGdpr } = require('../$request-gdpr-context');

  test('should parse "0" and "1"', () => {
    expect(parseGdpr('0')).toBe(0);
    expect(parseGdpr('1')).toBe(1);
  });

  test('should ignore an empty string', () => {
    expect(parseGdpr('')).toBeNull;
    expect(parseGdpr(null)).toBeNull;
    expect(parseGdpr(undefined)).toBeNull;
  });

  test('should ignore incorrect values', () => {
    expect(parseGdpr('true')).toBeNull;
    expect(parseGdpr('false')).toBeNull;
    expect(parseGdpr('01')).toBeNull;
    expect(parseGdpr('00')).toBeNull;
    expect(parseGdpr('11')).toBeNull;
    expect(parseGdpr('10')).toBeNull;
  });
});

describe('parseConsent', () => {
  const { parseConsent } = require('../$request-gdpr-context');

  test('should ignore macros', () => {
    Array(1000)
      .fill()
      .map((_, index) => pad(index, 3))
      .forEach((v, _i, _a) => {
        expect(parseConsent(`\$\{GDPR_CONSENT_${v}\}`)).toBeNull;
      });

    Array(1000)
      .fill()
      .map((_, index) => index)
      .forEach((v, _i, _a) => {
        expect(parseConsent(`\$\{GDPR_CONSENT_${v}\}`)).toBeNull;
      });
  });

  test('should ignore empty strings', () => {
    expect(parseConsent('')).toBeNull;
  });

  test('should pass on strings', () => {
    expect(parseConsent('I consent')).toBe('I consent');
  });

  test('should pass on undefined/null', () => {
    expect(parseConsent(undefined)).toBeNull;
    expect(parseConsent(null)).toBeNull;
  });
});

describe('$request.gdpr context enricher', () => {
  let enrich;
  let tcfApi;
  let parseScriptDataAttributes;

  beforeEach(() => {
    jest.resetModules();
    jest.mock('../tcfapi');
    jest.mock('../../../config-provider/src/lemonpi-config-provider');

    enrich = require('../$request-gdpr-context').default;
    tcfApi = require('../tcfapi').default;
    parseScriptDataAttributes =
      require('../../../config-provider/src/lemonpi-config-provider').parseScriptDataAttributesFromScript;
  });

  test('returns empty object on missing api', () => {
    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toHaveBeenCalledWith('$request', {});
  });

  test('returns empty object on no success', () => {
    tcfApi.mockImplementationOnce(
      () => (command, version, cb) =>
        cb({ tcString: 'tcString', gdprApplies: true }, false)
    );

    const cb = jest.fn();
    enrich({}, cb);
    expect(cb).toHaveBeenCalledWith('$request', {});
  });

  test('returns gdpr data on success', () => {
    tcfApi.mockImplementationOnce(
      () => (command, version, cb) =>
        cb({ tcString: 'tcString', gdprApplies: true }, true)
    );

    const cb1 = jest.fn();
    enrich({}, cb1);
    expect(cb1).toHaveBeenCalledWith('$request', {
      'gdpr-consent': 'tcString',
      gdpr: 1
    });

    tcfApi.mockImplementationOnce(
      () => (command, version, cb) =>
        cb({ tcString: 'tcString', gdprApplies: false }, true)
    );

    const cb2 = jest.fn();
    enrich({}, cb2);
    expect(cb2).toHaveBeenCalledWith('$request', {
      'gdpr-consent': 'tcString',
      gdpr: 0
    });
  });

  test('lemonpi with fallback strategy', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => null);

    tcfApi.mockImplementationOnce(
      () => (command, version, cb) =>
        cb({ tcString: 'tcStringTcf', gdprApplies: true }, true)
    );

    const cb = jest.fn();

    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {
      'gdpr-consent': 'tcStringTcf',
      gdpr: 1
    });
  });

  test('passed consent string when GDPR consented', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => ({
      gdpr: '1',
      gdprConsent: 'tcString'
    }));

    const cb = jest.fn();
    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {
      'gdpr-consent': 'tcString',
      gdpr: 1
    });
  });

  test('passed consent string when GDPR required', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => ({
      gdpr: '0',
      gdprConsent: 'tcString'
    }));

    const cb = jest.fn();
    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {
      'gdpr-consent': 'tcString',
      gdpr: 0
    });
  });

  test('passed consent string when GDPR not required', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => ({
      gdpr: '0',
      gdprConsent: 'tcString'
    }));

    const cb = jest.fn();
    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {
      'gdpr-consent': 'tcString',
      gdpr: 0
    });
  });

  test('no callback with unmaterialized gdpr string', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => ({
      gdpr: '${GDPR}',
      gdprConsent: 'tcString'
    }));

    const cb = jest.fn();
    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {});
  });

  test('callback with unmaterialized gdpr-consent-98 string', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => ({
      gdpr: '1',
      gdprConsent: '${GDPR_CONSENT_98}'
    }));

    const cb = jest.fn();
    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {});
  });

  test('callback with unmaterialized gdpr-consent-999 string', () => {
    parseScriptDataAttributes.mockImplementationOnce(() => ({
      gdpr: '1',
      gdprConsent: '${GDPR_CONSENT_999}'
    }));

    const cb = jest.fn();
    enrich({ adserver: { code: 'lemonpi' } }, cb);

    expect(cb).toBeCalledWith('$request', {});
  });
});
