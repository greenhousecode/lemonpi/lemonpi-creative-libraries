/**
 * Appnexus context enricher.
 *
 * @module appnexus-context
 */

import { CallbackFunction } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import nativeDataStore from '../../native-data-store/src/native-data-store';

/**
 * Definition of available Appnexus macros
 *
 * @access private
 * @type {Array}
 */
export const appnexusMacros = [
  'CPG_ID',
  'CPG_CODE',
  'CP_CODE',
  'CP_ID',
  'CREATIVE_ID',
  'CREATIVE_SIZE',
  'DEAL_ID',
  'PUBLISHER_ID',
  'REFERER_URL_ENC',
  'SELLER_MEMBER_ID',
  'TAG_ID',
  'PT1',
  'PT2',
  'PT3',
  'PT4',
  'SUPPLY_TYPE',
  'EXT_APP_ID',
  'USER_ID',
  'AUCTION_ID',
  'LEMONPI',
  'ADV_ID',
  'SSP_DATA',
  'GEO_LAT',
  'GEO_LON'
];

export default function enrich(
  config: LemonpiEnrichedConfig,
  cb: CallbackFunction
) {
  if (!window.APPNEXUS) {
    return cb('appnexus', {});
  }
  window.APPNEXUS.ready(() => {
    const mobileAppBundleId = window.APPNEXUS.getMacroByName('EXT_APP_ID');
    if (mobileAppBundleId) {
      nativeDataStore.dispatchPartialData({ app_bundle_id: mobileAppBundleId });
    }
    const macros = appnexusMacros.reduce(
      (acc, name) => ({
        ...acc,
        [name]: window.APPNEXUS.getMacroByName(name)
      }),
      {}
    );

    return cb('appnexus', macros);
  });
}
