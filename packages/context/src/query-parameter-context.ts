/**
 * Convert host url into query object
 *
 * @access private
 */

import { CallbackFunction, UnknownObject } from '../../../types/utility.types';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

const getUrlQueryParameters = (search: string) =>
  search
    .replace(/^\?/, '')
    .split('&')
    .filter((parameter) => parameter)
    .reduce(
      (parameters, parameter) => ({
        ...parameters,
        [decodeURI(parameter.split('=')[0])]: decodeURI(parameter.split('=')[1])
      }),
      {}
    );

/**
 * Filters a parameter object to only contain LemonPI context variable parameters
 *
 * @access private
 * @param {object} obj key value object of params
 * @param {array} keys list of keys to filter with
 */

const pick = (obj: UnknownObject, keys: string[]) =>
  keys
    .filter((key) => obj[key] !== undefined)
    .reduce(
      (acc, key) => ({
        ...acc,
        [key]: obj[key]
      }),
      {}
    );

export default function enrich(
  config: LemonpiEnrichedConfig,
  cb: CallbackFunction
) {
  const windowToUse = config.window || window;

  if (!windowToUse.location || !windowToUse.location.search) {
    return cb('query-parameters', {});
  }

  const params = getUrlQueryParameters(windowToUse.location.search || '');
  const filteredParams = pick(params, ['qp_1', 'qp_2', 'qp_3', 'qp_4']);

  return cb('query-parameters', filteredParams);
}
