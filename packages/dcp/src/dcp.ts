import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

let config: LemonpiEnrichedConfig | undefined;

const overlay = document.createElement('div');
overlay.style.position = 'absolute';
overlay.style.backgroundColor = 'black';
overlay.style.left = '0';
overlay.style.right = '0';
overlay.style.bottom = '0';
overlay.style.top = '0';
overlay.style.zIndex = '2147483647';
overlay.id = '__lemonpi_dcp_overlay__';

function hideTemplate() {
  if (config && config.overlayWithSignatureFrame) {
    document.body.appendChild(overlay);
  }
}

function showTemplate() {
  if (config && config.overlayWithSignatureFrame) {
    document.body.removeChild(overlay);
  }
}

window.addEventListener('lemonpi.dcp/timeline-start', () => {
  showTemplate();
  parent.postMessage({ type: 'timeline-started' }, '*');
});

window.addEventListener('lemonpi.dcp/timeline-stop', () => {
  hideTemplate();
  parent.postMessage({ type: 'timeline-stopped' }, '*');
});

window.addEventListener('lemonpi.dcp/capture-image', () => {
  parent.postMessage({ type: 'capture-image' }, '*');
});

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
  hideTemplate();
});
