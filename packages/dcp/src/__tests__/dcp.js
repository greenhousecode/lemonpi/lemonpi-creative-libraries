let dcp;
let handler;

beforeEach(() => {
  jest.resetModules();

  handler = jest.fn();
  global.parent.postMessage = handler;
  dcp = require('../dcp');

  document.body.innerHTML = '';
});

describe('dcp module overlay', () => {
  test('is not shown when overlayWithSignatureFrame is falsy', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', { detail: {} })
    );
    expect(document.body.innerHTML).toEqual('');
  });

  test('is shown initially, when overlayWithSignatureFrame is true', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { overlayWithSignatureFrame: true }
      })
    );
    expect(document.body.innerHTML).toMatchSnapshot();
  });

  test('is hidden after timeline-start', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { overlayWithSignatureFrame: true }
      })
    );
    window.dispatchEvent(new CustomEvent('lemonpi.dcp/timeline-start'));
    expect(document.body.innerHTML).toEqual('');
  });

  test('is shown after timeline-stop', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: { overlayWithSignatureFrame: true }
      })
    );

    window.dispatchEvent(new CustomEvent('lemonpi.dcp/timeline-start'));
    window.dispatchEvent(new CustomEvent('lemonpi.dcp/timeline-stop'));
    expect(document.body.innerHTML).toMatchSnapshot();
  });
});

describe('dcp module dispatches postmessage', () => {
  test('on timeline-start', () => {
    window.dispatchEvent(new CustomEvent('lemonpi.dcp/timeline-start'));
    expect(handler).toHaveBeenCalledWith({ type: 'timeline-started' }, '*');
  });

  test('on timeline-stop', () => {
    window.dispatchEvent(new CustomEvent('lemonpi.dcp/timeline-stop'));
    expect(handler).toHaveBeenCalledWith({ type: 'timeline-stopped' }, '*');
  });

  test('on capture-image', () => {
    window.dispatchEvent(new CustomEvent('lemonpi.dcp/capture-image'));
    expect(handler).toHaveBeenCalledWith({ type: 'capture-image' }, '*');
  });
});
