import { mock, MockProxy } from 'jest-mock-extended';
import { LemonpiConfig } from '../lemonpi-config-provider.types';
let configReadyHandler: any;

beforeEach(() => {
  configReadyHandler = jest.fn();
  window.addEventListener('lemonpi.config/ready', configReadyHandler);

  require('../lemonpi-config-provider');
});

describe('Config without preview parameter', () => {
  test('does not emit lemonpi-awaiting-config', () => {
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    expect(parent.postMessage).not.toHaveBeenCalled();
  });

  test('emits config/ready with window.lemonpiConfig', () => {
    window.lemonpiConfig = {};

    window.dispatchEvent(new CustomEvent('lemonpi/start'));

    expect(configReadyHandler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: expect.objectContaining({ ...window.lemonpiConfig })
      })
    );
  });

  test('enriches config with static config', () => {
    window.lemonpiConfig = {};
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    const event = configReadyHandler.mock.calls[0][0];
    expect(Object.keys(event.detail)).toEqual(
      expect.arrayContaining([
        'CLIENT_CONTEXT_TIMEOUT_MS',
        'DYNAMIC_CONTENT_TIMEOUT_MS',
        'DEFAULT_CONTENT_TIMEOUT_MS',
        'creativeEnv'
      ])
    );
  });
});

let locationMock: MockProxy<Location>;
describe('Config with preview parameter', () => {
  beforeAll(() => {
    locationMock = mock<Location>();
    locationMock.href = 'https://test.com?_lemonpiPreview';

    Object.defineProperty(window, 'location', {
      value: locationMock
    });
  });

  afterAll(() => {
    Object.defineProperty(window.location, 'href', {
      writable: true,
      configurable: true,
      value: 'https://test.com'
    });
  });

  test('sends lemonpi-awaiting-config post-message', () => {
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    expect(parent.postMessage).toHaveBeenCalledWith(
      { type: 'lemonpi-awaiting-config' },
      '*'
    );
  });

  test('emits config/ready after receiving new config', () => {
    const config1 = { test: '1' };
    const config2 = { test: '2' };

    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    expect(configReadyHandler).not.toHaveBeenCalled();

    window.postMessage({ type: 'lemonpi-config', config: config1 }, '*');
    expect(configReadyHandler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: expect.objectContaining(config1)
      })
    );

    window.postMessage({ type: 'lemonpi-config', config: config2 }, '*');
    expect(configReadyHandler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: expect.objectContaining(config2)
      })
    );
  });
});

describe('Normalize v2 config', () => {
  const config = {
    version: 2,
    advertiserId: 1,
    adsetId: 2,
    creativeId: 3,
    creativeRevisionId: 4,
    staticContent: false,
    campaignId: 1,
    adserver: { code: 'appnexus' },
    overlayWithSignatureFrame: true,
    preloadAssetsInContent: true,
    exportContent: { title: { type: 'text', value: 'test' } }
  };

  const testConfig = {
    adserver: { code: 'appnexus' },
    adsetId: 2,
    creativeId: 3,
    staticContent: false,
    defaultContentURL:
      'https://creative-content.test.lemonpi.io/default-content/1/2-3',
    dynamicContentURL: 'https://content.test.lemonpi.io/a/1/c/1/content/2-4',
    exportContent: { title: { type: 'text', value: 'test' } },
    interactionHandlerUrl:
      'https://content.test.lemonpi.io/track/event?e=LEMONPI%25EVENTDATA',
    lemonpiCookiesUrl: 'https://content.test.lemonpi.io/a/1/request-cookies',
    logProxyUrl: 'https://log.test.lemonpi.io/log',
    overlayWithSignatureFrame: true,
    preloadAssetsInContent: true
  };

  const prodConfig = {
    adserver: { code: 'appnexus' },
    adsetId: 2,
    creativeId: 3,
    staticContent: false,
    defaultContentURL:
      'https://creative-content.lemonpi.io/default-content/1/2-3',
    dynamicContentURL: 'https://content.lemonpi.io/a/1/c/1/content/2-4',
    exportContent: { title: { type: 'text', value: 'test' } },
    interactionHandlerUrl:
      'https://content.lemonpi.io/track/event?e=LEMONPI%25EVENTDATA',
    lemonpiCookiesUrl: 'https://content.lemonpi.io/a/1/request-cookies',
    logProxyUrl: 'https://log.lemonpi.io/log',
    overlayWithSignatureFrame: true,
    preloadAssetsInContent: true
  };

  test('returns full test config', () => {
    window.lemonpiConfig = { ...config, environment: 'test' } as LemonpiConfig;
    window.dispatchEvent(new CustomEvent('lemonpi/start'));

    expect(configReadyHandler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: expect.objectContaining(testConfig)
      })
    );
  });

  test('returns full prod config', () => {
    window.lemonpiConfig = { ...config, environment: 'prod' } as LemonpiConfig;
    window.dispatchEvent(new CustomEvent('lemonpi/start'));

    expect(configReadyHandler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: expect.objectContaining(prodConfig)
      })
    );
  });

  test('defaults to prod config on missing environment', () => {
    window.lemonpiConfig = config as LemonpiConfig;
    window.dispatchEvent(new CustomEvent('lemonpi/start'));

    expect(configReadyHandler).toHaveBeenCalledWith(
      expect.objectContaining({
        detail: expect.objectContaining(prodConfig)
      })
    );
  });
});
