/**
 * Module for retrieving the creative specific LemonPI configuration.
 * Configuration is injected in the creative on `window.lemonpiConfig`.
 *
 * This config can be overwritten from outside the creative by dispatching a
 * postMessage with an object like
 * { type: 'lemonpi-config', config: {@link module:lemonpi-config-provider~config}}
 *
 * @module lemonpi-config-provider
 */
import { v4 as uuidv4 } from 'uuid';
import { EmptyObject } from '../../../types/utility.types';
import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';
import {
  Environment,
  ConfigTriage,
  LemonpiConfig,
  NormalizedConfig
} from './lemonpi-config-provider.types';
import { captureNativeData } from '../../native-data-store/src/capture-native-data';

export const envSubdomain = (env: Environment = 'prod') =>
  env === 'prod' ? '' : `.${env}`;

const getDynamicContentUrl = (
  env: Environment,
  advertiserId: number,
  adsetId: number,
  campaignId: number,
  revisionId: number
) =>
  `https://content${envSubdomain(
    env
  )}.lemonpi.io/a/${advertiserId}/c/${campaignId}/content/${adsetId}-${revisionId}`;

const getDefaultContentUrl = (
  env: Environment,
  advertiserId: number,
  adsetId: number,
  creativeId: number
) =>
  `https://creative-content${envSubdomain(
    env
  )}.lemonpi.io/default-content/${advertiserId}/${adsetId}-${creativeId}`;

export const getInteractionHandlerUrl = (env: Environment) =>
  `https://content${envSubdomain(
    env
  )}.lemonpi.io/track/event?e=LEMONPI%25EVENTDATA`;

export const getLemonpiCookiesUrl = (env: Environment, advertiserId: number) =>
  `https://content${envSubdomain(
    env
  )}.lemonpi.io/a/${advertiserId}/request-cookies`;

const getLogProxyUrl = (env: Environment) =>
  `https://log${envSubdomain(env)}.lemonpi.io/log`;

const getCreativeEnv = () => {
  const environments = {
    _lemonpiLive: 'live',
    _lemonpiPreview: 'preview',
    _lemonpiRender: 'render',
    _lemonpiDisplayTag: 'displayTag'
  };

  const result = Object.keys(environments).reduce(
    (acc, k) =>
      getQuerystringParam(k, window.location.href) === k
        ? environments[k as keyof typeof environments]
        : acc,
    'live'
  );

  return result;
};

/**
 * Enrich config object with static values.
 *
 * @param {module:lemonpi-config~config} config
 * @return {module:lemonpi-config~config} enriched config
 */

function enrichConfig(config: ConfigTriage) {
  return {
    ...config,
    CLIENT_CONTEXT_TIMEOUT_MS: 2000,
    DYNAMIC_CONTENT_TIMEOUT_MS: 2000,
    DEFAULT_CONTENT_TIMEOUT_MS: 2000,
    creativeEnv: getCreativeEnv()
  };
}

function normalizeConfig(config: LemonpiConfig | EmptyObject): ConfigTriage {
  if (config.version === 2) {
    const {
      advertiserId,
      adsetId,
      creativeId,
      creativeRevisionId,
      campaignId
    } = config;

    const env = config.environment || 'prod';
    const staticContent = config.staticContent || false;

    return {
      adsetId,
      creativeId,
      campaignId,
      advertiserId,
      creativeRevisionId,
      staticContent: staticContent,
      exportContent: config.exportContent,
      lemonpiContent: config.lemonpiContent,
      context: config.context,
      adserver: config.adserver,
      version: config.version,
      adsetFallbackUrl: config.adsetFallbackUrl,
      impressionId: config.impressionId || uuidv4(),
      interactionHandlerUrl: getInteractionHandlerUrl(env),
      lemonpiCookiesUrl: getLemonpiCookiesUrl(env, advertiserId),
      logProxyUrl: getLogProxyUrl(env),
      dynamicContentURL: staticContent
        ? null
        : getDynamicContentUrl(
            env,
            advertiserId,
            adsetId,
            campaignId,
            creativeRevisionId
          ),
      defaultContentURL: staticContent
        ? null
        : getDefaultContentUrl(env, advertiserId, adsetId, creativeId),
      overlayWithSignatureFrame: config.overlayWithSignatureFrame,
      preloadAssetsInContent: config.preloadAssetsInContent,
      ...(config.iframeId ? { iframeId: config.iframeId } : {}),
      ...(config.lemonpiClickTag
        ? { lemonpiClickTag: config.lemonpiClickTag }
        : {})
    };
  }

  return config;
}

function getQuerystringParam(name: string, url: string): undefined | string {
  name = name.replace(/[[\]]/g, '\\$&');
  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');

  const match = regex.exec(url);
  if (!match) return undefined;
  return name || '';
}

/**
 * Retrieve data attributes from the running script
 *
 * @return {Object | null}  Params params or null
 */
export const parseScriptDataAttributesFromScript = (iframeId?: string) => {
  try {
    if (iframeId) {
      const iframe = window.parent.document.getElementById(iframeId);
      const script = Array.from(iframe?.parentElement.children || []).find(
        (child) => child.tagName === 'SCRIPT'
      ) as HTMLScriptElement | undefined;
      if (script && script.dataset) return script.dataset;
      else return null;
    } else {
      if (document.currentScript && document.currentScript.dataset)
        return document.currentScript.dataset;
      else return null;
    }
  } catch (e) {
    return null;
  }
};

function receivedConfig(config: LemonpiConfig | EmptyObject) {
  const normalizedConfig = normalizeConfig(config);
  const enrichedConfig = enrichConfig(normalizedConfig);
  captureNativeData(enrichedConfig as NormalizedConfig);
  dispatchCustomEvent('lemonpi.config/ready', enrichedConfig);
}

function getConfig() {
  if (getCreativeEnv() === 'live') {
    return receivedConfig(window.lemonpiConfig || {});
  }

  window.addEventListener('message', (event) => {
    if (event.data && event.data.type === 'lemonpi-config') {
      receivedConfig(event.data.config);
    }
  });

  parent.postMessage({ type: 'lemonpi-awaiting-config' }, '*');
}

window.addEventListener('lemonpi/start', getConfig);
