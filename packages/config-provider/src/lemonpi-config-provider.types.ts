import { EmptyObject } from '../../../types/utility.types';

export type Environment = 'dev' | 'test' | 'staging' | 'prod';

export interface LemonpiConfig {
  /**
   * adserver
   */
  adserver: any;
  /**
   * creative's adset
   */
  adsetId: number;
  /**
   * creative's id
   */
  creativeId: number;
  /**
   * url from which dynamic content can be retrieved
   */
  dynamicContentURL: string;
  /**
   * url from which default content can be retrieved
   */
  defaultContentURL: string;
  /**
   * url to the LemonPI interaction handler
   */
  interactionHandlerUrl: string;
  /**
   * url from which cookies on the lemonpi domain can be retrieved
   */
  lemonpiCookiesUrl: string;
  /**
   * content baked in on export
   */
  content?: any;
  /**
   * version of the config
   */
  version: number;
  /**
   * creative revision ID
   */
  creativeRevisionId: number;
  /**
   * campaign ID
   */
  campaignId: number;
  /**
   * staticContent ?
   */
  staticContent?: boolean;
  /**
   * template ?
   */
  template?: any;
  /**
   * advertiser ID
   */
  advertiserId?: number;
  /**
   * environment ID
   */
  environment?: Environment | undefined;
  /**
   * exportContent
   */
  exportContent?: any;
  /**
   * lemonpiContent is provided when loading the display tag.
   * When set, no additional requests to fetch dynamic content are needed.
   */
  lemonpiContent?: any;
  /**
   * overlayWithSignatureFrame ?
   */
  overlayWithSignatureFrame?: any;

  /**
   * preloadAssetsInContent ?
   */
  preloadAssetsInContent?: any;
  /**
   * preloadAssetsInContent ?
   */
  window?: any;
  /**
   * generated impression ID UUIDv4
   */
  impressionId: string;
  /**
   * Contains fallback values in case our services are not reachable
   */
  adsetFallbackUrl?: string;

  /**
   * context when provided by display-tag.js
   */
  context?: any;

  /**
   * iframeId
   */
  iframeId?: string;

  /**
   * Display tag injected click tag
   */
  lemonpiClickTag?: string;
}

export interface NormalizedConfig extends LemonpiConfig {
  staticContent: boolean;
  impressionId: string;
  logProxyUrl: string;
}
export type LemonpiEnrichedConfig = Partial<NormalizedConfig> & {
  CLIENT_CONTEXT_TIMEOUT_MS: number;
  DYNAMIC_CONTENT_TIMEOUT_MS: number;
  DEFAULT_CONTENT_TIMEOUT_MS: number;
  creativeEnv: string;
};

export type ConfigTriage = NormalizedConfig | LemonpiConfig | EmptyObject;

declare global {
  interface Window {
    lemonpiConfig: LemonpiConfig | EmptyObject;
    lemonpiClickTag: string;
  }
}
