# lemonpi-config-provider

This package is part of the lemonpi-creative-libraries. These libraries can be used in creative templates. This particular package's responsibility is to wait for a configuration and call the subscribed handlers with that configuration.
