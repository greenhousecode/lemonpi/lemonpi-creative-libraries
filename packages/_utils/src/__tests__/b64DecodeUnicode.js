const { b64DecodeUnicode } = require('../b64DecodeUnicode');

describe('unicode decoding', () => {
  test('can decode unicode', () => {
    expect(b64DecodeUnicode('eyJ3aGF0IjoizojOvc6xIM6szrvOv86zzr8hIn0=')).toBe(
      '{"what":"Ένα άλογο!"}'
    );
  });
});
