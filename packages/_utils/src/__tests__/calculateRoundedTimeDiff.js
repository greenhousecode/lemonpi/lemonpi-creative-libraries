const { calculateRoundedTimeDiff } = require('../calculateRoundedTimeDiff');

describe('calcualte timestamp diff', () => {
  test('can calulate timestamp difference', () => {
    jest.useFakeTimers();
    const timestamp1 = Date.now();
    jest.advanceTimersByTime(1000);
    const timestamp2 = Date.now();
    expect(calculateRoundedTimeDiff(timestamp2, timestamp1)).toBe(1);
  });
});
