const debugEvents = require('../../../bundle/src/debug-events');
const { tryCatchWrapper } = require('../tryCatchWrapper');

beforeEach(() => {
  window.lemonpiStore = {
    getUuidAndCount: () => ({
      lemonpiUuid: 123,
      lyfecycleCount: 1
    }),
    getIsDebugEnabled: () => true
  };
});

describe('tryCatchWrapper', () => {
  it('should execute the passed function without error', function () {
    const testFunc = () => 'test';
    const result = tryCatchWrapper(testFunc);
    expect(result()).toBe('test');
  });

  it('should catch the error if the passed function throws an error and debug is enabled', function () {
    const errorFunc = () => {
      throw new Error('test error');
    };
    const lemonpiEvent = jest.spyOn(debugEvents, 'lemonpiEvent');
    const result = tryCatchWrapper(errorFunc);
    result();
    expect(lemonpiEvent).toHaveBeenCalledWith(
      debugEvents.LEMONPI_EVENT.ERROR_EVENT,
      expect.objectContaining({
        error: Error('test error')
      })
    );
    lemonpiEvent.mockRestore();
  });
  it('should catch the error if the passed function throws an error but not send any events', function () {
    window.lemonpiStore = {
      getIsDebugEnabled: () => false
    };
    const errorFunc = () => {
      throw new Error('test error');
    };
    const lemonpiEvent = jest.spyOn(debugEvents, 'lemonpiEvent');
    const result = tryCatchWrapper(errorFunc);
    result();
    expect(lemonpiEvent).not.toHaveBeenCalled();
    lemonpiEvent.mockRestore();
  });
});
