const { extractMacrosAsParamsFromContext, convertGoogleCmToGeneric } = require("../macros");

const context = { 
    "$request": { "timestamp": "2023-11-15T11:14:14+02:00", "request-cookies": "cookieMonstah!" }, 
    "query-parameters": { "ignore": "me"},
    "google-cm": { "CAMPAIGN_ID": "%ebuy!" }, 
    "dsp-lineid": { "": "REPLACEWITHYOURMACRO" }
}

describe('dsp macro extraction', () => {
  test('empty case', () => {
    expect(extractMacrosAsParamsFromContext(null)).toEqual({})
  })
  test('lemonpi case', () => {
    expect(extractMacrosAsParamsFromContext(context)).toEqual({"dsp-lineid": "REPLACEWITHYOURMACRO"})
  })
})

const googleCmMacros = {
  'dv360-seg-id': '%psegID=!;',
  AD_ID: '0',
  'dv360-source-url-enc': '%psourceURLENC=!;',
  SITE_ID: '0',
  CAMPAIGN_ID: '0',
  ADVERTISER_ID: '0',
  'dv360-pub-id': '%ppubID=!;',
  'dv360-line-id': '%plineID=!;',
  geo: 'bw=5',
  'dv360-creative-id': '%pcreativeID=!;',
  CREATIVE_ID: '0',
  PLACEMENT_ID: '0',
  'dv360-site-id': '%psiteID=!;',
  'dv360-io-id': '%pioID=!;',
  'dv360-source-url': '%psourceURL=!;',
  'geo-city': null,
  'geo-state': null,
  'geo-zip': null
}

describe('google cm to generic macros', () => {
  test('conversion', () => {
    expect(convertGoogleCmToGeneric(googleCmMacros)).toEqual({
      "dsp-creativeid": "%25pcreativeID%3D!%3B",
      "dsp-ioid": "%25pioID%3D!%3B",
      "dsp-lineid": "%25plineID%3D!%3B",
      "dsp-publisherid": "%25ppubID%3D!%3B",
      "dsp-site-url": "%25psourceURL%3D!%3B",
    })
  })
})