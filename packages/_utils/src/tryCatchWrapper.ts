import { LEMONPI_EVENT, lemonpiEvent } from '../../bundle/src/debug-events';

type TryCatchWrapperFunction = (...args: any[]) => any;

export function tryCatchWrapper(fn: TryCatchWrapperFunction) {
  return function (...args: any[]) {
    try {
      return fn(...args);
    } catch (e) {
      if (!window.lemonpiStore.getIsDebugEnabled()) return;
      lemonpiEvent(LEMONPI_EVENT.ERROR_EVENT, { error: e });
    }
  };
}
