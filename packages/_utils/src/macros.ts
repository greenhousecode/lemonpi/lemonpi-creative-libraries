const lemonpiIgnoredMacroKeys = [
  '$request',
  'query-parameters',
  'google-cm',
  'dcm',
  'adform',
  'appnexus',
  'trade-desk'
];

export function extractMacrosAsParamsFromContext(context: any) {
  if (context) {
    return Object.entries(context).reduce((acc, [k, v]) => {
      return Object.entries(v).reduce((acc2, [k2, v2]) => {
        if (lemonpiIgnoredMacroKeys.includes(k)) {
          return acc2;
        } else {
          const obj: Record<string, any> = {};
          const key = k + (k2?.length > 0 ? '.' + k2 : '');
          obj[key] = v2;
          return Object.assign(acc2, obj);
        }
      }, acc);
    }, {});
  } else {
    return {};
  }
}

// At the time of writing we know that there isn't a generic equivalent for the following fields:
// - dv360-seg-id
// - dv360-site-id
// - dv360-source-url-enc
const googleCmToGeneric: Record<string, string> = {
  'dv360-creative-id': 'dsp-creativeid',
  'dv360-io-id': 'dsp-ioid',
  'dv360-line-id': 'dsp-lineid',
  'dv360-pub-id': 'dsp-publisherid',
  'dv360-source-url': 'dsp-site-url'
};

export function convertGoogleCmToGeneric(macros: Record<string, string>) {
  return Object.entries(macros).reduce(
    (acc: Record<string, string>, [k, v]) => {
      const mapped = googleCmToGeneric[k];
      if (mapped) {
        acc[mapped] = encodeURIComponent(v);
      }
      return acc;
    },
    {}
  );
}
