export function calculateRoundedTimeDiff(
  currentTime: number,
  timestamp: number
) {
  return Math.round((currentTime - timestamp) / 10) / 100;
}
