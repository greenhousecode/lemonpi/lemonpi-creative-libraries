/**
 * Module used to display creative templates in an iframe.
 * This module ensures valid configuration, download the creative template from
 * the source, serves the content inside an iframe.
 * @module display-tag
 */
import { v4 as uuidv4 } from 'uuid';
import {
  envSubdomain,
  getInteractionHandlerUrl,
  getLemonpiCookiesUrl
} from '../../config-provider/src/lemonpi-config-provider';
import { Environment } from '../../config-provider/src/lemonpi-config-provider.types';

import {
  MACRO_EVENT,
  replaceMacro
} from '../../interaction-handler/src/interaction-handler';

import { getTimestamp } from '../../context/src/$request-timestamp-context';
import { parseLemonpiGdprAttributes } from '../../context/src/$request-gdpr-context';
import { parseMacros } from '../../context/src/display-lemonpi-context';
import { merge } from '../../context/src/lemonpi-context';
import { extractMacrosAsParamsFromContext } from '../../_utils/src/macros';
import { b64DecodeUnicode } from '../../_utils/src/b64DecodeUnicode';
import { captureNativeData } from '../../native-data-store/src/capture-native-data';
import nativeDataStore from '../../native-data-store/src/native-data-store';

//save the reference to current script src
const currentScript = document.currentScript;
/**
 *
 * @param {string} host Env based API URL
 * @param {module:parse-query-decode-config~QueryConfig} query
 * @param {object} dimensions
 * @returns {string}
 */

type DisplayTagDimensions = {
  width: string;
  height: string;
};

declare global {
  interface Window {
    _lemonpiDislayTagCookiesLoaded: (results: any) => void;
    _lemonpiCookiesPromiseResolve: (value: any) => void;
  }
}

export interface DataAttributesConfig {
  advertiserId: number;
  version: number;
  adserver: {
    code: string;
  };
  creativeRevisionId: number;
  campaignId: number;
  creativeId: number;
  adsetId: number;
  contentType?: 'content-feed' | 'content-function';
  environment?: Environment;
  impressionId: string;
  interactionHandlerUrl?: string;
  workflow?: 'rules' | 'content-variants' | 'ad-variants';
}

function genRandHexStr(size: number) {
  return [...Array(size)]
    .map(() => Math.floor(Math.random() * 16).toString(16))
    .join('');
}

function displayTagEvent(eventName: string, config: DataAttributesConfig) {
  const event = {
    type: 'debug',
    name: eventName,
    adsetId: config.adsetId,
    creativeId: config.creativeId,
    impressionId: config.impressionId,
    advertiserId: config.advertiserId
  };
  const eventUrl = replaceMacro(
    getInteractionHandlerUrl(config.environment || 'prod'),
    MACRO_EVENT,
    JSON.stringify(event)
  );
  const imgTag = document.createElement('img');
  imgTag.src = eventUrl;
}

export function createGetRenderUrl(
  host: string,
  config: DataAttributesConfig,
  dimensions: DisplayTagDimensions
) {
  switch (true) {
    case config.workflow === 'ad-variants':
      return `${host}/v1/a/${config.advertiserId}/c/${config.campaignId}/d/t/${config.adsetId}/w/${dimensions.width}/h/${dimensions.height}`;
    // workflow may not exist for older tags to make the switch from contentType
    case config.contentType === 'content-feed':
      return `${host}/v1/a/${config.advertiserId}/c/${config.campaignId}/d/v/${config.adsetId}/w/${dimensions.width}/h/${dimensions.height}`;
    case config.contentType === 'content-function':
      return `${host}/v1/a/${config.advertiserId}/c/${config.campaignId}/d/r/${config.adsetId}/w/${dimensions.width}/h/${dimensions.height}`;
    default:
      return null;
  }
}

export function createDynamicContentUrl(
  host: string,
  config: DataAttributesConfig,
  dimensions: DisplayTagDimensions
) {
  return `${host}/a/${config.advertiserId}/c/${config.campaignId}/display/${config.adsetId}-${dimensions.width}-${dimensions.height}`;
}

/**
 *
 * @param {object} dimensions
 * @returns {HTMLIFrameElement} iframe
 */
export function createIframe(dimensions: DisplayTagDimensions) {
  const iframe = document.createElement('iframe');
  iframe.width = dimensions.width + 'px';
  iframe.height = dimensions.height + 'px';
  iframe.id = 'lemonpi-display-tags'.concat(genRandHexStr(16));
  iframe.setAttribute('seamless', 'true');
  iframe.style.border = 'none';
  iframe.setAttribute(
    'sandbox',
    'allow-same-origin allow-scripts allow-popups allow-top-navigation'
  );
  return iframe;
}
/**
 *
 * @param {HTMLIFrameElement} iframe
 * @returns {HTMLIFrameElement}
 */
export function appendElement(iframe: HTMLIFrameElement | HTMLScriptElement) {
  const isWrappedScript =
    currentScript.parentElement || document.currentScript.parentElement;
  if (isWrappedScript != null) {
    return isWrappedScript.appendChild(iframe);
  }
  return document.body.appendChild(iframe);
}

export function passDataAttributes(dataset: DOMStringMap, target: Element) {
  for (const data in dataset) {
    const dataAttributeName = data
      .split(/(?=[A-Z])/)
      .map((name) => name.toLowerCase())
      .join('-');
    target.setAttribute(`data-${dataAttributeName}`, dataset[data]);
  }
}

type RequestOptions = {
  queryParams: Record<string, any>;
  body?: any;
  mimeType?: string;
};

async function makeRequest(
  method: string,
  url: string,
  opts?: RequestOptions
): Promise<any> {
  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();

    if (opts?.mimeType) {
      xhr.overrideMimeType(opts.mimeType);
    }

    if (opts?.queryParams) {
      const queryStr = Object.entries(opts.queryParams)
        .map(([k, v]) => k + '=' + v)
        .join('&');
      url += '?' + queryStr;
    }

    xhr.open(method, url);
    xhr.onload = function () {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response);
      } else {
        reject({
          status: xhr.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: xhr.status,
        statusText: xhr.statusText
      });
    };
    if (method !== 'GET' && opts?.body) {
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify(opts?.body));
    } else {
      xhr.send();
    }
  });
}

function handleConfigMessage(
  event: MessageEvent<any>,
  config: DataAttributesConfig & { iframeId: string }
) {
  if (event.data && event.data.type === 'lemonpi-awaiting-config') {
    event.source.postMessage(
      {
        type: 'lemonpi-config',
        config
      },
      {
        targetOrigin: event.origin
      }
    );
  }
}

const blockedHtml = (config: { environment?: Environment }) => `
      <!DOCTYPE html>
      <html>
        <head>
          <style>
            body {
              margin: 0;
              padding: 0;
              background-color: #e0e0e0;
              width: 100%;
              height: 100vh;
              display: flex;
              justify-content: center;
              align-items: center;
              font-family: Arial, sans-serif;
              color: #666;
            }
            .blocked-message {
              text-align: center;
              padding: 20px;
            }
          </style>
        </head>
        <body>
          <div class="blocked-message">
            <h2>Content Unavailable</h2>
            <p>This content cannot be displayed.</p>
          </div>
          <script type="text/javascript" src="https://creative-libraries${envSubdomain(
            config.environment || 'prod'
          )}.lemonpi.io/lemonpi.js"></script>
        </body>
      </html>
    `;

let lemonpiClickTag: string | null = null;
function processTemplate(
  data: TemplateData,
  config: any,
  dimensions: Dimensions
) {
  const iframe = createIframe(dimensions);

  if ('blocked' in data && data.blocked === true) {
    const dataUrl =
      'data:text/html;charset=utf-8,' + encodeURIComponent(blockedHtml(config));

    iframe.src = dataUrl;
  } else if ('base-url' in data) {
    const templateBaseUrl = data['base-url'];

    config.creativeId = data['template-id'];
    config.creativeRevisionId = data.id;

    const iframeSrc =
      templateBaseUrl.substring(-1) === '/'
        ? templateBaseUrl + 'index.html' + '?_lemonpiDisplayTag'
        : templateBaseUrl + '/index.html' + '?_lemonpiDisplayTag';

    iframe.src = iframeSrc;
  }
  window.addEventListener('message', function (event: MessageEvent<any>) {
    const configWithIframeId = {
      ...config,
      iframeId: iframe.id,
      ...(lemonpiClickTag ? { lemonpiClickTag } : {})
    };
    handleConfigMessage(event, configWithIframeId);
  });
  appendElement(iframe);
}

// making ts compiler happy
function parseJsonResponse(response: any) {
  return JSON.parse(response);
}

type TemplateData =
  | { 'base-url': string; 'template-id'?: number; id?: number }
  | { blocked: boolean };

type Dimensions = { width: string; height: string };

// Get the cookies from backend
let cookies: any = {};

window._lemonpiDislayTagCookiesLoaded = function (result) {
  cookies = result;
  if (window._lemonpiCookiesPromiseResolve) {
    window._lemonpiCookiesPromiseResolve(result);
    window._lemonpiCookiesPromiseResolve = null;
  }
};

async function fetchCookies(
  config: DataAttributesConfig
): Promise<[string, any]> {
  try {
    const cookiesPromise = new Promise((res) => {
      window._lemonpiCookiesPromiseResolve = res;

      const tag = document.createElement('script');

      tag.onerror = function () {
        // eslint-disable-next-line no-console
        console.error('Failed to load cookies script');
        res({});
        window._lemonpiCookiesPromiseResolve = null;
      };

      const cookiesUrl = getLemonpiCookiesUrl(
        config.environment || 'prod',
        config.advertiserId
      );

      tag.src = cookiesUrl + '?callback=_lemonpiDislayTagCookiesLoaded';
      appendElement(tag);
    });

    const timeoutPromise = new Promise((res) => {
      setTimeout(() => {
        if (window._lemonpiCookiesPromiseResolve) {
          res({});
          window._lemonpiCookiesPromiseResolve = null;
        }
      }, 2000);
    });

    // Wait for those delicious chocolate chip cookies or not
    await Promise.race([cookiesPromise, timeoutPromise]);

    return ['$request', cookies];
  } catch (_) {
    // proceed without cookie information
  }
}

/**
 * Initializing context at this stage is needed to fetch the content.
 * As now we are at the display-tag flow we only need
 * the 'lemonpi' parts of the context.
 */
async function initContext(config: DataAttributesConfig) {
  const timestamp = getTimestamp();
  const cookies = await fetchCookies(config);
  const gdprAttrs = parseLemonpiGdprAttributes();
  const macros = parseMacros();
  const nativeData: [string, Record<string, any>] = [
    '$request',
    nativeDataStore.getData()
  ];

  const args: [string, any][] = [
    timestamp,
    gdprAttrs,
    macros,
    cookies,
    nativeData
  ];

  return args
    .filter((kv) => kv)
    .reduce((acc, [k, v]) => {
      if (k === 'lemonpi') {
        return merge(acc, v);
      } else {
        const obj: Record<string, any> = {};
        obj[k] = v;
        return merge(acc, obj);
      }
    }, {});
}

export async function displayTagScript() {
  // Get template information
  const displayDataset = { ...currentScript.dataset };
  const config = JSON.parse(b64DecodeUnicode(displayDataset.lemonpiConfig));
  const dimensions = {
    width: displayDataset.width,
    height: displayDataset.height
  };

  if (displayDataset.clickTag) {
    try {
      new URL(displayDataset.clickTag);
      lemonpiClickTag = displayDataset.clickTag;
    } catch (e) {
      // do nothing
    }
  }
  //Log the display tag
  config.impressionId = uuidv4();
  // Capture native data
  captureNativeData(config);
  //Fetch the template and append to body
  const env = config.environment || 'prod';
  const templateProvider = `https://content${envSubdomain(env)}.lemonpi.io`;
  const getRenderUrl = createGetRenderUrl(templateProvider, config, dimensions);

  if (config.template) {
    processTemplate(config.template, config, dimensions);
  } else if (getRenderUrl) {
    config.context = await initContext(config);

    const macroParams = extractMacrosAsParamsFromContext(config.context);

    const requestOpts = {
      queryParams: Object.assign(macroParams, {
        'impression-id': config.impressionId
      }),
      body: { context: config.context }
    };
    makeRequest('POST', getRenderUrl, requestOpts)
      .then((response) => {
        const json = parseJsonResponse(response);
        if (json.blocked === true) {
          return json as TemplateData;
        }
        config.lemonpiContent = json.content;
        return json.template;
      })
      .catch(async () => {
        if (config.adsetFallbackUrl) {
          const response = await makeRequest('GET', config.adsetFallbackUrl);
          const json = parseJsonResponse(response);
          if (json.blocked === true) {
            return json as TemplateData;
          }
          // This makes lemonpi.js to load the fallback values.
          config.exportContent = json.content;
          displayTagEvent('display-tag-fallback-used', config);
          return json.template;
        } else {
          return Promise.reject();
        }
      })
      .then((data: TemplateData) => {
        processTemplate(data, config, dimensions);
      })
      .catch((e) => {
        // eslint-disable-next-line no-console
        console.error('display-template-failed', e);
        displayTagEvent('display-template-failed', config);
      });
  } else {
    const dynamicContentUrl = createDynamicContentUrl(
      templateProvider,
      config,
      dimensions
    );

    makeRequest('GET', dynamicContentUrl)
      .then((response) => {
        return parseJsonResponse(response);
      })
      .catch(async () => {
        if (config.adsetFallbackUrl) {
          const response = await makeRequest('GET', config.adsetFallbackUrl);
          const json = parseJsonResponse(response);
          if (json.blocked === true) {
            return json as TemplateData;
          }
          // This makes lemonpi.js to load the fallback values.
          config.exportContent = json.content;
          displayTagEvent('display-tag-fallback-used', config);
          return json.template;
        } else {
          return Promise.reject();
        }
      })
      .then((data: TemplateData) => {
        processTemplate(data, config, dimensions);
      })
      .catch((e) => {
        // eslint-disable-next-line no-console
        console.error('display-template-failed', e);
        displayTagEvent('display-template-failed', config);
      });
  }
}

displayTagScript();
