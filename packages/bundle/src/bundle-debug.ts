/**
 * Module that combines all other modules in the lemonpi-creative-libraries.
 * This module ensures that all the other modules are initialized correctly and
 * serves as a simple to use high-level interface to the various modules.
 *
 * Dynamic content is automatically loaded and replaced in your creative by
 * including this library. You can programatically gain access to the content
 * by using the {@link module:bundle~subscribe|subscribe} method.
 *
 * @module bundle
 */
import 'core-js/actual';
import './lemonpi-store';
import { dispatchCustomEvent } from './dispatch-event';
import '../../config-provider/src/lemonpi-config-provider';
import '../../context/src/lemonpi-context';
import '../../content/src/lemonpi-content';
import '../../content/src/preload';
import '../../dcp/src/dcp';
import '../../dom/src/lemonpi-dom';
import '../../interaction-handler/src/interaction-handler';
import '../../engagement-metrics/src/lemonpi-engagement-handler';
import '../../logger/src/logger';
import '../../logger/src/postmessage-logger';
import { BinaryFunc, UnaryFunc } from '../../../types/utility.types';
import { enableDebugMode } from './debug-events';

let lastContent: any;
let lastContext: unknown;
let lastConfig: unknown;
let didRender: boolean;

window.addEventListener('lemonpi.content/ready', (event: CustomEvent): void => {
  lastContent = event.detail;
});
window.addEventListener('lemonpi.content/rendered', () => {
  didRender = true;
});

window.addEventListener('lemonpi.config/ready', (event: CustomEvent): void => {
  lastConfig = event.detail;
});
window.addEventListener('lemonpi.context/ready', (event: CustomEvent): void => {
  lastContext = event.detail;
});

// Expose context.subscribe
export const context = {
  subscribe: (cb: UnaryFunc) => {
    const handler = (e: CustomEvent) => {
      lastContext = e.detail;
      cb(lastContext);
    };

    window.addEventListener('lemonpi.context/ready', handler);

    if (lastContext) {
      cb(lastContext);
    }

    return () => window.removeEventListener('lemonpi.context/ready', handler);
  }
};

// Expose config.subscribe
export const config = {
  subscribe: (cb: UnaryFunc) => {
    const handler = (e: CustomEvent) => {
      lastConfig = e.detail;
      cb(lastConfig);
    };

    window.addEventListener('lemonpi.config/ready', handler);

    if (lastConfig) {
      cb(lastConfig);
    }

    return () => window.removeEventListener('lemonpi.config/ready', handler);
  }
};

/**
 * Subscribe to changes in dynamic content using a callback function. When
 * content is rendered, or already rendered upon subscribing, the callback handler
 * function will be called with the resolved content.
 *
 * @access public
 * @param {function} handler callback
 * @return {function} Unsubscribe
 */
export function subscribe(cb: BinaryFunc) {
  const onRendered = () => {
    if (!lastContent) {
      throw new Error(
        'Invariant: lemonpi.content/rendered dispatched before lemonpi.content/ready'
      );
    }
    cb(lastContent.content, lastContent.source);
  };

  window.addEventListener('lemonpi.content/rendered', onRendered);

  if (didRender) {
    onRendered();
  }

  return () =>
    window.removeEventListener('lemonpi.content/rendered', onRendered);
}

/**
 * Subscribe to specific events. Possible event names include 'content' and
 * 'dom-replaced'
 *
 * @access public
 * @param  {string} eventName event to subscribe to
 * @param  {function} handler callback
 * @return {function} Unsubscribe
 */
export function on(name: string, cb: UnaryFunc) {
  dispatchCustomEvent('lemonpi.debug/on-handler', { name });

  if (name === 'dom-replaced') {
    const handler = () => cb();
    window.addEventListener('lemonpi.content/rendered', handler);
    return () =>
      window.removeEventListener('lemonpi.content/rendered', handler);
  }

  if (name === 'content') {
    const handler = (e: CustomEvent) => cb(e.detail.content);
    window.addEventListener('lemonpi.content/ready', handler);
    return () => window.removeEventListener('lemonpi.content/ready', handler);
  }

  return () => ({});
}

function lemonpiInit() {
  dispatchCustomEvent('lemonpi/start', undefined);
  enableDebugMode();
}

lemonpiInit();
