import { v4 as uuidv4 } from 'uuid';

type LemonpiStore = {
  getUuidAndCount: () => {
    lifecycleCount: number;
    lemonpiUuid: string | null;
  };
  getIsDebugEnabled: () => boolean;
  setDebugEnable: (debugMode: boolean) => boolean;
};
declare global {
  interface Window {
    lemonpiStore: LemonpiStore | null;
  }
}

(function () {
  const lemonpiStore = (function () {
    let instance: LemonpiStore | null = null;
    let lifecycleCount = 0;
    let lemonpiUuid: string | null = null;
    let isDebugEnabled = false;

    function createInstance() {
      lemonpiUuid = uuidv4();
      window.addEventListener('lemonpi/start', () => {
        lifecycleCount++;
      });
      return {
        getUuidAndCount: function () {
          return { lemonpiUuid, lifecycleCount };
        },
        getIsDebugEnabled: function () {
          return isDebugEnabled;
        },
        setDebugEnable: function (debugMode: boolean) {
          isDebugEnabled = debugMode;
          return isDebugEnabled;
        }
      };
    }

    return {
      getInstance: function () {
        if (!instance) {
          instance = createInstance();
        }
        return instance;
      }
    };
  })();

  const instance = lemonpiStore.getInstance();
  Object.freeze(instance);
  if (!window.lemonpiStore) {
    window.lemonpiStore = instance;
  }
})();

export {};
