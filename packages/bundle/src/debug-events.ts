import { MACRO_EVENT } from '../../interaction-handler/src/interaction-handler';
import { replaceMacro } from '../../interaction-handler/src/interaction-handler';
import { LemonpiConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import { getInteractionHandlerUrl } from '../../config-provider/src/lemonpi-config-provider';

let lastConfig: unknown;

export const LEMONPI_EVENT = {
  CONTENT_RENDERED: 'lemonpi.content/rendered',
  BEFORE_UNLOAD: 'lemonpi.debug/before-unload',
  ERROR_EVENT: 'lemonpi.debug/error-event',
  OMSKD_READY: 'lemonpi.omsdk/ready',
  OMSKD_IMPRESSION: 'lemonpi.omsdk/impression'
} as const;

type LemonpiEventType = typeof LEMONPI_EVENT[keyof typeof LEMONPI_EVENT];
let lastEvent: LemonpiEventType | null = null;

export const isConfigType = (config: unknown): config is LemonpiConfig => {
  if (
    typeof config === 'object' &&
    'adsetId' in config &&
    'creativeId' in config &&
    'impressionId' in config &&
    'advertiserId' in config
  ) {
    return true;
  }
  return false;
};
export const isLiveTemplate = (config: unknown): boolean => {
  if (typeof config === 'object' && 'creativeEnv' in config)
    return config.creativeEnv === 'live' || config.creativeEnv === 'displayTag';
};

export function encodeData(
  eventName: LemonpiEventType,
  payload?: Record<string, unknown>
) {
  const { lemonpiUuid, lifecycleCount } = window.lemonpiStore.getUuidAndCount();
  let event: Record<string, string | number | object> = {
    type: 'debug',
    name: eventName,
    data: {
      lemonpiUuid: lemonpiUuid,
      lifecycleCount: lifecycleCount,
      ...payload
    }
  };
  if (lastConfig && isConfigType(lastConfig)) {
    event = {
      ...event,
      adsetId: lastConfig.adsetId,
      creativeId: lastConfig.creativeId,
      impressionId: lastConfig.impressionId,
      advertiserId: lastConfig.advertiserId
    };
  }
  const eventUrl = replaceMacro(
    getInteractionHandlerUrl('prod'),
    MACRO_EVENT,
    JSON.stringify(event)
  );
  return eventUrl;
}
export function beaconData(
  eventName: LemonpiEventType,
  payload?: Record<string, unknown>
) {
  const { lemonpiUuid, lifecycleCount } = window.lemonpiStore.getUuidAndCount();
  let event: Record<string, string | number | object> = {
    type: 'debug',
    name: eventName,
    data: {
      lemonpiUuid: lemonpiUuid,
      lifecycleCount: lifecycleCount,
      ...payload
    }
  };
  if (lastConfig && isConfigType(lastConfig)) {
    event = {
      ...event,
      adsetId: lastConfig.adsetId,
      creativeId: lastConfig.creativeId,
      impressionId: lastConfig.impressionId,
      advertiserId: lastConfig.advertiserId
    };
  }

  const url = getInteractionHandlerUrl('prod');
  const eventData = new Blob([JSON.stringify(event)], {
    type: 'application/json'
  });

  return { url, eventData };
}

export function lemonpiEvent(
  eventName: LemonpiEventType,
  payload?: Record<string, unknown>
) {
  if (!isLiveTemplate(lastConfig)) return;
  const encodedUrl = encodeData(eventName, payload);
  const imgTag = document.createElement('img');
  imgTag.src = encodedUrl;
  lastEvent = eventName;
}

function onEarlyUnload() {
  const payload = {
    lastEvent
  };

  const { url, eventData } = beaconData(LEMONPI_EVENT.BEFORE_UNLOAD, payload);

  if (navigator.sendBeacon && url && eventData) {
    navigator.sendBeacon(url, eventData);
  }
}

export function enableDebugMode() {
  if (window.lemonpiStore.getIsDebugEnabled()) return;

  window.lemonpiStore.setDebugEnable(true);
  window.addEventListener('beforeunload', onEarlyUnload);
  Object.values(LEMONPI_EVENT).forEach((eventName: LemonpiEventType) => {
    if (
      eventName === LEMONPI_EVENT.BEFORE_UNLOAD ||
      eventName === LEMONPI_EVENT.ERROR_EVENT
    ) {
      return;
    }
    window.addEventListener(eventName, () => {
      lemonpiEvent(eventName);

      if (eventName === LEMONPI_EVENT.CONTENT_RENDERED) {
        window.removeEventListener('beforeunload', onEarlyUnload);
      }
    });
  });
}

window.addEventListener('lemonpi.config/ready', (event: CustomEvent): void => {
  lastConfig = event.detail;
});
