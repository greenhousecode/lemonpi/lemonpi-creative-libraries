// Polyfill CustomEvent constructor
// https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent

type CustomEventPolifill = {
  new <T>(event: string, params?: CustomEventInit<T>): CustomEvent<T>;
};

(function () {
  if (typeof window.CustomEvent === 'function') return false;

  function CustomEvent(event: string, params: any) {
    params = params || { bubbles: false, cancelable: false, detail: null };
    const evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(
      event,
      params.bubbles,
      params.cancelable,
      params.detail
    );
    return evt;
  }

  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent as unknown as CustomEventPolifill;
})();

/**
 * Simple wrapper for dispatchEvent
 *
 * @param {string} event
 * @param {any|undefined} data
 */
export const dispatchCustomEvent = (event: string, data: any) => {
  window.dispatchEvent(new CustomEvent(event, { detail: data }));
};
