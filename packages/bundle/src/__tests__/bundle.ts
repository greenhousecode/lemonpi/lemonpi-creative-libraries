let bundle: any;

const mockModule = (path: any) => jest.mock(path, () => ({}));

beforeEach(() => {
  jest.resetModules();
  mockModule('../lemonpi-store');
  jest.mock('../debug-events', () => ({
    enableDebugMode: jest.fn(),
    isConfigType: jest.fn()
  }));
  mockModule('../../../config-provider/src/lemonpi-config-provider');
  mockModule('../../../context/src/lemonpi-context');
  mockModule('../../../content/src/lemonpi-content');
  mockModule('../../../content/src/preload');
  mockModule('../../../dcp/src/dcp');
  mockModule('../../../dom/src/lemonpi-dom');
  mockModule('../../../interaction-handler/src/interaction-handler');
  mockModule('../../../engagement-metrics/src/lemonpi-engagement-handler');
  mockModule('../../../logger/src/logger');
  mockModule('../../../logger/src/postmessage-logger');
  bundle = require('../bundle');
});

describe('bundle backwards compatibility', () => {
  describe('content subscribe', () => {
    test('callback is called when content is rendered', () => {
      const handler = jest.fn();
      bundle.subscribe(handler);

      expect(handler).not.toHaveBeenCalled();

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/ready', {
          detail: { content: { k: 'v' }, source: 'test' }
        })
      );
      window.dispatchEvent(new CustomEvent('lemonpi.content/rendered'));

      expect(handler).toHaveBeenCalledWith({ k: 'v' }, 'test');
    });

    test('callback is called when content has already rendered', () => {
      const handler = jest.fn();

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/ready', {
          detail: { content: { k: 'v' }, source: 'test' }
        })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.content/rendered'));

      bundle.subscribe(handler);
      expect(handler).toHaveBeenCalledWith({ k: 'v' }, 'test');
    });

    test("Error is thrown when content is rendered before it's ready", () => {
      bundle.subscribe(jest.fn());
      expect(() =>
        window.dispatchEvent(new CustomEvent('lemonpi.content/rendered'))
      ).toThrow();
    });
  });

  describe('context subscribe', () => {
    test('callback is called when context is received', () => {
      const handler = jest.fn();
      bundle.context.subscribe(handler);
      expect(handler).not.toHaveBeenCalled();

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', {
          detail: { appnexus: { k: 'v' } }
        })
      );

      expect(handler).toHaveBeenCalledWith({ appnexus: { k: 'v' } });
    });

    test('callback is called when context has already resolved', () => {
      const handler = jest.fn();

      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', {
          detail: { appnexus: { k: 'v' } }
        })
      );

      bundle.context.subscribe(handler);
      expect(handler).toHaveBeenCalledWith({ appnexus: { k: 'v' } });
    });
  });

  describe('config subscribe', () => {
    test('callback is called when config is received', () => {
      const handler = jest.fn();
      bundle.config.subscribe(handler);

      expect(handler).not.toHaveBeenCalled();

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', {
          detail: { k: 'v' }
        })
      );

      expect(handler).toHaveBeenCalledWith({ k: 'v' });
    });

    test('callback is called when config has already resolved', () => {
      const handler = jest.fn();

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', {
          detail: { k: 'v' }
        })
      );

      bundle.config.subscribe(handler);
      expect(handler).toHaveBeenCalledWith({ k: 'v' });
    });
  });

  describe('on', () => {
    test('dispatches debug/on-handler', () => {
      const handler = jest.fn();
      window.addEventListener('lemonpi.debug/on-handler', handler);

      bundle.on('dom-replaced', () => ({}));
      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({ detail: { name: 'dom-replaced' } })
      );
    });

    test('calls callback when content is resolved', () => {
      const handler = jest.fn();
      bundle.on('content', handler);

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/ready', {
          detail: { content: { k: 'v' }, source: 'test' }
        })
      );

      expect(handler).toHaveBeenCalledWith({ k: 'v' });
    });

    test('calls callback when dom is replaced', () => {
      const handler = jest.fn();
      bundle.on('dom-replaced', handler);

      window.dispatchEvent(new CustomEvent('lemonpi.content/rendered'));
      expect(handler).toHaveBeenCalledWith();
    });
  });
});
