describe('logging module', () => {
  let xhrInstances, XMLHttpRequestCopy;
  let config = {
    adsetId: 1,
    creativeId: 2,
    logProxyUrl: 'https://log.test.lemonpi.io/log'
  };
  jest.useFakeTimers();
  beforeEach(() => {
    jest
      .spyOn(Date, 'now')
      .mockImplementationOnce(() => 1)
      .mockImplementationOnce(() => 2);
    require('../logger.ts');
    xhrInstances = [];
    XMLHttpRequestCopy = global.XMLHttpRequest;
    global.XMLHttpRequest = jest.fn(() => {
      const instance = {
        open: jest.fn(),
        send: jest.fn()
      };
      xhrInstances.push(instance);
      return instance;
    });
  });
  afterEach(() => {
    global.XMLHttpRequest = XMLHttpRequestCopy;
  });

  test('sends google logs with success flag true', () => {
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: config
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/partial', {
        detail: { key: 'google-cm', context: { advertiserId: 1 } }
      })
    );
    expect(xhrInstances.length).toBe(1);
    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      success: 1,
      duration: 1,
      type: 'google-macro-load'
    };
    expect(body).toEqual(result);
  });

  test('sends google logs with success flag false', () => {
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: config
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/partial', {
        detail: { key: 'google-cm', context: {} }
      })
    );
    expect(xhrInstances.length).toBe(1);
    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      success: 0,
      duration: 1,
      type: 'google-macro-load'
    };
    expect(body).toEqual(result);
  });

  test('sends appnexus logs with success flag false', () => {
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: config
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/partial', {
        detail: { key: 'appnexus', context: {} }
      })
    );
    expect(xhrInstances.length).toBe(1);
    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      success: 0,
      duration: 1,
      type: 'appnexus-macro-load'
    };
    expect(body).toEqual(result);
  });

  test('sends appnexus logs with success flag true', () => {
    window.dispatchEvent(new CustomEvent('lemonpi/start'));
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: config
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/partial', {
        detail: { key: 'appnexus', context: { advertiserId: 1 } }
      })
    );

    expect(xhrInstances.length).toBe(1);
    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      success: 1,
      duration: 1,
      type: 'appnexus-macro-load'
    };
    expect(body).toEqual(result);
  });

  test('sends on-handler deprecation logs', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: config
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.debug/on-handler', {
        detail: { name: 'content' }
      })
    );

    expect(xhrInstances.length).toBe(1);
    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      type: 'deprecated-on-handler',
      name: 'content'
    };
    expect(body).toEqual(result);
  });

  test('sends dco request log with appnexus', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: {
          adserver: { code: 'appnexus' },
          ...config
        }
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', {
        detail: {
          appnexus: {}
        }
      })
    );
    window.dispatchEvent(new CustomEvent('lemonpi.content/fetched', {}));

    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      adserver: 'appnexus',
      'adserver-context': true,
      duration: 1,
      type: 'dco-request'
    };
    expect(body).toEqual(result);
  });

  test('sends dco request log with dcm', () => {
    window.dispatchEvent(
      new CustomEvent('lemonpi.config/ready', {
        detail: {
          adserver: { code: 'dcm' },
          ...config
        }
      })
    );

    window.dispatchEvent(
      new CustomEvent('lemonpi.context/ready', {
        detail: {
          'google-cm': {}
        }
      })
    );
    window.dispatchEvent(new CustomEvent('lemonpi.content/fetched', {}));

    let body = JSON.parse(xhrInstances[0].send.mock.calls[0][0]);
    let result = {
      'adset-id': 1,
      'creative-id': 2,
      adserver: 'google-cm',
      'adserver-context': true,
      duration: 1,
      type: 'dco-request'
    };
    expect(body).toEqual(result);
  });
});
