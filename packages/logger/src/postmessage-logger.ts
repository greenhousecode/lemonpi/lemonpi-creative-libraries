window.addEventListener('lemonpi/config-ready', () => {
  parent.postMessage(
    { type: 'lemonpi/log', eventType: 'lemonpi/config-ready' },
    '*'
  );
});

window.addEventListener('lemonpi.content/ready', () => {
  parent.postMessage(
    { type: 'lemonpi/log', eventType: 'lemonpi.content/ready' },
    '*'
  );
});

window.addEventListener('lemonpi.content/fetched', () => {
  parent.postMessage(
    { type: 'lemonpi/log', eventType: 'lemonpi.content/fetched' },
    '*'
  );
});

window.addEventListener('lemonpi.content/rendered', () => {
  parent.postMessage(
    { type: 'lemonpi/log', eventType: 'lemonpi.content/rendered' },
    '*'
  );
});

window.addEventListener(
  'lemonpi.content/preload-error',
  (event: CustomEvent) => {
    parent.postMessage(
      {
        type: 'lemonpi/log',
        eventType: 'lemonpi.content/preload-error',
        error: event.detail
      },
      '*'
    );
  }
);
