import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';

let startTime: number;
let config: LemonpiEnrichedConfig | null;
let dcoRequestStart: any;
let dcoContext: any;

interface EventData {
  'adset-id': number;
  'creative-id': number;
  type: string;
  duration?: number;
  success?: number;
  adserver?: unknown;
  'adserver-context'?: boolean;
}

function logEvent(data: EventData) {
  if (!config.logProxyUrl) return;
  const xhr = new XMLHttpRequest();
  xhr.open('POST', config.logProxyUrl, true);
  xhr.send(JSON.stringify(data));
}

function setStartTime() {
  startTime = Date.now();
}

function logGoogle(context: any) {
  const dataSend = {
    'adset-id': config.adsetId,
    'creative-id': config.creativeId,
    type: 'google-macro-load',
    duration: Date.now() - startTime,
    success: Object.keys(context).length !== 0 ? 1 : 0
  };

  logEvent(dataSend);
}

function logAppnexus(context: any) {
  const dataSend = {
    'adset-id': config.adsetId,
    'creative-id': config.creativeId,
    type: 'appnexus-macro-load',
    duration: Date.now() - startTime,
    success: Object.keys(context).length !== 0 ? 1 : 0
  };

  logEvent(dataSend);
}

function logDeprecatedOnHandler(name: string) {
  const dataSend = {
    'adset-id': config.adsetId,
    'creative-id': config.creativeId,
    type: 'deprecated-on-handler',
    name
  };

  logEvent(dataSend);
}

window.addEventListener('lemonpi/start', setStartTime);

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
});

window.addEventListener('lemonpi.context/partial', (event: CustomEvent) => {
  if (event.detail.key === 'google-cm') logGoogle(event.detail.context);
  if (event.detail.key === 'appnexus') logAppnexus(event.detail.context);
});

window.addEventListener('lemonpi.debug/on-handler', (event: CustomEvent) => {
  logDeprecatedOnHandler(event.detail.name);
});

window.addEventListener('lemonpi.context/ready', (event: CustomEvent) => {
  dcoContext = event.detail;
  dcoRequestStart = Date.now();
});

window.addEventListener('lemonpi.content/fetched', () => {
  let code = config && config.adserver && config.adserver.code;
  code = { dcm: 'google-cm' }[code as 'dcm'] || code;
  const hasAdserverContext = !!dcoContext && !!dcoContext[code];

  logEvent({
    'adset-id': config.adsetId,
    'creative-id': config.creativeId,
    adserver: code,
    'adserver-context': hasAdserverContext,
    type: 'dco-request',
    duration: Date.now() - dcoRequestStart
  });
});
