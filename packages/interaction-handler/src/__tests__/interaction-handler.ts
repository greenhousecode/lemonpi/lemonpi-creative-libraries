import { v4 as uuidv4 } from 'uuid';
import { getDataAtrributesClickTag } from '../interaction-handler';

const interactionHandlerUrl = 'https://track.lemonpi.io/events';
const interactionHandlerUrlWithEventPlaceholder =
  interactionHandlerUrl + '?e=LEMONPI%25EVENTDATA';

function configureCreativeLibraries(config: any) {
  window.dispatchEvent(
    new CustomEvent('lemonpi.config/ready', {
      detail: {
        interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
        adsetId: 1,
        creativeId: 2,
        impressionId: '00000000-0000-0000-0000-000000000000',
        version: 2,
        adserver: { code: 'lemonpi' },
        ...config
      }
    })
  );
}

function fetchedContent(content: any) {
  window.dispatchEvent(
    new CustomEvent('lemonpi.content/fetched', {
      detail: {
        content: content
      }
    })
  );
}

function clickOnAd({ placeholder = 'click1', query = {} } = {}) {
  window.dispatchEvent(
    new CustomEvent('lemonpi.interaction/click', {
      detail: { placeholder: placeholder, query: query }
    })
  );
}

describe('Interaction handler module', () => {
  const originalOpen = window.open;
  const { location } = window;

  beforeAll(() => {
    delete window.location;
    window.location = {
      href: '',
      search: ''
    } as any;
  });
  beforeEach(() => {
    jest.resetModules();
    window.open = jest.fn();
    jest.spyOn(window, 'open');
    window.lemonpiStore = {
      getIsDebugEnabled: jest.fn(),
      getUuidAndCount: jest.fn(),
      setDebugEnable: jest.fn()
    };
    require('../interaction-handler');
  });

  afterEach(() => {
    window.open = originalOpen;
  });

  afterAll(() => {
    window.location = location;
  });
  describe('interaction/click', () => {
    test('navigates to placeholder url', () => {
      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', {
          detail: {
            content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
          }
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.interaction/click', {
          detail: { placeholder: 'click1' }
        })
      );

      expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
    });

    test('dispatch url/open event', () => {
      const handler = jest.fn();
      window.addEventListener('lemonpi.url/open', handler);

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', {
          detail: {
            content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
          }
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.interaction/click', {
          detail: { placeholder: 'click1', elementId: 'background', query: {} }
        })
      );

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: {
            elementId: 'background',
            url: 'http://lemonpi.io',
            query: {}
          }
        })
      );
    });

    test('dispatch url/open event with collection placeholder', () => {
      const handler = jest.fn();
      window.addEventListener('lemonpi.url/open', handler);
      const content = {
        my_collection: {
          type: 'collection',
          value: [
            {
              image: { type: 'image', value: 'https://image.com' },

              url: {
                type: 'click',
                value: 'https://www.lemonpi.io/?product=lemons'
              }
            }
          ]
        }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', {
          detail: {
            content: content
          }
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.interaction/click', {
          detail: {
            placeholder: ['my_collection', 0, 'url']
          }
        })
      );

      expect(handler).toHaveBeenCalledWith(
        expect.objectContaining({
          detail: {
            elementId: undefined,
            url: 'https://www.lemonpi.io/?product=lemons',
            query: undefined
          }
        })
      );
    });

    test('ignores invalid event', () => {
      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', {
          detail: {}
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', {
          detail: {
            content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
          }
        })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/click'));

      window.dispatchEvent(
        new CustomEvent('lemonpi.interaction/click', {
          detail: {}
        })
      );

      expect(window.open).not.toHaveBeenCalled();
    });

    test('ignores unknown adservers', () => {
      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', {
          detail: { adserver: { code: 'non-existing' } }
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', {
          detail: {
            content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
          }
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.interaction/click', {
          detail: { placeholder: 'click1' }
        })
      );

      expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
    });

    describe('prepends appnexus click tracker', () => {
      let originalHref: string;

      beforeEach(() => {
        originalHref = window.location.href;

        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: { adserver: { code: 'appnexus' } }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );
      });

      afterEach(() => {
        window.location.href = originalHref;
      });

      test('unless clickTag is not in url', () => {
        window.location.href = 'http://www.url.to.creative.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('unless clickTag is malformed', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('with no encoding', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=http%3A%2F%2Fwww.appnexus.com%2Ftrack%3Fr%3Dhttps%3A%2F%2Fdonotchangethis.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.appnexus.com/track?r=http://lemonpi.io',
          '_blank'
        );
      });

      test('encoded once', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=http%3A%2F%2Fwww.appnexus.com%2Ftrack%3Fr%3Dhttps%253A%252F%252Fdonotchangethis.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.appnexus.com/track?r=http%3A%2F%2Flemonpi.io',
          '_blank'
        );
      });

      test('encoded twice', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=http%3A%2F%2Fwww.appnexus.com%2Ftrack%3Fr%3Dhttps%25253A%25252F%25252Fdonotchangethis.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.appnexus.com/track?r=http%253A%252F%252Flemonpi.io',
          '_blank'
        );
      });
    });

    describe('prepends google click tracker', () => {
      beforeEach(() => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: { adserver: { code: 'google-cm' } }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );
      });

      afterEach(() => {
        window.clickTag1 = undefined;
      });

      test('unless clickTag is missing', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('unless clickTag is malformed', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=SOMETHING-ELSE';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=SOMETHING-ELSE',
          '_blank'
        );
      });

      test('with no encoding', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%CLICKURL';
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=http://lemonpi.io',
          '_blank'
        );
      });

      test('encoded once', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%25CLICKURL';
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=http%3A%2F%2Flemonpi.io',
          '_blank'
        );
      });

      test('encoded twice', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%2525CLICKURL';
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=http%253A%252F%252Flemonpi.io',
          '_blank'
        );
      });
    });

    describe('prepends adform click tracker', () => {
      beforeEach(() => {
        window.dhtml = {} as any;
        window.dhtml.getVar = (): any => undefined;

        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: { adserver: { code: 'adform' } }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );
      });

      test('unless clickTag is missing', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('if MACRO_DO_NOT_CHANGE_THIS not in url, we should just append `;cpdir=` with the redirect url', () => {
        window.dhtml.getVar = () => 'http://someone-changed-it.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://someone-changed-it.lemonpi.io;cpdir=http://lemonpi.io',
          '_blank'
        );
      });

      test('with no encoding', () => {
        window.dhtml.getVar = () =>
          'http://www.adform.com/track?r=https%3A%2F%2Fdonotchangethis.lemonpi.io';
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.adform.com/track?r=http%3A%2F%2Flemonpi.io',
          '_blank'
        );
      });

      test('encoded once', () => {
        window.dhtml.getVar = () =>
          'http://www.adform.com/track?r=https%253A%252F%252Fdonotchangethis.lemonpi.io';
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.adform.com/track?r=http%253A%252F%252Flemonpi.io',
          '_blank'
        );
      });

      test('encoded twice', () => {
        window.dhtml.getVar = () =>
          'http://www.adform.com/track?r=https%25253A%25252F%25252Fdonotchangethis.lemonpi.io';
        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.adform.com/track?r=http%25253A%25252F%25252Flemonpi.io',
          '_blank'
        );
      });
    });

    describe('prepends trade desk click tracker', () => {
      let originalHref: string;

      beforeEach(() => {
        originalHref = window.location.href;

        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: { adserver: { code: 'trade-desk' } }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );
      });

      afterEach(() => {
        window.location.href = originalHref;
      });

      test('unless clickTag is not in url', () => {
        window.location.href = 'http://www.url.to.creative.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('unless clickTag is malformed', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('without macro', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=http%3A%2F%2Fdesk.thetradedesk.com%2Ftrack%2Fclk%3Fr%3D';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://desk.thetradedesk.com/track/clk?r=http%3A%2F%2Flemonpi.io',
          '_blank'
        );
      });

      test('with macro encoded once', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=http%3A%2F%2Fdesk.thetradedesk.com%2Ftrack%2Fclk%3Fr%3Dhttps%253A%252F%252Fdonotchangethis.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://desk.thetradedesk.com/track/clk?r=http%3A%2F%2Flemonpi.io',
          '_blank'
        );
      });

      test('with macro encoded twice', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=http%3A%2F%2Fdesk.thetradedesk.com%2Ftrack%2Fclk%3Fr%3Dhttps%25253A%25252F%25252Fdonotchangethis.lemonpi.io';

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://desk.thetradedesk.com/track/clk?r=http%253A%252F%252Flemonpi.io',
          '_blank'
        );
      });
    });

    describe('prepends lemonpi click trackers with v1 config', () => {
      beforeEach(() => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: {
              interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
              adsetId: 1,
              creativeId: 2,
              impressionId: 'id'
            }
          })
        );
      });

      test('1st party', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: {
                  type: 'click',
                  value: 'http://lemonpi.io'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                type: 'click',
                schema: 'adset-creative',
                adsetId: 1,
                creativeId: 2,
                out: encodeURIComponent('http://lemonpi.io')
              })
            ),
          '_blank'
        );
      });

      test('1st party and 3rd party', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http://lemonpi.io' },
                tracker1: {
                  type: 'click-tracker',
                  value: 'http://my.tracker.com?out=(:landing_url:)'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                type: 'click',
                schema: 'adset-creative',
                adsetId: 1,
                creativeId: 2,
                out: encodeURIComponent('http://lemonpi.io'),
                tracker: encodeURIComponent(
                  'http://my.tracker.com?out=(:landing_url:)'
                )
              })
            ),
          '_blank'
        );
      });
    });

    describe('prepends lemonpi click trackers with v2 config', () => {
      beforeEach(() => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: {
              interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
              adsetId: 1,
              creativeId: 2,
              version: 2,
              impressionId: 'id'
            }
          })
        );
      });

      test('1st party', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: {
                  type: 'click',
                  value: 'http://lemonpi.io'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                version: 2,
                type: 'click',
                schema: 'adset-creative',
                'impression-id': 'id',
                'adset-id': 1,
                'creative-id': 2,
                out: encodeURIComponent('http://lemonpi.io')
              })
            ),
          '_blank'
        );
      });

      test('1st party and 3rd party', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http://lemonpi.io' },
                tracker1: {
                  type: 'click-tracker',
                  value: 'http://my.tracker.com?out=(:landing_url:)'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                version: 2,
                type: 'click',
                schema: 'adset-creative',
                'impression-id': 'id',
                'adset-id': 1,
                'creative-id': 2,
                out: encodeURIComponent('http://lemonpi.io'),
                tracker: encodeURIComponent(
                  'http://my.tracker.com?out=(:landing_url:)'
                )
              })
            ),
          '_blank'
        );
      });
    });

    describe('prepends lemonpi and adserver click trackers v1 config', () => {
      beforeEach(() => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: {
              interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
              adsetId: 1,
              creativeId: 2,
              impressionId: 'id',
              adserver: { code: 'google-cm' }
            }
          })
        );
      });

      test('adserver and 1st party', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%CLICKURL';

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                type: 'click',
                schema: 'adset-creative',
                adsetId: 1,
                creativeId: 2,
                out: encodeURIComponent('http://lemonpi.io')
              })
            ),
          '_blank'
        );
      });

      test('adserver, 1st party and 3rd party', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%CLICKURL';

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http://lemonpi.io' },
                tracker1: {
                  type: 'click-tracker',
                  value: 'http://my.tracker.com?out=(:landing_url:)'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                type: 'click',
                schema: 'adset-creative',
                adsetId: 1,
                creativeId: 2,
                out: encodeURIComponent('http://lemonpi.io'),
                tracker: encodeURIComponent(
                  'http://my.tracker.com?out=(:landing_url:)'
                )
              })
            ),
          '_blank'
        );
      });
    });

    describe('prepends lemonpi and adserver click trackers v2 config', () => {
      beforeEach(() => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: {
              interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
              adsetId: 1,
              creativeId: 2,
              impressionId: 'id',
              version: 2,
              adserver: { code: 'google-cm' }
            }
          })
        );
      });

      test('adserver and 1st party', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%CLICKURL';

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                version: 2,
                type: 'click',
                schema: 'adset-creative',
                'impression-id': 'id',
                'adset-id': 1,
                'creative-id': 2,
                out: encodeURIComponent('http://lemonpi.io')
              })
            ),
          '_blank'
        );
      });

      test('adserver, 1st party and 3rd party', () => {
        window.clickTag1 = 'http://www.dcm.com/track?r=LEMONPI%CLICKURL';

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http://lemonpi.io' },
                tracker1: {
                  type: 'click-tracker',
                  value: 'http://my.tracker.com?out=(:landing_url:)'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1' }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://www.dcm.com/track?r=https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                version: 2,
                type: 'click',
                schema: 'adset-creative',
                'impression-id': 'id',
                'adset-id': 1,
                'creative-id': 2,
                out: encodeURIComponent('http://lemonpi.io'),
                tracker: encodeURIComponent(
                  'http://my.tracker.com?out=(:landing_url:)'
                )
              })
            ),
          '_blank'
        );
      });
    });

    describe('Given a tag served by lemonpi adserver', () => {
      const clickUrl =
        'https://nym1-ib.adnxs-simple.com/click?AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA___________4QlZjAAAAAAEAAABsAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgIAAAAAAAEAYQoXBwAAAAA./bcr=AAAAAAAAAAA=/bn=0/test=1/dnt=1/clickenc=';
      const clickUrlEncoded = encodeURIComponent(clickUrl);

      const impressionId = uuidv4();
      const out = 'http://lemonpi.io';

      const encodedClickEvent = encodeURIComponent(
        JSON.stringify({
          version: 2,
          type: 'click',
          schema: 'adset-creative',
          'impression-id': impressionId,
          'adset-id': 1,
          'creative-id': 2,
          out: encodeURIComponent(out)
        })
      );

      const interactionHandlerUrlWithEncodedClickEvent =
        interactionHandlerUrl + '?e=' + encodedClickEvent;

      beforeEach(() => {
        configureCreativeLibraries({ impressionId: impressionId });
        fetchedContent({ click1: { type: 'click', value: out } });
      });

      test('without a click clickTag=${CLICK_URL_ENC} macro, when clicked', () => {
        window.location.href = 'http://www.url.to.creative.lemonpi.io';

        clickOnAd();

        expect(window.open).toHaveBeenCalledWith(
          interactionHandlerUrlWithEncodedClickEvent,
          '_blank'
        );
      });

      test('with an empty clickTag, when clicked', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=';

        clickOnAd();

        expect(window.open).toHaveBeenCalledWith(
          interactionHandlerUrlWithEncodedClickEvent,
          '_blank'
        );
      });

      test('without materializing the clickTag, when clicked', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?width=300&clickTag=${CLICK_URL_ENC}';

        clickOnAd();

        expect(window.open).toHaveBeenCalledWith(
          interactionHandlerUrlWithEncodedClickEvent,
          '_blank'
        );
      });

      test('with a malformed clickTag, when clicked', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=<ERROR 500>';

        clickOnAd();

        expect(window.open).toHaveBeenCalledWith(
          interactionHandlerUrlWithEncodedClickEvent,
          '_blank'
        );
      });

      test('with a DSP clickTag, when clicked', () => {
        window.location.href =
          'http://www.url.to.creative.lemonpi.io?clickTag=' + clickUrlEncoded;

        clickOnAd();

        expect(window.open).toHaveBeenCalledWith(
          clickUrl +
            encodeURIComponent(interactionHandlerUrlWithEncodedClickEvent),
          '_blank'
        );
      });
    });

    describe('appends query parameters', () => {
      test('noop when there are no query parameters', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1', query: {} }
          })
        );

        expect(window.open).toHaveBeenCalledWith('http://lemonpi.io', '_blank');
      });

      test('when there are no click-trackers and no existing parameters', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: { click1: { type: 'click', value: 'http://lemonpi.io' } }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1', query: { k1: 'v1', k2: 'v2' } }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://lemonpi.io?k1=v1&k2=v2',
          '_blank'
        );
      });

      test('when there are no click-trackers and existing parameters', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http://lemonpi.io?a=b' }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1', query: { k: 'v' } }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http://lemonpi.io?a=b&k=v',
          '_blank'
        );
      });

      test("does not consider the url's encoding", () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http%3A%2F%2Flemonpi.io' }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1', query: { k: 'v' } }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'http%3A%2F%2Flemonpi.io?k=v',
          '_blank'
        );
      });

      test('when there are click-trackers', () => {
        window.dispatchEvent(
          new CustomEvent('lemonpi.config/ready', {
            detail: {
              interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
              adsetId: 1,
              creativeId: 2
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.content/fetched', {
            detail: {
              content: {
                click1: { type: 'click', value: 'http://lemonpi.io?a=b' },
                tracker1: {
                  type: 'click-tracker',
                  value: 'http://my.tracker.com?out=(:landing_url:)'
                }
              }
            }
          })
        );

        window.dispatchEvent(
          new CustomEvent('lemonpi.interaction/click', {
            detail: { placeholder: 'click1', query: { k: 'v' } }
          })
        );

        expect(window.open).toHaveBeenCalledWith(
          'https://track.lemonpi.io/events?e=' +
            encodeURIComponent(
              JSON.stringify({
                type: 'click',
                schema: 'adset-creative',
                adsetId: 1,
                creativeId: 2,
                out: encodeURIComponent('http://lemonpi.io?a=b&k=v'),
                tracker: encodeURIComponent(
                  'http://my.tracker.com?out=(:landing_url:)'
                )
              })
            ),
          '_blank'
        );
      });
    });
  });

  describe('interaction/impression', () => {
    let urls: any[];

    beforeEach(() => {
      urls = [];

      jest.spyOn(document, 'createElement').mockImplementation((tag) => {
        const handler = {
          set(target: any, prop: any, value: any) {
            if (tag === 'img' && prop === 'src') {
              urls.push(value);
            }
            target[prop] = value;
            return true;
          }
        };

        return new Proxy({}, handler);
      });
    });

    afterEach(() => {
      (document.createElement as any as jest.SpyInstance<any>).mockRestore();
    });

    test('tracks lemonpi impressions when given a valid v1 config', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );
      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));
      expect(urls.length).toEqual(1);
      expect(urls[0]).toEqual(
        'https://track.lemonpi.io/events?e=%7B%22type%22%3A%22impression%22%2C%22schema%22%3A%22adset-creative%22%2C%22adsetId%22%3A1%2C%22creativeId%22%3A1%7D'
      );
    });

    test('tracks lemonpi impressions when given a valid v2 config', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        version: 2,
        impressionId: 'uuid',
        advertiserId: 1,
        creativeRevisionId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));

      expect(urls.length).toEqual(1);
      expect(urls[0]).toEqual(
        'https://track.lemonpi.io/events?e=%7B%22version%22%3A2%2C%22type%22%3A%22impression%22%2C%22schema%22%3A%22adset-creative%22%2C%22impression-id%22%3A%22uuid%22%2C%22adset-id%22%3A1%2C%22creative-id%22%3A1%2C%22advertiser-id%22%3A1%2C%22creative-revision-id%22%3A1%7D'
      );
    });

    test('does not track lemonpi impressions when given a invalid config', () => {
      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));
      expect(urls.length).toEqual(0);
    });

    test('tracks 3rd party impressions', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };

      const content = {
        impressionTracker1: {
          type: 'impression-tracker',
          value: 'http://www.example.io/landing'
        },
        impressionTracker2: {
          type: 'impression-tracker',
          value: 'http://www.example2.io/landing'
        },
        text1: {
          type: 'text',
          value: 'test'
        }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', { detail: { content } })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));

      expect(urls.length).toBe(3);
    });

    test('content is not added on impression with content from creative', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        version: 2,
        impressionId: 'uuid',
        advertiserId: 1,
        creativeRevisionId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };
      const content = {
        source: 'export',
        content: { test: 'test' }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', { detail: content })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));

      expect(urls.length).toEqual(1);
      expect(urls[0]).toEqual(
        'https://track.lemonpi.io/events?e=%7B%22content%22%3A%7B%22source%22%3A%22creative%22%7D%2C%22version%22%3A2%2C%22type%22%3A%22impression%22%2C%22schema%22%3A%22adset-creative%22%2C%22impression-id%22%3A%22uuid%22%2C%22adset-id%22%3A1%2C%22creative-id%22%3A1%2C%22advertiser-id%22%3A1%2C%22creative-revision-id%22%3A1%7D'
      );
    });

    test('content is not added on impression with content from s3', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        version: 2,
        impressionId: 'uuid',
        advertiserId: 1,
        creativeRevisionId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };
      const content = {
        source: 'default',
        content: { test: 'test' }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', { detail: content })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));

      expect(urls.length).toEqual(1);
      expect(urls[0]).toEqual(
        'https://track.lemonpi.io/events?e=%7B%22content%22%3A%7B%22source%22%3A%22s3%22%7D%2C%22version%22%3A2%2C%22type%22%3A%22impression%22%2C%22schema%22%3A%22adset-creative%22%2C%22impression-id%22%3A%22uuid%22%2C%22adset-id%22%3A1%2C%22creative-id%22%3A1%2C%22advertiser-id%22%3A1%2C%22creative-revision-id%22%3A1%7D'
      );
    });

    test('content is not added on impression with content from lemonpi', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        version: 2,
        impressionId: 'uuid',
        advertiserId: 1,
        creativeRevisionId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };
      const content = {
        source: 'dynamic',
        content: { test: 'test' }
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', { detail: content })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));

      expect(urls.length).toEqual(1);
      expect(urls[0]).toEqual(
        'https://track.lemonpi.io/events?e=%7B%22content%22%3A%7B%22source%22%3A%22lemonpi%22%7D%2C%22version%22%3A2%2C%22type%22%3A%22impression%22%2C%22schema%22%3A%22adset-creative%22%2C%22impression-id%22%3A%22uuid%22%2C%22adset-id%22%3A1%2C%22creative-id%22%3A1%2C%22advertiser-id%22%3A1%2C%22creative-revision-id%22%3A1%7D'
      );
    });

    test('context without gdpr consent is not present so not passed to url', () => {
      const config = {
        adsetId: 1,
        creativeId: 1,
        version: 2,
        impressionId: 'uuid',
        advertiserId: 1,
        creativeRevisionId: 1,
        interactionHandlerUrl:
          'https://track.lemonpi.io/events?e=LEMONPI%25EVENTDATA'
      };
      const content = {
        source: 'dynamic',
        content: { test: 'test' }
      };

      const context = {
        $request: {}
      };

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', { detail: content })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', { detail: config })
      );
      window.dispatchEvent(
        new CustomEvent('lemonpi.context/ready', { detail: context })
      );

      window.dispatchEvent(new CustomEvent('lemonpi.interaction/impression'));

      expect(urls.length).toEqual(1);
      expect(urls[0]).toEqual(
        'https://track.lemonpi.io/events?e=%7B%22content%22%3A%7B%22source%22%3A%22lemonpi%22%7D%2C%22version%22%3A2%2C%22type%22%3A%22impression%22%2C%22schema%22%3A%22adset-creative%22%2C%22impression-id%22%3A%22uuid%22%2C%22adset-id%22%3A1%2C%22creative-id%22%3A1%2C%22advertiser-id%22%3A1%2C%22creative-revision-id%22%3A1%7D'
      );
    });
  });
});

describe('getDataAtrributesClickTag', () => {
  test('no map should be null', () => {
    expect(getDataAtrributesClickTag(null)).toBeNull();
  });

  test('empty map should be null', () => {
    expect(getDataAtrributesClickTag({})).toBeNull();
  });

  test('empty clickTag should be null', () => {
    expect(getDataAtrributesClickTag({ clickTag: '' })).toBeNull();
    expect(getDataAtrributesClickTag({ clickTag: null })).toBeNull();
  });

  test('unmaterialized DSP macro should be null', () => {
    expect(getDataAtrributesClickTag({ clickTag: '${CLICK_URL}' })).toBeNull();
  });

  test('non URLs should return null', () => {
    expect(getDataAtrributesClickTag('file://a.url' as any)).toBeNull();
  });

  test('Redirect URL should be returned', () => {
    const url = 'http://dsp.redirect.me/to?url=';
    expect(getDataAtrributesClickTag({ clickTag: url })).toBe(url);
  });
});

describe('Tradedesk clickout handling', () => {
  const originalOpen = window.open;
  const { location } = window;

  beforeAll(() => {
    delete window.location;
    window.location = {
      href: '',
      search: ''
    } as any;
  });
  beforeEach(() => {
    jest.resetModules();
    window.open = jest.fn();
    jest.spyOn(window, 'open');
    window.lemonpiStore = {
      getIsDebugEnabled: jest.fn(),
      getUuidAndCount: jest.fn(),
      setDebugEnable: jest.fn()
    };
    require('../interaction-handler');
  });

  afterEach(() => {
    window.open = originalOpen;
  });

  afterAll(() => {
    window.location = location;
  });
  describe('prepends google click tracker', () => {
    beforeEach(() => {
      window.dispatchEvent(
        new CustomEvent('lemonpi.config/ready', {
          detail: {
            adserver: { code: 'google-cm', extraQuery: true },
            advertiserId: 97,
            adsetId: 27988,
            interactionHandlerUrl: interactionHandlerUrlWithEventPlaceholder,
            impressionId: 'id',
            creativeId: 1,
            version: 2
          }
        })
      );

      window.dispatchEvent(
        new CustomEvent('lemonpi.content/fetched', {
          detail: {
            content: {
              click1: {
                type: 'click',
                value:
                  'https://www.dell.com/en-us/shop/cty/pdp/spd/latitude-15-5540-laptop/s105l5540usvp_ac?tfcid=123'
              }
            }
          }
        })
      );
    });

    afterEach(() => {
      window.clickTag1 = undefined;
    });
    test('clickout with clickTag', () => {
      window.clickTag1 =
        'http://www.dcm.com/track?r=LEMONPI%CLICKURL%3Fdclid%3D%25edclid!%26gacd%3D123%26dgc%3D1';

      const clickTagQueries = window.clickTag1.split(encodeURIComponent('?'));
      const addedQuery = clickTagQueries[clickTagQueries.length - 1];
      const decodedAddedQuery = decodeURIComponent(addedQuery);

      const clickWithAddedQuery =
        'https://track.lemonpi.io/events?e=' +
        encodeURIComponent(
          JSON.stringify({
            version: 2,
            type: 'click',
            schema: 'adset-creative',
            'impression-id': 'id',
            'adset-id': 27988,
            'creative-id': 1,
            out: encodeURIComponent(
              'https://www.dell.com/en-us/shop/cty/pdp/spd/latitude-15-5540-laptop/s105l5540usvp_ac?tfcid=123' +
                '&' +
                decodedAddedQuery
            )
          })
        );

      window.dispatchEvent(
        new CustomEvent('lemonpi.interaction/click', {
          detail: { placeholder: 'click1' }
        })
      );

      expect(window.open).toHaveBeenCalledWith(
        'http://www.dcm.com/track?r=' +
          clickWithAddedQuery +
          encodeURIComponent('?') +
          addedQuery,
        '_blank'
      );
    });
  });
});
