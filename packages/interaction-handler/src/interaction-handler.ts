import { UnknownObject } from '../../../types/utility.types';
import { dispatchCustomEvent } from '../../bundle/src/dispatch-event';
import { LemonpiEnrichedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import { tryCatchWrapper } from '../../_utils/src/tryCatchWrapper';

let config: any;
let content: any;
let source: any;

const noop = <T = unknown>(v: T) => v;

export const MACRO_EVENT = 'LEMONPI%EVENTDATA';
const MACRO_CLICK_URL = 'LEMONPI%CLICKURL';
const MACRO_DO_NOT_CHANGE_THIS = 'https://donotchangethis.lemonpi.io';

export function getDataAtrributesClickTag(attributes: UnknownObject) {
  const url = attributes && attributes['clickTag'];
  const tryUrl = (url: string) => {
    try {
      new URL(url);
      return url;
    } catch (_) {
      return null;
    }
  };

  return tryUrl(url);
}

function urlSearchParam(search: string, key: string) {
  const regex = new RegExp('[?&]' + key + '(=([^&#]*)|&|#|$)');
  const results = regex.exec(search);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * Retrieve the click tag from the clickTag URL parameter.
 *
 * Depending on the used ad server, it might be a complete redirect URL
 * (containing the `MACRO_DO_NOT_CHANGE_THIS URL`) or a prefix to which the real
 * redirect URL should be appended.
 *
 * @access private
 * @return {string | null} click tag
 */
function getUrlParameterClickTag() {
  const name = 'clickTag';

  try {
    const url = new URL(window.location.href);

    const clickTag = urlSearchParam(url.search, name);

    // Check whether the clicktag is a valid url, throws an exception when the clickTag is not a valid URL
    new URL(clickTag);

    return clickTag;
  } catch (_) {
    return null;
  }
}

/**
 * Retrieve the Adform click tag.
 *
 * @access private
 * @return {string} Adform click tag
 */
function getAdformClickTag() {
  const name = 'clickTAG';
  return window.dhtml.getVar(name);
}

/**
 * Url encode a `input` `n` times.
 *
 * @access private
 * @param  {int} times number of encoding passes
 * @param  {string} input value to encode
 * @return {string} url encoded string
 */
function encodeTimes(n: number, input: string): string {
  if (n === 0) {
    return input;
  }

  return encodeTimes(n - 1, encodeURIComponent(input));
}

/**
 * Replace a `macro` with `value` in `string`. The value will be url encoded
 * as many times as the macro instance in the string is encoded.
 *
 * This does not replace all instances of the macro in the string, only the
 * first, where first is defined as the least encoded macro instance.
 *
 * @access private
 * @param {string} string Source in which to replace a macro
 * @param {string} macro Macro to replace with the value
 * @param {string} value Value to replace the macro with
 */
export function replaceMacro(string: string, macro: string, value: string) {
  let nrTimesToEncode = 0;
  let previouslyDecoded = string;

  try {
    while (previouslyDecoded.indexOf(macro) === -1) {
      const currentDecoded = decodeURIComponent(previouslyDecoded);
      if (currentDecoded === previouslyDecoded) {
        break;
      }

      nrTimesToEncode++;
      previouslyDecoded = currentDecoded;
    }
  } catch (e) {
    // cannot decode anymore
  }

  const encodedMacro = encodeTimes(nrTimesToEncode, macro);
  const encodedValue = encodeTimes(nrTimesToEncode, value);
  return string.replace(encodedMacro, encodedValue);
}

/**
 * Returns a function f that returns the interaction handler url
 * in `config`. This url has a request parameter representing a click event;
 * its value is a stringified json object.
 * Parameter `redirectURL` of f will be uri-encoded and then set as event.out.
 * If event.tracker exists, like event.out it is a uri-encdoded url.
 *
 * @access private
 * @param {module:lemonpi-config-provider~config} config LemonPI configuration.
 * @param {module:lemonpi-content~content} content LemonPI content object.
 * @return {function} Function f that returns the interaction handler url
 * in `config`. This url has a request parameter representing a click event;
 * its value is a stringified json object.
 * Parameter `redirectURL` of f will be uri-encoded and then set as event.out.
 * If event.tracker exists, like event.out it is a uri-encdoded url.
 */
function addLemonpiTracker(config: LemonpiEnrichedConfig, content: any) {
  return (redirectURL: string) => {
    if (
      config.adserver &&
      config.adserver.extraQuery === true &&
      config.adserver.code === 'google-cm'
    ) {
      const trackUrl = window.clickTag1;
      const encodedQuestionMark = encodeURIComponent('?');
      //Account for added in CM search params for TTD
      const trackUrlSearchParams = trackUrl.split(encodedQuestionMark);
      // Get the last query param added to the clickTag
      let addedQueryParams =
        trackUrlSearchParams[trackUrlSearchParams.length - 1] !== ''
          ? decodeURIComponent(
              trackUrlSearchParams[trackUrlSearchParams.length - 1]
            )
          : '';

      // handle any additional query params that might have been added to the clickTag
      if (addedQueryParams !== '') {
        if (addedQueryParams.includes('?')) {
          addedQueryParams = addedQueryParams.split('?')[1];
        }
        if (addedQueryParams.includes(encodedQuestionMark)) {
          const decodedAddedQueryParams =
            addedQueryParams.split(encodedQuestionMark)[1];
          addedQueryParams = decodeURIComponent(decodedAddedQueryParams);
        }
      }

      if (redirectURL.includes('?')) {
        if (redirectURL.endsWith('&')) {
          redirectURL = redirectURL + addedQueryParams;
        } else {
          redirectURL = redirectURL + '&' + addedQueryParams;
        }
      } else {
        redirectURL = redirectURL + '?' + addedQueryParams;
      }
    }
    const event: Record<string, any> =
      config.version == 2
        ? {
            version: 2,
            type: 'click',
            schema: 'adset-creative',
            'impression-id': config.impressionId,
            'adset-id': config.adsetId,
            'creative-id': config.creativeId,
            out: encodeURIComponent(redirectURL)
          }
        : {
            type: 'click',
            schema: 'adset-creative',
            adsetId: config.adsetId,
            creativeId: config.creativeId,
            out: encodeURIComponent(redirectURL)
          };

    const tracker = Object.values<any>(content).find(
      (v: any) => v.type === 'click-tracker'
    );

    if (tracker && tracker.value) {
      event.tracker = encodeURIComponent(tracker.value);
    }

    return replaceMacro(
      config.interactionHandlerUrl,
      MACRO_EVENT,
      JSON.stringify(event)
    );
  };
}

/**
 * Returns a function f that returns `trackUrl` with the redirect URL (from the
 * function parameter) appended.
 *
 * If `trackUrl` is empty, f returns the redirect URL as is.
 *
 * If `MACRO_DO_NOT_CHANGE_THIS` is included in `trackUrl`, it is replaced by
 * the redirect URL. Otherwise `parameter` and the redirect URL are appended to
 * it after encoding it `timesToEncode` times.
 *
 * @access private
 * @param {string} trackUrl the tracker URL used by f
 * @param {string} parameter the URL parameter to use for the appended
 * redirect URL, e.g. `&r=`; blank if the tracker URL already includes
 * @param {number} timesToEncode encode the redirect URL so many times
 * if appending it to the URL; otherwise encode it as much
 * as `MACRO_DO_NOT_CHANGE_THIS` is encoded in `trackUrl`
 */
function addRedirectToTrackerUrl(
  trackUrl: string,
  parameter: string,
  timesToEncode: number
) {
  if (!trackUrl) {
    return noop;
  }
  // recursively decode the url
  const revealUrl = (value: string): string => {
    const decoded = decodeURIComponent(value);
    return value === decoded ? value : revealUrl(decoded);
  };
  const decodedTrackUrl = revealUrl(trackUrl);
  const hasMacro = decodedTrackUrl.includes(MACRO_DO_NOT_CHANGE_THIS);
  return hasMacro
    ? (redirectURL: string) =>
        replaceMacro(trackUrl, MACRO_DO_NOT_CHANGE_THIS, redirectURL)
    : (redirectURL: string) =>
        `${trackUrl}${parameter}${encodeTimes(timesToEncode, redirectURL)}`;
}

/**
 * Returns a function f that returns an `adserver` specific click tag url.
 * This url has a request parameter specifying a redirect url.
 * Parameter `redirectURL` of f will be set as this request parameter.
 *
 * @access private
 * @param {module:lemonpi-config-provider~adserver} adserver adserver in which
 *  the creative is served
 * @return {function} Function f that returns an `adserver` specific click tag url.
 * This url has a request parameter specifying a redirect url.
 * Parameter `redirectURL` of f will be set as this request parameter.
 */
function addAdserverTracker(adserver: any) {
  if (adserver.code === 'dcm' || adserver.code === 'google-cm') {
    const trackUrl = window.clickTag1;

    if (!trackUrl) {
      return noop;
    }

    return (redirectURL: string) =>
      replaceMacro(trackUrl, MACRO_CLICK_URL, redirectURL);
  }

  if (adserver.code === 'appnexus') {
    const trackUrl = getUrlParameterClickTag();

    if (!trackUrl) {
      return noop;
    }

    return (redirectURL: string) =>
      replaceMacro(trackUrl, MACRO_DO_NOT_CHANGE_THIS, redirectURL);
  }

  if (adserver.code === 'trade-desk') {
    const trackUrl = getUrlParameterClickTag();

    return addRedirectToTrackerUrl(trackUrl, '', 1);
  }

  if (adserver.code === 'adform') {
    const trackUrl = getAdformClickTag();

    return addRedirectToTrackerUrl(trackUrl, ';cpdir=', 0);
  }
  if (adserver.code === 'lemonpi') {
    const lemonpiClickTag = config.lemonpiClickTag;

    if (lemonpiClickTag) {
      return addRedirectToTrackerUrl(lemonpiClickTag, '', 1);
    }

    const urlParameterClickTag = getUrlParameterClickTag();

    if (urlParameterClickTag) {
      return addRedirectToTrackerUrl(urlParameterClickTag, '', 1);
    }
  }

  return noop;
}

/**
 * Returns a function that adds `query` to `url` as query parameters.
 */
function addQueryParams(query: UnknownObject | null) {
  if (!query || typeof query !== 'object' || Object.keys(query).length === 0) {
    return noop;
  }

  return (url: string) => {
    const [path, qs = ''] = url.split('?');

    const parts = [
      ...qs.split('&'),
      ...Object.entries(query).map(([k, v]) => k + '=' + v)
    ].filter((str) => !!str);

    return path + '?' + parts.join('&');
  };
}

/**
 * When requesting the url that results from this function,
 * a chain of redirects might take place such that multiple tracking services
 * are invoked sequentially, eventually landing on `url`.
 *
 * Adds `query` as query parameters to `url`. Tracking might be added
 * by embedding `url` in a tracker url such that when the request to the
 * tracker url is handled, a redirect to `url` takes place.
 * This adding of tracking might happen multiple times, recursively.
 */
function buildLandingUrl(
  url: string,
  query: UnknownObject,
  config: LemonpiEnrichedConfig,
  content: any
) {
  const transformers = [addQueryParams(query)];

  if (
    config &&
    config.interactionHandlerUrl &&
    config.adsetId &&
    config.creativeId
  ) {
    transformers.push(addLemonpiTracker(config, content));
  }

  if (config && config.adserver) {
    transformers.push(addAdserverTracker(config.adserver));
  }

  return transformers.reduce((acc, tracker) => tracker(acc), url);
}

/**
 * Handle interaction/click `event`.
 * @param {CustomEvent} event
 */
function handleClick(event: CustomEvent) {
  const phSelector = !!event.detail && event.detail.placeholder;
  if (!phSelector) {
    return;
  }

  const phValue = (() => {
    const [phName, ...phPath] = !Array.isArray(phSelector)
      ? [phSelector]
      : phSelector;

    const phContent =
      content[phName as any] && phPath.length
        ? phPath.reduce((acc, prop) => acc && acc[prop], content[phName].value)
        : content[phName];

    return phContent && phContent.value;
  })();

  const url = buildLandingUrl(phValue, event.detail.query, config, content);

  dispatchCustomEvent('lemonpi.url/open', {
    elementId: event.detail.elementId,
    url: url,
    query: event.detail.query
  });

  window.open(url, '_blank');
}

function normalizeContent() {
  switch (source) {
    case 'export':
      return { source: 'creative' };
    case 'default':
      return { source: 's3' };
    case 'dynamic':
      return { source: 'lemonpi' };
  }
}

/**
 * Handle interaction/impression `event`
 */
function handleImpression() {
  const urls = [];
  if (
    config &&
    config.version == 2 &&
    config.interactionHandlerUrl &&
    config.impressionId &&
    config.advertiserId &&
    config.creativeRevisionId &&
    config.adsetId
  ) {
    const normalizedContent = normalizeContent();
    const event = {
      content: normalizedContent,
      version: 2,
      type: 'impression',
      schema: 'adset-creative',
      'impression-id': config.impressionId,
      'adset-id': config.adsetId,
      'creative-id': config.creativeId,
      'advertiser-id': config.advertiserId,
      'creative-revision-id': config.creativeRevisionId
    };

    urls.push(
      replaceMacro(
        config.interactionHandlerUrl,
        MACRO_EVENT,
        JSON.stringify(event)
      )
    );
  } else if (
    config &&
    config.interactionHandlerUrl &&
    config.adsetId &&
    config.creativeId
  ) {
    const event = {
      type: 'impression',
      schema: 'adset-creative',
      adsetId: config.adsetId,
      creativeId: config.creativeId
    };

    urls.push(
      replaceMacro(
        config.interactionHandlerUrl,
        MACRO_EVENT,
        JSON.stringify(event)
      )
    );
  }

  if (content) {
    for (const [, assignment] of Object.entries<UnknownObject>(content)) {
      if (assignment.type === 'impression-tracker') {
        urls.push(assignment.value);
      }
    }
  }

  urls.forEach((url) => {
    const tag = document.createElement('img');
    tag.src = url;
  });
}

window.addEventListener('lemonpi.config/ready', (event: CustomEvent) => {
  config = event.detail;
});

window.addEventListener('lemonpi.content/fetched', (event: CustomEvent) => {
  source = event.detail.source;
  content = event.detail.content;
});

const wrappedHandleClick = tryCatchWrapper(handleClick);
window.addEventListener('lemonpi.interaction/click', wrappedHandleClick);

const wrappedHandleImpression = tryCatchWrapper(handleImpression);
window.addEventListener(
  'lemonpi.interaction/impression',
  wrappedHandleImpression
);
