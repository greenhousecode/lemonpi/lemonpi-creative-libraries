import { LEMONPI_EVENT, lemonpiEvent } from '../../bundle/src/debug-events';

let sessionClient: OmidSessionClient | null = null;
let adContext: Omid.Context | null = null;
let adSession: Omid.AdSession | null = null;

const CreativeTypes = {
  DEFINED_BY_JAVASCRIPT: 'definedByJavaScript',
  HTML_DISPLAY: 'htmlDisplay',
  NATIVE_DISPLAY: 'nativeDisplay',
  VIDEO: 'video',
  AUDIO: 'audio'
} as const;

const InteractionTypes = {
  DEFINDED_BY_JAVASCRIPT: 'definedByJavaScript',
  UNSPECIFIED: 'unspecified',
  LOADED: 'loaded',
  BEGIN_TO_RENDER: 'beginToRender',
  ONE_PIXEL: 'onePixel',
  VIEWABLE: 'viewable',
  AUDIBLE: 'audible',
  OTHER: 'other'
} as const;

// TODO - Get the correct partner name and version after registering with IAB
const PARTNER_NAME = 'Choreograph_Create';
const PARTNER_VERSION = '1.0';

export function isOmidSupported(): boolean {
  try {
    if (sessionClient) return true;
    if (window?.OmidSessionClient && window?.OmidSessionClient['default']) {
      sessionClient = window.OmidSessionClient['default'];
      lemonpiEvent(LEMONPI_EVENT.OMSKD_READY, {
        lemonpiUuid: window.lemonpiStore.getUuidAndCount().lemonpiUuid
      });
      return true;
    }
    return false;
  } catch (_) {
    return false;
  }
}

export function handleOmidSessionStart(): void {
  if (!sessionClient) return;
  const AdSession = sessionClient.AdSession;
  const Partner = sessionClient.Partner;
  const Context = sessionClient.Context;

  const partner = new Partner(PARTNER_NAME, PARTNER_VERSION);

  adContext = new Context(partner, null, null, null);
  adContext.setSlotElement(document.getElementsByTagName('body')[0]);

  adSession = new AdSession(adContext);
  adSession.setCreativeType(CreativeTypes.HTML_DISPLAY);

  //Prepare the services and verification scripts for event tracking
  adSession.setImpressionType(InteractionTypes.BEGIN_TO_RENDER);

  if (!adSession.isSupported()) {
    lemonpiEvent(LEMONPI_EVENT.ERROR_EVENT, {
      error: 'OMID session is not supported'
    });
    return;
  }

  adSession.registerSessionObserver((event) => {
    if (event.type === 'sessionError') {
      lemonpiEvent(LEMONPI_EVENT.ERROR_EVENT, {
        error: event
      });
    }
  });
  adSession.start();
}

export function handleOmidImpressionEvent(source: any, content: any) {
  dispatchCustomEvent('lemonpi.content/fetched', { source, content });
  if (adSession) {
    adSession.impressionOccurred();
    lemonpiEvent(LEMONPI_EVENT.OMSKD_IMPRESSION, {
      lemonpiUuid: window.lemonpiStore.getUuidAndCount().lemonpiUuid
    });
    dispatchCustomEvent('lemonpi.interaction/impression', undefined);
  }
}

export function finishOmidSession() {
  if (adSession) {
    adSession.finish();
  }
}
