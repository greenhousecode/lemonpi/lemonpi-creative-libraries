export function loadOmScripts(
  env: string,
  appendTarget: HTMLElement | undefined = document.body
) {
  const baseUrl =
    env === 'test'
      ? 'https://creative-libraries-test.lemonpi.io/v1/'
      : 'https://creative-libraries.lemonpi.io/v1/';

  const omWebUrl = baseUrl + 'omweb-v1.js';
  const omidSessionClientUrl = baseUrl + 'omid-session-client-v1.js';
  try {
    [omWebUrl, omidSessionClientUrl].forEach((url) => {
      const script = document.createElement('script');
      script.src = url;
      appendTarget.appendChild(script);
    });
  } catch (e) {
    // do nothing
  }
}
