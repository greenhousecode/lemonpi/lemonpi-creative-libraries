const {
  getMobileAppIdFromUrl,
  getPublisherData,
  findDspFromUrl,
  findEnvFromUA,
  captureNativeData
} = require('../capture-native-data');
const { default: nativeDataStore } = require('../native-data-store');

const urlAdform =
  'https://example.com/somepath/com.another.android.app/details?ADFbanPlacID=1234567890';

const urlXandr =
  'https://example.com/somepath/com.another.android.app/details?CPG_ID=test-X-1';

const urlGoogle =
  'https://example.com/somepath/com.another.android.app/details?%eaid!=1234567890';

const urlTradeDesk =
  'https://example.com/somepath/com.another.android.app/details?tdid=%%TDID%%&td_s=%%TD_SESS_ID%%&td_c=%%TD_CAMPAIGN_ID%%&td_a=%%TD_AD_ID%%&td_p=%%TD_PLACEMENT_ID%%&td_aid=%%TD_CREATIVE_ID%%&td_pid=%%TD_PUBLISHER_ID%%&td_sid=%%TD_SITE_ID%%&td_iid=%%TD_INSERTION';

const urlWithAndoridIdInQuery =
  'https://play.google.com/store/apps/details?app-id-mobile-id-ad=com.google.android.apps.maps&utm_campaign=email';

const urlWithAndoridIdInPathName =
  'https://example.com/somepath/com.another.android.app/details?mobile=com.another.android.app';

const urlWithAppleIdInQuery =
  'https://apps.apple.com/us/app/apple-store/id375380948?mt=8&id=id123123123123';

const urlWithQueryParams =
  'https://example.com/somepath/com.another.android.app/details?utm_campaign=com.another.android.app';

describe('getMobileAppIdFromUrl', () => {
  test('can get androind mobile app id from url', () => {
    expect(getMobileAppIdFromUrl(urlWithAndoridIdInQuery)).toBe(
      'com.google.android.apps.maps'
    );
    expect(getMobileAppIdFromUrl(urlWithAndoridIdInPathName)).toBe(
      'com.another.android.app'
    );
  });

  test('can get ios mobile app id from url', () => {
    expect(getMobileAppIdFromUrl(urlWithAppleIdInQuery)).toBe('id375380948');
  });
});

describe('getPublisherData', () => {
  delete global.window.location;
  window.location = new URL(urlWithQueryParams);
  const publisherData = getPublisherData();
  test('can get publisher data from url', () => {
    expect(publisherData).toEqual({
      'source-url':
        'https://example.com/somepath/com.another.android.app/details?utm_campaign=com.another.android.app'
    });
  });
});
describe('findDspFromUrl', () => {
  window.location = new URL(urlAdform);
  const publisherDataAdform = findDspFromUrl();
  test('can find adserver data from url - adform', () => {
    expect(publisherDataAdform).toEqual('adform');
  });

  window.location = new URL(urlXandr);
  const publisherDataXandr = findDspFromUrl();
  test('can find adserver data from url - xandr', () => {
    expect(publisherDataXandr).toEqual('xandr');
  });

  window.location = new URL(urlGoogle);
  window.parent.window.googletag = {
    pubadsReady: true
  };
  const publisherDataGoogle = findDspFromUrl();
  test('can find adserver data from url - google-cm', () => {
    expect(publisherDataGoogle).toEqual('google-cm | dcm');
  });

  window.location = new URL(urlTradeDesk);
  const publisherDataTradeDesk = findDspFromUrl();
  test('can find adserver data from url - trade-desk', () => {
    expect(publisherDataTradeDesk).toEqual('trade-desk');
  });
});

describe('findEnvFromUA', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('can find environment from fbAndroidUa', () => {
    const fbAndroidUa =
      'Mozilla/5.0 (Linux; Android 4.4.4; One Build/KTU84L.H4) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/28.0.0.20.16;]';
    const { mobileMatched, uaEnv } = findEnvFromUA(fbAndroidUa);
    expect(uaEnv).toBe('mobile-app');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['Android'])
    );
  });
  test('can find environment from twitterAndroindUA', () => {
    const twitterAndroindUA =
      'Mozilla/5.0 (Linux; Android 4.4.4; One Build/KTU84L.H4) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36';
    const { mobileMatched, uaEnv } = findEnvFromUA(twitterAndroindUA);
    expect(uaEnv).toBe('mobile-app');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['Android'])
    );
  });
  test('can find environment from chromeAndroindUa', () => {
    const chromeAndroindUa =
      'Mozilla/5.0 (Linux; Android 4.4.4; One Build/KTU84L.H4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.135 Mobile Safari/537.36';
    const { mobileMatched, uaEnv } = findEnvFromUA(chromeAndroindUa);
    expect(uaEnv).toBe('mobile-web');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['Android'])
    );
  });
  test('can find environment from fbIosUa', () => {
    const fbIosUa =
      'Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12D508 [FBAN/FBIOS;FBAV/27.0.0.10.12;FBBV/8291884;FBDV/iPhone7,1;FBMD/iPhone;FBSN/iPhone OS;FBSV/8.2;FBSS/3; FBCR/vodafoneIE;FBID/phone;FBLC/en_US;FBOP/5]';
    const { mobileMatched, uaEnv } = findEnvFromUA(fbIosUa);
    expect(uaEnv).toBe('mobile-app');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['iPhone'])
    );
  });
  test('can find environment from twitterIosUa', () => {
    const twitterIosUa =
      'Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12D508';
    const { mobileMatched, uaEnv } = findEnvFromUA(twitterIosUa);
    expect(uaEnv).toBe('mobile-app');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['iPhone'])
    );
  });
  test('can find environment from twitBotrIosUa', () => {
    const twitBotIosUa =
      'Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12D508';
    const { mobileMatched, uaEnv } = findEnvFromUA(twitBotIosUa);
    expect(uaEnv).toBe('mobile-app');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['iPhone'])
    );
  });
  test('can find environment from safariIosUa', () => {
    const safariIosUa =
      'Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12D508';
    const { mobileMatched, uaEnv } = findEnvFromUA(safariIosUa);
    expect(uaEnv).toBe('mobile-app');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['iPhone'])
    );
  });
  test('can find environment from WhatsappIosUa', () => {
    const WhatsappIosUa =
      'Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12D508 Safari/600.1.4';
    const { mobileMatched, uaEnv } = findEnvFromUA(WhatsappIosUa);
    expect(uaEnv).toBe('mobile-web');
    expect(JSON.stringify(mobileMatched)).toStrictEqual(
      JSON.stringify(['iPhone'])
    );
  });
});

describe('captureNativeData', () => {
  beforeEach(() => {
    Object.defineProperty(navigator, 'userAgent', {
      value:
        'Mozilla/5.0 (Linux; Android 10; 123123123) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36',
      writable: true
    });
    navigator.userAgent =
      'Mozilla/5.0 (Linux; Android 10; 123123123) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36';

    delete window.location;
    window.location = new URL(urlWithAndoridIdInQuery);
    window.googletag = {
      pubadsReady: true
    };
  });

  test('can capture native data', () => {
    captureNativeData(
      {
        adserver: {
          code: 'google-cm'
        }
      },
      false
    );
    const data = nativeDataStore.getData();
    expect(data).toEqual(
      expect.objectContaining({
        adserver: 'google-cm',
        dsp: 'google-cm | dcm',
        environment: 'mobile-web',
        'app-platform': 'Android',
        'source-url':
          'https://play.google.com/store/apps/details?app-id-mobile-id-ad=com.google.android.apps.maps&utm_campaign=email',
        'encoded-user-agent':
          '"Mozilla/5.0 (Linux; Android 10; 123123123) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36"'
      })
    );
  });

  test('can capture native data', () => {
    captureNativeData({}, false);
    const data = nativeDataStore.getData();
    expect(data).toEqual(
      expect.objectContaining({
        adserver: null,
        dsp: 'google-cm | dcm',
        environment: 'mobile-web',
        'app-platform': 'Android',
        'source-url':
          'https://play.google.com/store/apps/details?app-id-mobile-id-ad=com.google.android.apps.maps&utm_campaign=email'
      })
    );
  });
});
