import { DataAttributesConfig } from '../../bundle/src/display-tag';
import { NormalizedConfig } from '../../config-provider/src/lemonpi-config-provider.types';
import NativeDataStore from './native-data-store';

export const environment = {
  MOBILE_APP: 'mobile-app',
  MOBILE_WEB: 'mobile-web',
  WEB: 'web'
} as const;

type EnvironmentValues = typeof environment[keyof typeof environment];

const rules = [
  'WebView',
  // IOS but without Safari
  '(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)',
  // Android with Version
  'Android.*Version/[0-9].[0-9]',
  // Android with Chrome with Mobile and version
  'Version/[d.]+.*Chrome/[d.]+ Mobile',
  // Android with wv
  'Android.*wv',
  // Old Android
  'Linux; U; Android'
];

const webViewRegex = new RegExp(`(${rules.join('|')})`, 'ig');

//mobileRegex - to match app platform
const mobileRegex =
  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Silk|Playbook|Mobi|Windows Phone/i;
// AppleID - `id` followed by 9 digits
const iosAppIdRegex = /id\d{9}/;

// Select domain like pattern with words separated by dots or matching id and ending wtih &
const androidAppIdRegex = /((?!.*:\/\/)(\w+\.)+(\w+)|(id\d+))(?=(?:&|$))/i;

let appId: string | null = null;

export const getMobileAppIdFromUrl = (url: string): string | null => {
  try {
    const urlObject = new URL(url);
    const urlPathAndSearch = urlObject.pathname + urlObject.search;

    const iosAppIdMatch = urlPathAndSearch.match(iosAppIdRegex);
    if (iosAppIdMatch) {
      return iosAppIdMatch[0];
    }
    const androidAppIdMatch = urlPathAndSearch.match(androidAppIdRegex);
    if (androidAppIdMatch) {
      return androidAppIdMatch[0];
    }
    return null;
  } catch (e) {
    return null;
  }
};

export function getTopFrame(withAppIdSearch = false) {
  let currentWindow = window;

  // Keep climbing up the parent frames until we reach the top level
  do {
    if (withAppIdSearch) {
      const potentialAppId = getMobileAppIdFromUrl(currentWindow.location.href);
      if (potentialAppId && !appId) {
        appId = potentialAppId;
      }
    }
    // If we are at the top level or no parent, break out of the loop
    if (currentWindow === window.top || !currentWindow.parent) {
      break;
    }
    currentWindow = currentWindow.parent as Window & typeof globalThis;
  } while (currentWindow !== window.top);
  // Now, currentWindow is the top-level window
  const topFrame = currentWindow;

  return topFrame;
}

// Function to extract query string parameters from the URL
export function getPublisherData(isMobile = false) {
  try {
    const topFrame = getTopFrame(isMobile);
    return {
      'source-url': topFrame?.location?.href as string
    };
  } catch (e) {
    // Accessing top frame loacation might be blocked by the browser
    return {
      'source-url': null as null,
      'referrer-url': document.referrer || null
    };
  }
}

const AdserversIdWithMacros = [
  {
    adserver: 'adform',
    macro: 'ADFbanPlacID'
  },
  {
    adserver: 'xandr',
    macro: 'CPG_ID'
  },
  {
    adserver: 'trade-desk',
    macro: 'tdid'
  }
];

export function findDspFromUrl(): string | null {
  try {
    const urlObject = new URL(window.location.href);
    const searchParams = new URLSearchParams(urlObject.search);
    for (const adserver of AdserversIdWithMacros) {
      const macroValue = searchParams.get(adserver.macro);
      if (macroValue) {
        return adserver.adserver;
      }
    }
    if (window.googletag && window.googletag.pubadsReady) {
      return 'google-cm | dcm';
    }
    if (
      !window.parent.top &&
      window.parent.window.googletag &&
      window.parent.window.googletag.pubadsReady
    ) {
      return 'google-cm | dcm';
    }
    if (window.APPNEXUS) {
      return 'xandr';
    }
    if (window.dhtml) {
      return 'adform';
    }

    return null;
  } catch (e) {
    return null;
  }
}

type MobileAgentReturnType = {
  uaEnv: EnvironmentValues | null;
  mobileMatched?: string[];
};

export function findEnvFromUA(userAgent: string): MobileAgentReturnType {
  try {
    // User Agent Regex to capture possible mobile webview user agents
    const webViewMatched = userAgent.match(webViewRegex);

    if (webViewMatched) {
      const mobileMatched = webViewMatched[0].match(mobileRegex);
      return {
        uaEnv: environment.MOBILE_APP,
        mobileMatched: mobileMatched
      };
    }
    // User Agent Regex to capture possible mobile web user agents
    const mobileMatched = userAgent.match(mobileRegex);
    if (mobileMatched) {
      return { uaEnv: environment.MOBILE_WEB, mobileMatched };
    }
    return { uaEnv: environment.WEB };
  } catch (e) {
    return { uaEnv: null };
  }
}

export type CapturedNativeData = ReturnType<typeof captureNativeData>;

export function captureNativeData(
  config: NormalizedConfig | DataAttributesConfig
) {
  const configAdserver = config?.adserver?.code || null;
  try {
    if (typeof window === 'undefined' && typeof navigator === 'undefined') {
      return {};
    }
    const mobileAgent = navigator && navigator.userAgent;
    const rawMobileAgent = JSON.stringify(mobileAgent);
    const { uaEnv, mobileMatched } = findEnvFromUA(mobileAgent);
    const dspFromUrl = findDspFromUrl();

    switch (uaEnv) {
      case environment.MOBILE_APP: {
        return NativeDataStore.dispatchPartialData({
          ...getPublisherData(true),
          'app-platform': mobileMatched.join(' , '),
          'app-bundle-id': appId,
          environment: uaEnv,
          dsp: dspFromUrl,
          adserver: configAdserver,
          'encoded-user-agent': rawMobileAgent
        });
      }
      case environment.MOBILE_WEB: {
        return NativeDataStore.dispatchPartialData({
          ...getPublisherData(true),
          'app-platform': mobileMatched.join(' , '),
          environment: uaEnv,
          dsp: dspFromUrl,
          adserver: configAdserver,
          'encoded-user-agent': rawMobileAgent
        });
      }
      case environment.WEB: {
        return NativeDataStore.dispatchPartialData({
          ...getPublisherData(),
          environment: environment.WEB,
          dsp: dspFromUrl,
          adserver: configAdserver,
          'encoded-user-agent': rawMobileAgent
        });
      }
      default:
        break;
    }
  } catch (e) {
    return NativeDataStore.dispatchPartialData({
      adserver: configAdserver
    });
  }
}
