class NativeDataStore {
  private nativeData: Record<string, any> = {};

  dispatchPartialData(dataObject: Record<string, any>) {
    this.nativeData = { ...this.nativeData, ...dataObject };
  }

  getData() {
    return this.nativeData;
  }
}

const nativeDataStore = new NativeDataStore();

export default nativeDataStore;
