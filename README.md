# lemonpi-creative-libraries

This project contains the source code for libraries that can be used in LemonPI creative
templates.

## Quickstart

Create a `index.html` file and load the creative-libraries script in the `body`.

```html
<html>
  <head>
  </head>
  <body>
    <script src="https://creative-libraries.lemonpi.io/v1/lemonpi.js"></script>
  </body>
</html>
```

Add the `ad.size` meta tag describing your creative's dimensions to the `head` of the `index.html` file

```html
<html>
  <head>
    <meta name="ad.size" content="width=300,height=600"></meta>
  </head>
  <body>
    <script src="https://creative-libraries.lemonpi.io/v1/lemonpi.js"></script>
  </body>
</html>
```


Add `data-placeholder` attributes to the elements that should contain dynamic content.


```html
<html>
  <head>
    <meta name="ad.size" content="width=300,height=600"></meta>
  </head>
  <body>
    <div data-placeholder="landing_click">
      <div data-placeholder="title"></div>
      <div data-placeholder="product_image"></div>
    </div>
    <script src="https://creative-libraries.lemonpi.io/v1/lemonpi.js"></script>
  </body>
</html>
```

Create a `template.json` file describing the `type` of your placeholders. Possible types include `text`, `image` and `click`.

```json
{
  "placeholders": {
    "title": { "type": "text" },
    "product_image": { "type": "image" },
    "landing_click": { "type": "click" }
  }
}
```

Zip the `index.html`, `template.json` and any other assets you want to include. Note that the `index.html` and `template.json` files need to be in the root of the zip.

You should now be able to upload this zip as a template to LemonPI. Once this `template` is assigned to an `adset`, LemonPI will replace the placeholder elements with `content` defined in the adset.


## Overview

The creative-libraries are designed to be `event-driven`. [CustomEvents](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent) are dispatched using the [window.dispatchEvent](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/dispatchEvent) method. You can listen to these events using the [window.addEventListener](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener) method.

The creative-libraries' responsibilities are:

- Collect config
- Collect context
- Resolve content
- Render content
- Handle interactions

## Collect config

Upon receiving the `lemonpi/start` event, the `config` will be collected. this config contains values such as timeout durations and identifiers. As a consumer of the creative-libraries, you can not currently modify the config. Upon completion, the `lemonpi.config/ready` event is dispatched

### Collect context

Upon receiving the `lemonpi.config/ready` event, `context` will be collected. Context is the data upon which you can decide what `content` to display, e.g adserver macros. When a piece of context is collected, a `lemonpi.context/partial` event is dispatched. When all context has been collected, or after a timeout, the `lemonpi.context/ready` event is dispatched.

### Resolve content

Upon receiving the `lemonpi.context/ready` event, the creative-libraries will request `content` from the LemonPI backend. If we fail to fetch this content, we will use increasingly reliable yet less dynamic alternatives. Upon completion the `lemonpi.content/fetched` event is dispatched. The creative-libraries might do some preloading of the assets contained in the `content`, after which the `lemonpi.content/ready` event will be dispatched.

### Render content

Upon receiving the `lemonpi.content/ready` event, the creative-libraries will find all elements with `data-placeholder` attributes and render the corresponding content to these elements. The rendering behavior depends on the `type` of placeholder. Upon completion, the `lemonpi.content/rendered` event is dispatched.

#### Text

The [textContent](https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent) of the target element is replaced.

#### Image

Two classes are added to the target element: `.placeholder-{name}` and `.placeholder-image`. A
stylesheet is added to the start of the `<head>` tag, setting the desired image as the element's `background-image`.


#### Click

A `.placeholder-click` class and a `onclick` handler are added to the target element. Once clicked, a `lemonpi.interaction/click` event will be dispatched.

### Handle interactions

The following events representing interactions are dispatched and handled

#### lemonpi.interaction/impression
A impression is measured in LemonPI and any dynamic impressions-trackers defined in the resolved content.

#### lemonpi.interaction/click
The landing url is build by composing the Adserver click-tracker, LemonPI click-tracker and any dynamic click-trackers defined in the resolved content.

## Advanced use-cases

### Programmatically accessing context

You can either add an event listener for the `lemonpi.context/ready` event, or use the higher level `lemonpi.context.subscribe` method. The advantage of using the `lemonpi.context.subscribe` method is that it doesn't matter when you use it. If the context is already collected upon subscribing, the callback will be called immediately. Otherwise, the callback will be called once context is collected.

```js
// Called when lemonpi.context/ready is dispatched. You need to setup the event
// listener before this happens.
window.addEventListener('lemonpi.context/ready', event => {
  const context = event.detail
  // Do stuff
})

// Called when lemonpi.context/ready is dispatched or has previously been
// dispatched. It doesn't matter when you setup this event listener.
window.lemonpi.context.subscribe(context => {
  // Do stuff
})
```

Note that it's good practice to check for the existence of a key in the context object before you attempt to access it.

```js
// Bad, `context.appnexus` might be undefined if we didn't manage to collect
// the appnexus context in a timely fashion
window.lemonpi.context.subscribe(context => {
  const advertiserId = context.appnexus.ADV_ID
})

// Good
window.lemonpi.context.subscribe(context => {
  if (context.appnexus) {
    const advertiserId = context.appnexus.ADV_ID
  }
})
```

### Programmatically accessing content

Similar to accessing `context`, you can either add an event listener for the `lemonpi.content/ready` event, or use the higher level `lemonpi.subscribe` method.

```js
// Called when lemonpi.content/ready is dispatched. You need to setup the event
// listener before this happens.
window.addEventListener('lemonpi.content/ready', event => {
  const content = event.detail.content
  const source = event.detail.source
  // Do stuff
})

// Called when lemonpi.content/ready is dispatched or has previously been
// dispatched. It doesn't matter when you setup this event listener.
window.lemonpi.subscribe((content, source) => {
  // Do stuff
})
```

### Programmatically dispatching click interactions

In some cases, the default `lemonpi.interaction/click` that is dispatched when a `click` placeholder element is clicked is insufficient. You might want to programmatically determine which click placeholder should be navigated to, or you might want to add additional query paremeters. In that case, you can add your own `onclick` handler to an element and dispatch your own `lemonpi.interaction/click` event.

```js
document.body.onclick = () =>
  window.dispatchEvent(
    new CustomEvent('lemonpi.interaction/click', {
      detail: {
        placeholder: 'click_2',
        query: { postcode: document.getElementById('postcode-input').value }
      }
    })
  );
```

#### Dispatching click interactions within collection placeholder

You might want to dispatch `lemonpi.interaction/click` with click placeholders nested in collection placeholders. You can do this by using an Array as selector path for the `placeholder` property within `detail`.
Example `detail`: `{ placeholder: ['my_collection', 0, 'click_placeholder'] }`.

It uses the following structure:

```ts
<Array<placeholderName, index, collectionItemPlaceholderName>>
```

```js
document.body.onclick = () =>
  window.dispatchEvent(
    new CustomEvent('lemonpi.interaction/click', {
      detail: {
        placeholder:  ['my_collection', 0, 'click_placeholder'],
        query: { postcode: document.getElementById('postcode-input').value }
      }
    }));
```

## Setup

### Running tests

``` shell
# for development
# when running into error "`fsevents` unavailable", best to install Facebook's watchman
$ yarn test:watch
```

## Test creative

Test version of `lemonpi.js` is deployed at https://creative-libraries-test.lemonpi.io/v1/lemonpi.js.
