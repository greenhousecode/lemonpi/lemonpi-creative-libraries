# Introduction

Thank you for considering to contribute to Lemonpi Creative Libraries. With your help we hope to increase the ease of creating creatives and make them as sophisticated as necessary for your work.

There are many ways you can contribute to LemonPi Creative Libraries, spreading knowledge on the technicalities of the creatives, improving the documentation, submitting bug reports and feature requests or writing code which can be incorporated into the Libraries.

Please, don't use the issue tracker for `general support questions`. The best way to reach us for support requests is support@lemonpi.io. If you have a `technical question` related to code, please file an `issue` with the `question` label.

# Ground Rules

LemonPi Creative-Libraries powers tens of millions of creative impressions, therefore any change must be reviewed by a LemonPi Developer.

1. Create issues for any changes and enhancements that you wish to make.
2. We'll do our best to attend your contribution as soon as possible.

# Your First Contribution

If you'd like to suggest a change to the Creative Libraries please file an issue with a `"request"` label or with a `"bug"` label.
See how to create an issue at https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html. Let us know if you need any help!

## Contribution steps

### Getting started

1. Read the README for a detailed overview of how the creative-libraries work.
2. Submit a GitLab issue explaining what you'd like to change. The purpose of the issue is to track the progress of your contribution. We will communicate with you via the issue. More info: https://docs.gitlab.com/ee/user/project/issues/
3. In order to be able to submit Merge Requests request access by clicking "Request access" next to the Project ID, under the Project title. You will be granted "Developer" role.

### Writing code

1. The creative-libraries are designed to be event-driven. Please write event-driven code, check the README 'Overview' section for more details on this.
2. Be careful when using ES6 features. Do not use Generator functions or async/await, as these are not polyfilled.
3. Write test for each contribution, when necessary. Read existing tests for context.
4. Run test with `yarn test:unit`. For running test in watch mode add the flag `--watch`.
5. Develop on your local feature branch and then push to the remote branch.
6. Submit a merge request with your remote feature branch, check the options to `"Squash commits"` and `"Delete source branch"`.
7. Once you receive 2 approvals from LemonPi Developers, `Merge` your feature branch into the master branch.
8. Congratulations! Your contribution is now part of the LemonPi Creative Libraries.

TIP. If a maintainer asks you to `"rebase"` your merge request, they're saying that a lot of code has changed, and that you need to update your branch so it's easier to merge. See an explanation of this @ https://www.freecodecamp.org/news/an-introduction-to-git-merge-and-rebase-what-they-are-and-how-to-use-them-131b863785f/
