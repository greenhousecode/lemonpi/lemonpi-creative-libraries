const { uglify } = require('rollup-plugin-uglify');

const baseConfig = require('./rollup.base');

const config = baseConfig.map((configItem) =>
  Object.assign(configItem, {
    plugins: [...configItem.plugins, uglify()]
  })
);

module.exports = config;
