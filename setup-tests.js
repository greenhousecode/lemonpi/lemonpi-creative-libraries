import 'jest-expect-message';
import { TextEncoder, TextDecoder } from 'util';
global.TextEncoder = TextEncoder;
global.TextDecoder = TextDecoder;
beforeEach(() => {
  jest.resetModules();

  const handlers = {};

  jest
    .spyOn(window, 'addEventListener')
    .mockImplementation((event, handler) => {
      handlers[event] = handlers[event]
        ? [...handlers[event], handler]
        : [handler];
    });

  jest
    .spyOn(window, 'postMessage')
    .mockImplementation((event, allowedDomains) => {
      handlers['message'] &&
        handlers['message'].forEach((handler) => handler({ data: event }));
    });

  jest.spyOn(window, 'dispatchEvent').mockImplementation((event) => {
    handlers[event.type] &&
      handlers[event.type].forEach((handler) => handler(event));
  });
});
