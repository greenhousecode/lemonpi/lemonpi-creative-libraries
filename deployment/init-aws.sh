#!/usr/bin/env sh

set -eu


NAME=$(echo "AWS_SECRET_ACCESS_KEY_${CI_ENVIRONMENT_NAME}" | tr [:lower:] [:upper:])
DATA=$(eval "echo \$$NAME")
export AWS_SECRET_ACCESS_KEY=$DATA

NAME=$(echo "AWS_ACCESS_KEY_ID_${CI_ENVIRONMENT_NAME}" | tr [:lower:] [:upper:])
DATA=$(eval "echo \$$NAME")
export AWS_ACCESS_KEY_ID=$DATA
