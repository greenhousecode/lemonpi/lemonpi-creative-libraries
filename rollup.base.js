const typescript = require('@rollup/plugin-typescript');
const babel = require('rollup-plugin-babel');
const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');

const tsconfig = {
  tsconfig: './tsconfig.json'
};

const baseConfig = [
  {
    input: './packages/bundle/src/bundle.ts',
    output: {
      format: 'umd',
      file: './dist/lemonpi.js',
      name: 'lemonpi',
      globals: {
        crypto: 'crypto'
      }
    },
    plugins: [
      resolve({
        preferBuiltins: true,
        browser: true
      }),
      commonjs(),
      typescript(tsconfig),
      babel({
        exclude: 'node_modules/**',
        presets: [['@babel/preset-env', { corejs: 3 }]]
      })
    ],
    external: ['core-js']
  },
  {
    input: './packages/bundle/src/bundle-debug.ts',
    output: {
      format: 'umd',
      file: './dist/lemonpi-debug.js',
      name: 'lemonpi',
      globals: {
        crypto: 'crypto'
      }
    },
    plugins: [
      resolve({
        preferBuiltins: true,
        browser: true
      }),
      commonjs(),
      typescript(tsconfig),
      babel({
        exclude: 'node_modules/**',
        presets: [['@babel/preset-env', { corejs: 3 }]]
      })
    ],
    external: ['core-js']
  },
  {
    input: './packages/bundle/src/bundle-native.ts',
    output: {
      format: 'umd',
      file: './dist/lemonpi-native.js',
      name: 'lemonpi',
      globals: {
        crypto: 'crypto'
      }
    },
    plugins: [
      resolve({
        preferBuiltins: true,
        browser: true
      }),
      commonjs(),
      typescript(tsconfig),
      babel({
        exclude: 'node_modules/**',
        presets: [['@babel/preset-env', { corejs: 3 }]]
      })
    ],
    external: ['core-js']
  },
  {
    input: './packages/bundle/src/display-tag.ts',
    output: {
      format: 'umd',
      file: './dist/display-tag.js',
      name: 'lemonpi',
      globals: {
        crypto: 'crypto'
      }
    },
    plugins: [
      resolve({
        preferBuiltins: true,
        browser: true
      }),
      commonjs(),
      typescript(tsconfig),
      babel({
        exclude: 'node_modules/**',
        presets: [['@babel/preset-env', { corejs: 3 }]]
      })
    ],
    external: ['core-js']
  },
  {
    input: './packages/bundle/src/omweb-v1.js',
    output: {
      format: 'umd',
      file: './dist/omweb-v1.js',
      name: 'lemonpi',
      globals: {
        crypto: 'crypto'
      }
    },
    plugins: [
      resolve({
        preferBuiltins: true,
        browser: true
      }),
      commonjs(),
      babel({
        exclude: 'node_modules/**',
        presets: [['@babel/preset-env', { corejs: 3 }]]
      })
    ],
    external: ['core-js']
  },
  {
    input: './packages/bundle/src/omid-session-client-v1.js',
    output: {
      format: 'umd',
      file: './dist/omid-session-client-v1.js',
      name: 'lemonpi',
      globals: {
        crypto: 'crypto'
      }
    },
    plugins: [
      resolve({
        preferBuiltins: true,
        browser: true
      }),
      commonjs(),
      babel({
        exclude: 'node_modules/**',
        presets: [['@babel/preset-env', { corejs: 3 }]]
      })
    ],
    external: ['core-js']
  }
];

module.exports = baseConfig;
