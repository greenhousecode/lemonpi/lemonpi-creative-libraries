# Change Log

All notable changes to this project will be documented in this file. This
project adheres to [Semantic Versioning](http://semver.org/) and
[Keep A Changelog](http://keepachangelog.com).

Release Candidates changes will be added to [Unreleased]
## [v0.17.0]

### Added
  - Finalized display script with template download
  - Lemonpi context 
## [v0.16.0]

### Added
  - New display-tag script responsible for displaying and downloading the ad template in iframe
### Changed
- Changed the way config is read and sliced for appnexus config 
## [v0.15.0]

### Added
  - The `config` can be now pulled from script query base64 encoded string
  - Appnexus macros can be pulled from script query when query param `internal` is set to `appnexus`
## [v0.14.0]

### Changed

- Removed timeout in `config` module

## [v0.13.0]

### Added

- The `context` module is now exposed through the bundle

## [v0.12.0]

### Added

- Enrich client-side context with $request object

## [v0.11.0]

### Changed

- Transpile Object methods in bundle

## [v0.10.0]

- Pass Adserver context when requesting content from dynamic
- Retrieve Appnexus Adserver context
- Add support for third party impression-trackers
- Add support for third party click-trackers

## [v0.9.0]

### Fixed

* Fix bug caused by IE not supporting NodeList.forEach
* Fix error caused by null/empty content being sent to lemonpi-track

## [v0.8.0]

### Fixed

* Fix lemonpi.js bundle for IE11 (and probably other older browsers)

## [v0.7.0]

### Changed

* Track clicks and impressions using the new LemonPI interaction handler
* Removed ability to replace placeholder values based on dom element ids

## [v0.6.0]

### Added

* Bundle that wraps all other libraries in a single package (D2867)
* Click tracking support for AppNexus (D2899)

## [v0.5.1]

### Fixed

* Prevent errors from being thrown when no content is passed to
  `replacePlaceholderValues` (D2813)

## [v0.5.0]

### Added

* LemonPI Track library can track impressions (LEMONPI-3655) (D2565)

### Changed

* LemonPI Track library can now add the LemonPI click tracker in addition to the
  DCM click tracker (LEMONPI-3653) (D2552)

## [v0.4.0]

### Added

* LemonPI Track library that can be used to convert a content object to a
  content object with DCM click tracking (LEMONPI-3652) (D2526)

## [v0.3.0]

### Added

* Show the default images configured at export time (LEMONPI-3642) (D2491)
* Show the latest available default images if LemonPI is down (LEMONPI-3643)
* Show the latest default images configured in LemonPI Manage (LEMONPI-3644)
* Config Provider to allow dynamically changing configuration of Creative
  (D2476)

### Changed

* Content script now uses Config Provider (D2476)

### Fixed

* Prevent duplicate classnames for 'click' placeholders (D2520)

## [v0.2.0]

### Added

* Content script now falls back to loading default content if dynamic content
  can't be loaded

### Changed

* Renamed content sources, we now have dynamic, default, and export content

## [v0.1.0]

### Added

* Initial setup with first version of the lemonpi-content script
